package com.phonethics.nissanmicra;

import java.util.ArrayList;

import com.phonethics.adapters.FeaturesAdapter;
import com.phonethics.adapters.FeaturesAdapterNew;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MicraFeaturesFragment extends Fragment {
	
	
	Activity 			context;
	ArrayList<String> 	data;
	ArrayList<String> 	presence;
	ArrayList<ArrayList<String>> tempData;
	ListView			listView;
	Context	context_1;
	int		index;
	FeaturesAdapterNew		adapter;
	boolean isPetrol;
	
	public MicraFeaturesFragment(){
		
	}
	
	
	public MicraFeaturesFragment(Activity context, Context context_1,ArrayList<String> data, ArrayList<ArrayList<String>> tempData, int index){
		
		this.context = context;
		this.context_1 = context_1;
		this.data = data;
		this.presence = presence;
		this.isPetrol = isPetrol;
		this.tempData = tempData;
		this.index = index;
		adapter = new FeaturesAdapterNew(context);
		
	/*	Log.d("", "Feture Fragment "+ Features_new.tempValue);
		for(int i =0; i<data.size();i++){
			Log.d("", "Feture == "+ data.get(i));
		}
		
		Toast.makeText(context_1, "New Fragment "+Features_new.tempValue, 0).show();*/
	}
	
	@Override
	public View getView() {
		// TODO Auto-generated method stub
		return super.getView();
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		if(savedInstanceState != null){
			context = (Activity) getActivity();
		}
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if(savedInstanceState != null){
			context = (Activity) getActivity().getApplicationContext();
		}
		View view	=	inflater.inflate(R.layout.fragment_layout, container,false);
		listView	= 	(ListView) view.findViewById(R.id.list_featues);	
		View footerView		= inflater.inflate(R.layout.footer_view, null,false);
		TextView	text = (TextView)footerView.findViewById(R.id.text_list_footer);
		text.setText(getResources().getString(R.string.text_footer_condition));
		adapter		= 	new FeaturesAdapterNew(context, data,tempData,index);
		
		listView.setAdapter(adapter);
		/*listView.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, data));*/		
		return view;
		
	}


	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		if(outState.isEmpty()){
			context_1 = getActivity().getApplicationContext();
		}
		
		
		
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		if(savedInstanceState != null){
			context_1 = getActivity().getApplicationContext();
		}
	}

	public void notifyData(){
		adapter		= 	new FeaturesAdapterNew(context, data,tempData,index);
		listView.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
	
	
	
}
