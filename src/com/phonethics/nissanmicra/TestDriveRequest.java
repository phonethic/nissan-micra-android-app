package com.phonethics.nissanmicra;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceActivity.Header;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.phonethics.network.NetworkCheck;

public class TestDriveRequest extends SherlockActivity implements OnNavigationListener {

	Context		context;
	Activity	actContext;
	Typeface 	font;
	NetworkCheck	netCheck;

	EditText	edit_name,edit_mobile,edit_email,edit_city,edit_model;
	TextView	text_title,text_terms;
	Button		bttn_submit;

	String		app_color	=	"";
	String		POST_URL	=	"http://nissanmicra.co.in/micra-data/testdrive.php";
	public static final int 		progress_bar_type = 0; 
	private ProgressDialog 			pDialog;

	ActionBar 	ac;
	ArrayAdapter<CharSequence> model_list;

	String statusToCheck;
	ImageView			img_trans;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_drive_request);

		context				=	this;
		actContext			=	this;

		edit_name			=	(EditText) findViewById(R.id.edit_name);
		edit_mobile			=	(EditText) findViewById(R.id.edit_mobile_no);
		edit_email			=	(EditText) findViewById(R.id.edit_email);
		edit_city			=	(EditText) findViewById(R.id.edit_city);
		edit_model			=	(EditText) findViewById(R.id.edit_model);

		text_title			=	(TextView) findViewById(R.id.text_test_drive);
		text_terms			=	(TextView) findViewById(R.id.text_test_terms);

		img_trans			=	(ImageView) findViewById(R.id.img_transperant);
		img_trans.getBackground().setAlpha(130);
		bttn_submit			=	(Button) findViewById(R.id.bttn_submit);

		netCheck			=	new NetworkCheck(context);

		font 				= 	Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf");

		ac 					= 	getSupportActionBar();
		ac.setTitle("Test Drive");
		ac.setDisplayHomeAsUpEnabled(true);
		ac.show();

		app_color = getResources().getString(R.string.app_color);
		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#"+app_color));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);

		edit_name.setTypeface(font);
		edit_mobile.setTypeface(font);
		edit_email.setTypeface(font);
		edit_city.setTypeface(font);
		edit_model.setTypeface(font);

		text_title.setTypeface(font);
		text_terms.setTypeface(font);

		bttn_submit.setTypeface(font);



		Context acontext = getSupportActionBar().getThemedContext();
		model_list = ArrayAdapter.createFromResource(acontext, R.array.mdoel_type, R.layout.list_menus);
		model_list.setDropDownViewResource(R.layout.list_dropdown_item);

		//ac.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		//ac.setListNavigationCallbacks(model_list, this);

		bttn_submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(netCheck.isNetworkAvailable()){
					
					String emailChk = edit_email.getText().toString();

					if(edit_name.length() == 0){

						Toast.makeText(context, "Please enter your name", 0).show();
					}
					else if(edit_mobile.length() == 0){

						Toast.makeText(context, "Please enter mobile number", 0).show();
					}
					else if(edit_mobile.length()<10){

						Toast.makeText(context, "Number should be 10 digits", 0).show();
					}
					else if(edit_email.length() == 0){

						Toast.makeText(context, "Please enter email", 0).show();
					}
					else if(edit_email.length() == 0){

						Toast.makeText(context, "Please enter email", 0).show();
					}
					else if(!isValidEmail(emailChk)){

						Toast.makeText(context, "Please enter a valid email", 0).show();
					}
					else if(edit_city.length() == 0){

						Toast.makeText(context, "Please enter city", 0).show();
					}
					else if(edit_model.length() == 0){

						Toast.makeText(context, "Please select model", 0).show();
					}
					else{


						PostRequest request = new PostRequest();
						request.execute("");

					}
				}else{
					Toast.makeText(context, "No Internet Connection!", Toast.LENGTH_SHORT).show();
				}
			}
		});

		edit_model.setFocusable(false);

		edit_model.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				final ArrayList<String> model_type = new ArrayList<String>();

				model_type.add("Petrol");
				model_type.add("Diesel");
				model_type.add("Active");


				final CharSequence[] cs = model_type.toArray(new CharSequence[model_type.size()]);

				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle("Select model");
				builder.setIcon(R.drawable.ic_launcher);
				builder.setItems(cs, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {

						if(cs[item].equals("Petrol")){
							edit_model.setText("Petrol");	
						}else if(cs[item].equals("Diesel")){
							edit_model.setText("Diesel");
						}else{
							edit_model.setText("Active");
						}


						//edit_model.setText(model_type.get(item).toString().toLowerCase());
					}

				});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});



	}


	class PostRequest extends AsyncTask<String, String, String> {

		String name="",mobile_no="",email="",city="",model="",message="";
		int code;
		JSONObject jObj = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showDialog(progress_bar_type);
			name = edit_name.getText().toString();
			mobile_no = edit_mobile.getText().toString();
			email = edit_email.getText().toString();
			city = edit_city.getText().toString();
			model = edit_model.getText().toString();
		}


		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			removeDialog(progress_bar_type);

			if(statusToCheck.equalsIgnoreCase("error")){

				Toast.makeText(context, ""+message, Toast.LENGTH_LONG).show();	
			}
			else{

				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle("Nissan Micra");
				alertDialogBuilder3
				.setMessage(message)
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						TestDriveRequest.this.finish();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();
			}




			super.onPostExecute(result);
		}



		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			try{
				HttpClient httpClient	=	new DefaultHttpClient();
				HttpPost httpPost		=	new HttpPost(POST_URL);
				httpPost.setHeader("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");



				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
				nameValuePairs.add(new BasicNameValuePair("name", name));
				nameValuePairs.add(new BasicNameValuePair("mobileno", mobile_no));
				nameValuePairs.add(new BasicNameValuePair("email", email));
				nameValuePairs.add(new BasicNameValuePair("city", city));
				nameValuePairs.add(new BasicNameValuePair("source", "android"));
				nameValuePairs.add(new BasicNameValuePair("model", model));
				nameValuePairs.add(new BasicNameValuePair("submit", "submit"));

				UrlEncodedFormEntity urlen	=	new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8);
				httpPost.setEntity(urlen);


				HttpResponse response 	= httpClient.execute(httpPost);
				String status = response.getStatusLine().toString();
				Log.d("Status", "Status " + status);

				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 

				}
				bufferedReader.close();
				Log.i("Response : ", " Response "+stringBuffer.toString());


				//try parse the string to a JSON object
				try{
					JSONObject job = new JSONObject(stringBuffer.toString());
					message = job.getString("message");
					statusToCheck = job.getString("err_status");

					Log.i("ChkStat", " ChkStat"+statusToCheck);

				}catch(Exception ex){
					ex.printStackTrace();
				}



			}catch(Exception ex){
				ex.printStackTrace();
			}

			return null;
		}

	}


	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setIndeterminate(false);
			pDialog.setMessage("Please wait");
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}


	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub

		String modelName = model_list.getItem(itemPosition).toString();

		//Toast.makeText(context, ""+modelName, 0).show();

		if(modelName.equalsIgnoreCase("Active")){
			edit_model.setText(model_list.getItem(itemPosition).toString().toLowerCase());	
		}else{
			edit_model.setText(model_list.getItem(itemPosition).toString().toLowerCase());
		}

		return true;
	}


	/*@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub


		SubMenu subMenu1 = menu.addSubMenu("Menu");

		subMenu1.add("Home").setIcon(R.drawable.home);
		subMenu1.add("Features").setIcon(R.drawable.fetures_tick);
		subMenu1.add("Technical Specification").setIcon(R.drawable.tech_spec);
		subMenu1.add("Price List").setIcon(R.drawable.price);
		subMenu1.add("Test Drive").setIcon(R.drawable.test_drive_new);
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);

		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Contact Us")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,ContactUs.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Features")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Features_new.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Technical Specification")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,TechnicalSpecification.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Price List")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Price_list_new.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Home")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Home_new.class);
			startActivity(intent);
			finish();
		}
		return true;
	}*/

	 //Validating Email id.
	public final static boolean isValidEmail(CharSequence target) {
	if (target == null) {
	return false;
	} else {
	return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	}
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		
		if(item.getTitle().toString().equalsIgnoreCase("Test Drive")){
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		return  true;
	}
	
	


}
