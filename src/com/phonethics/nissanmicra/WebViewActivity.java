package com.phonethics.nissanmicra;

import com.flurry.android.FlurryAgent;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class WebViewActivity extends Activity {

	WebView web;
	ImageView img,bckimg,share_btn,bck,forth,refresh;	
	ProgressBar prog;
	boolean							useFlurry;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_view);
		Bundle bundle=getIntent().getExtras();

		//img = (ImageView)findViewById(R.id.header);
		useFlurry			= Boolean.parseBoolean(getResources().getString(R.string.useflurry));
		bck = (ImageView) findViewById(R.id.webback);
		forth = (ImageView) findViewById(R.id.forth);
		refresh = (ImageView) findViewById(R.id.refresh);
		prog=(ProgressBar)findViewById(R.id.showProgress);
		web=(WebView)findViewById(R.id.linkwebview);
		web.getSettings().setJavaScriptEnabled(true);
		web.getSettings().setPluginState(PluginState.ON);
		web.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
		web.getSettings().setBuiltInZoomControls(true);
		
		web.setWebViewClient(new MyWebViewClient());
		web.loadUrl("https://www.facebook.com/nissanindia");
	}
	
	
	class MyWebViewClient extends WebViewClient {
		@Override
		// show the web page in webview but not in web browser
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			view.loadUrl(url);

		
		
				bck.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(web.canGoBack())
						{
						web.goBack();
						}
					}
				});
		
				forth.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(web.canGoForward())
						{
						web.goForward();
						}


					}
				});
		

			refresh.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					web.reload();

				}
			});



			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);
			prog.setVisibility(View.GONE);
		}
	

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
			prog.setVisibility(View.VISIBLE);

		
		}

		@Override
		public void onLoadResource(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onLoadResource(view, url);

		}

	}

	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(this, "4RX5FGXBQDT92F3KGF7J");
			FlurryAgent.logEvent("Facebook Page");
		}

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(this);
		}
	}
}
