package com.phonethics.nissanmicra;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.aphidmobile.flip.FlipViewController;
import com.google.android.gms.internal.ad;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.phonethics.adapters.FlipAdapter;
import com.phonethics.customclass.NeonGalleryLinks;
import com.phonethics.customclass.NissanGroupsAndLinks;
import com.phonethics.imageloader.ImageLoader;
import com.phonethics.network.NetworkCheck;
import com.phonethics.parser.SAXXMLParser;
import com.phonethics.pref.Share;

public class Home extends SherlockActivity {

	Context							context;
	Activity						actContext;

	//String 							xml,NEONXML		="/sdcard/neoncache/neongallery1.xml";
	//String 							xmlurl			= "http://stage.phonethics.in/proj/neon/neon_gallery1.php";

	String 							xml,NEONXML		="/sdcard/neoncache/micra.xml";
	String 							xmlurl			= "http://stage.phonethics.in/proj/neon/micra_gallery.php";


	FileInputStream					filename;
	NissanGroupsAndLinks			nissanGrpsLinks;
	ArrayList<Integer> 				countArr;
	ArrayList<ArrayList<String>> 	allLinks;
	ArrayList<ArrayList<String>> 	allNames;
	ArrayList<ArrayList<String>> 	allCaption;
	ArrayList<ArrayList<String>> 	allThumbnails,allThumbnailsCopy,allDescription,allVLinks;
	ArrayList<NeonGalleryLinks> 	neongallery;
	ArrayList<String> 				link,caption,description,carName,vlinks,groups,groupsData,captionData,tempGroup,thumbnailArr,orderArr;
	ImageLoader						imgloader;
	NetworkCheck					netAvailable;
	FileFromURL 					downloadFile;
	public static final int 		progress_bar_type = 0; 
	//NissanGalleryAdapter			adapter;
	int                             SCREEN_WIDTH,IMAGE_HIGHT,ANIMATE_DURATION = 3000,x=0,x0=0,x1=0,x2=0,x5=0,x6=0,x7=0,x8=0,x9=0,x10=0,x11=0,y=0,y1=0;
	int								count =0;
	int 							VISIBLE_PICS = 2;
	ArrayList<ImageView>			imgview_arr,imgview_arr_1;
	Animation 						anim_push_up,anim_push_down,anim_from_right,anim_from_left,anim_fade_in,anim_fade_out,anim1;
	Animation						temp_fade_in,temp_fade_out;
	Handler							handler_pos_1,hanler_pos_2;
	Runnable						runnable_pos_1,flipRunnable;
	boolean							runWithFlip = false;
	ArrayList<Animation> 			animArr;	
	Random 							randomGenerator;
	ValueAnimator					val_anim,val_anim1;
	int								flip_duration = 800;
	boolean							selfTabVisible 		= true;
	boolean							downloadEveryTime 	= true;
	boolean							applyFlip			= true;
	ArrayList<FlipViewController> 	flipView_arr;


	DefaultHttpClient 	httpClient;
	HttpPost 			httpPost;
	HttpResponse		httpRes;
	HttpEntity			httpEnt;

	Share				share;


	private ProgressDialog 			pDialog;
	ListView						 list;
	LinearLayout					mainLayout;
	FlipAdapter adapter;
	ActionBar ac;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		context				= this;
		actContext			= this;

		ac = getSupportActionBar();
		ac.setTitle("Nissan Micra");
		ac.show();


		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);

		link				= new ArrayList<String>();
		description			= new ArrayList<String>();
		carName				= new ArrayList<String>();
		vlinks				= new ArrayList<String>();
		groups				= new ArrayList<String>();
		groupsData			= new ArrayList<String>();
		caption				= new ArrayList<String>();
		captionData			= new ArrayList<String>();
		tempGroup			= new ArrayList<String>();
		thumbnailArr		= new ArrayList<String>();
		orderArr			= new ArrayList<String>();
		countArr			= new ArrayList<Integer>();

		allLinks 			= new ArrayList<ArrayList<String>>();
		allNames 			= new ArrayList<ArrayList<String>>();
		allCaption			= new ArrayList<ArrayList<String>>();
		allThumbnails		= new ArrayList<ArrayList<String>>();
		allThumbnailsCopy	= new ArrayList<ArrayList<String>>();
		allDescription		= new ArrayList<ArrayList<String>>();
		allVLinks			= new ArrayList<ArrayList<String>>();

		imgview_arr			= new ArrayList<ImageView>();
		imgview_arr_1		= new ArrayList<ImageView>();
		animArr				= new ArrayList<Animation>();

		nissanGrpsLinks 	= new NissanGroupsAndLinks();
		imgloader 			= new ImageLoader(context);
		netAvailable 		= new NetworkCheck(context);
		randomGenerator 	= new Random();	
		share				= new Share(context);

		mainLayout			= (LinearLayout) findViewById(R.id.main_layout);
		list 				= (ListView) findViewById(R.id.list_photos);

		flipView_arr		= new ArrayList<FlipViewController>();
		flipView_arr.add(new FlipViewController(context, FlipViewController.VERTICAL));


		anim_push_up		= AnimationUtils.loadAnimation(context, R.anim.push_up_in);
		anim_push_down		= AnimationUtils.loadAnimation(context, R.anim.push_down_in);
		anim_from_left		= AnimationUtils.loadAnimation(context, R.anim.slide_anim_from_left);
		anim_from_right		= AnimationUtils.loadAnimation(context, R.anim.slide_anim_from_right);

		anim_fade_in		= AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
		anim_fade_out		= AnimationUtils.loadAnimation(context, R.anim.shrink_fade_out_center);

		temp_fade_in = anim_fade_in;
		temp_fade_out = anim_fade_out;



		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		SCREEN_WIDTH = metrics.widthPixels;
		if(SCREEN_WIDTH == 480){
			IMAGE_HIGHT = (int) ((SCREEN_WIDTH/1.5)-5);
		}else if(SCREEN_WIDTH == 800){
			IMAGE_HIGHT = (int) ((SCREEN_WIDTH/1.5));
		}else{
			IMAGE_HIGHT = (int) ((SCREEN_WIDTH/1.5));
			IMAGE_HIGHT = IMAGE_HIGHT;
		}

		boolean isFileAvailable = false;
		if(share.isAppOpen()){
			//Toast.makeText(context, "App Launch "+share.isAppLaunch(), 0).show();
			if(!downloadEveryTime){
				isFileAvailable = checkNetworkAndFile();
				if(!isFileAvailable){
					if(netAvailable.isNetworkAvailable()){
						File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
						if(!myDirectory.exists()) {                                 
							myDirectory.mkdirs();
						}

						File xmlFile1	=	new File(NEONXML);
						downloadFile	=	new FileFromURL();
						downloadFile.execute(xmlurl);

					}else{
						Toast.makeText(context, "Check Internet connection", Toast.LENGTH_SHORT).show();

					}
				}

			}else{
				if(!isFileAvailable){
					if(netAvailable.isNetworkAvailable()){
						File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
						if(!myDirectory.exists()) {                                 
							myDirectory.mkdirs();
						}

						File xmlFile1	=	new File(NEONXML);
						downloadFile	=	new FileFromURL();
						downloadFile.execute(xmlurl);

					}else{
						isFileAvailable = checkNetworkAndFile();
						if(!isFileAvailable){
							Toast.makeText(context, "Check Internet connection", Toast.LENGTH_SHORT).show();
						}
					}
				}
			}
		}else{
			//Toast.makeText(context, "App Launch "+share.isAppLaunch(), 0).show();
			isFileAvailable = checkNetworkAndFile();
			if(!isFileAvailable){
				Toast.makeText(context, "Check Internet connection", Toast.LENGTH_SHORT).show();
			}
		}
		/*setLinksAndGroups();


		allThumbnails = nissanGrpsLinks.getAllThumbnails();
		allCaption = nissanGrpsLinks.getAllCaption();
		allNames	= nissanGrpsLinks.getAllNames();

		printData();
		setLayout();
		addAnimations();
		applyAnimation(true);*/
		//adapter = new NissanGalleryAdapter(actContext, orderArr,allThumbnails,allCaption);
		//list.setAdapter(adapter);

		//int pos = list.getFirstVisiblePosition() - list.getLastVisiblePosition();
		//Toast.makeText(context, "position "+ pos, 0).show();
		//adapter.startAnim(0);

		list.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub

				switch (scrollState) {
				case OnScrollListener.SCROLL_STATE_FLING:

					break;
				case OnScrollListener.SCROLL_STATE_IDLE:

					int pos = list.getLastVisiblePosition();
					//Toast.makeText(context, "position "+ pos, 0).show();
					//adapter.startAnim(pos);

					break;
				case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
					//Toast.makeText(context, "Touch scroll", 0).show();
					//adapter.stopAnim();
					break;
				default:
					break;
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				//Toast.makeText(context, "scroll "+ "First "+firstVisibleItem + " visible "+visibleItemCount+" total "+ totalItemCount, 0).show();
			}
		});





	}



	public boolean checkNetworkAndFile(){
		try{
			File xmlFile=new File(NEONXML);
			if(xmlFile.exists() && xmlFile.length()!=0){

				filename	=	new FileInputStream(NEONXML);
				neongallery	=	SAXXMLParser.parse(filename);

				addData();	
				setLinksAndGroups();



				printData();
				setLayout();
				addAnimations();


				applyAnim(true);
				applyFlipAnimation();
				return true;
			}else{
				return false;
			}
		}catch(NullPointerException npx){
			npx.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return false;

	}


	public void addData(){
		
		link.clear();
		description.clear();
		vlinks.clear();
		carName.clear();
		caption.clear();
		thumbnailArr.clear();
		groupsData.clear();
		orderArr.clear();
		imgview_arr_1.clear();
		imgview_arr.clear();
		flipView_arr.clear();
		countArr.clear();
		
		try{
			for(int i=0; i<neongallery.size();i++){
				link.add(neongallery.get(i).getLINK());
				description.add(neongallery.get(i).getDESCRIPTION());
				vlinks.add(neongallery.get(i).getVLINK());
				carName.add(neongallery.get(i).getCARRNAME());
				caption.add(neongallery.get(i).getCAPTION());
				thumbnailArr.add(neongallery.get(i).getThumbnail());
				groupsData.add(neongallery.get(i).getGROUP());
				orderArr.add(neongallery.get(i).getOrder());
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Log.d("!!!!!!!!!!!!!!!", "Exception - add data");
		}
		/*
		allThumbnails 	= nissanGrpsLinks.getAllThumbnails();
		allCaption 		= nissanGrpsLinks.getAllCaption();
		allNames		= nissanGrpsLinks.getAllNames();
		allDescription  = nissanGrpsLinks.getAlldescription();
		allVLinks		= nissanGrpsLinks.getAllvlink();
		allLinks		= nissanGrpsLinks.getAllLinks();*/
		//allThumbnailsCopy	= nissanGrpsLinks.getAllThumbnails();

	}

	public void setLinksAndGroups(){

		try{
			for(int i=0; i<orderArr.size();i++){

				String	grpStr 			= groupsData.get(i);
				String	carNameStr		= carName.get(i);

				//nissanGrpsLinks.setGroup(grpStr);
				nissanGrpsLinks.setOrder(orderArr.get(i));

				if(i==groupsData.size()-1){
					nissanGrpsLinks.setLastLink(true);
				}
				nissanGrpsLinks.setCaption(caption.get(i));	
				nissanGrpsLinks.setLink(link.get(i));
				nissanGrpsLinks.setCarName(carNameStr);
				nissanGrpsLinks.setThumbnail(thumbnailArr.get(i));
				nissanGrpsLinks.setDescription(description.get(i));
				nissanGrpsLinks.setvLink(vlinks.get(i));


			}
		}catch(Exception ex){
			ex.printStackTrace();
			Log.d("!!!!!!!!!!!!!!!", "Exception - setLinksAndGroups");
		}
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();


		cancelHandlers();
	}

	/*@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		applyAnimation(true);
	}*/



	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		//applyAnimation(true);
		applyAnim(true);
	}



	public void removeAnimation(){
		//adapter.stopAnim();
	}


	public void restartAnimation(int position){
		//adapter.startAnim(position);
	}

	public void printData(){

		

		allThumbnails 	= nissanGrpsLinks.getAllThumbnails();
		allCaption 		= nissanGrpsLinks.getAllCaption();
		allNames		= nissanGrpsLinks.getAllNames();
		allDescription  = nissanGrpsLinks.getAlldescription();
		allVLinks		= nissanGrpsLinks.getAllvlink();
		allLinks		= nissanGrpsLinks.getAllLinks();


		orderArr.clear();
		orderArr = nissanGrpsLinks.getOrderArr();
		Log.d("Message", "==== order arr "+ orderArr.size() );
		Log.d("Message", "==== thumbnail arr "+ allThumbnails.size() );
		for(int i=0;i<allThumbnails.size();i++){
			ArrayList<String> tempList = allThumbnails.get(i);
			for(int p=0;p<tempList.size();p++){
				Log.d(">>>>", ">>>> url "+i+"--"+p+" >>> " +tempList.get(p));
			}
		}
		allThumbnails.remove(0);
		allCaption.remove(0);
		allNames.remove(0);
		allDescription.remove(0);
		allVLinks.remove(0);
		allLinks.remove(0);
		/*allThumbnailsCopy = allThumbnails;*/
		/*for(int i =0;i<allThumbnails.size();i++){
			allThumbnailsCopy.add(allThumbnails.get(i));
		}*/
		allThumbnailsCopy.addAll(allThumbnails);
		Log.d("Message", "==== thumbnail arr size "+ allThumbnails.size() +" copu arr size "+allThumbnailsCopy.size() );


	}

	public void addAnimations(){
		for(int i=0;i<orderArr.size();i++){
			animArr.add(anim_push_up);
			animArr.add(anim_push_down);
			animArr.add(anim_from_right);
			animArr.add(anim_from_left);
			animArr.add(anim_fade_in);
			//animArr.add(anim_fade_out);
		}
	}
	public void cancelHandlers(){

		if(handler_pos_1!=null){


			handler_pos_1.removeCallbacks(runnable_pos_1);
			imgloader.clearCache();
		}
	}

	public void setLayout(){

		LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT,1f);
		params1.setMargins(2, 2, 2, 0);


		for(int i=0;i<orderArr.size();i++){

			final int position = i;

			LinearLayout 	childLinear		= new LinearLayout(this);
			RelativeLayout 	childRelative_1 = new RelativeLayout(this);
			childRelative_1.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT));

			if(i!=orderArr.size()-1){
				countArr.add(i, 0);
				ImageView	 	childImage1	= new ImageView(this);
				ImageView 		childImage2	= new ImageView(this);

				childImage1.setId(1);
				childImage2.setId(2);
				childImage1.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT));
				childImage2.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT));
				childImage1.setScaleType(ScaleType.FIT_XY);
				childImage2.setScaleType(ScaleType.FIT_XY);
				imgview_arr_1.add(childImage1);
				imgview_arr.add(childImage2);

				RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
				//par.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
				par.addRule(RelativeLayout.BELOW,childImage1.getId());
				par.addRule(RelativeLayout.BELOW,childImage2.getId());

				final TextView			childtextView1 = new TextView(this);
				childtextView1.setLayoutParams(par);

				childtextView1.setGravity(Gravity.CENTER);
				childtextView1.setBackgroundColor(Color.parseColor("#FFFFFF"));
				childtextView1.setTextSize(22);
				//childtextView1.getBackground().setAlpha(220);
				childtextView1.setPadding(2, 2, 2, 2);
				childtextView1.setText(allCaption.get(i).get(0).toString());
				childtextView1.setTextColor(Color.parseColor("#c71444"));

				imgloader.DisplayImage(allThumbnails.get(i).get(0), childImage2);

				childRelative_1.addView(childImage1);
				childRelative_1.addView(childImage2);
				childRelative_1.addView(childtextView1);

				childLinear.addView(childRelative_1, params1);
				mainLayout.addView(childLinear);

				childImage2.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						//Toast.makeText(context, "Position " + position, 0).show();
						Intent intent = new Intent(context, NissanCarGallery.class);
						ArrayList<String> thumbnails = allLinks.get(position);
						//Toast.makeText(context, "Size "+thumbnails.size(), 0).show();
						intent.putStringArrayListExtra("imgArr", allLinks.get(position));
						intent.putStringArrayListExtra("imgDesc", allDescription.get(position));
						intent.putStringArrayListExtra("imgCaption", allCaption.get(position));
						intent.putStringArrayListExtra("vlinksArr", allVLinks.get(position));
						startActivity(intent);

					}
				});
			}else{
				ArrayList<String> tempArrayList = allThumbnails.get(i);
				FlipViewController flipView = new FlipViewController(context, FlipViewController.VERTICAL);
				flipView.setAnimationBitmapFormat(Config.RGB_565);
				flipView.setId(i);
				flipView.setLayoutParams(new RelativeLayout.LayoutParams(720,IMAGE_HIGHT));
				adapter = new FlipAdapter(actContext, context, tempArrayList,allDescription.get(position), allCaption.get(position),allVLinks.get(position));
				flipView.setAdapter(adapter);

				flipView_arr.add(flipView);

				RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
				//par.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
				par.addRule(RelativeLayout.BELOW,flipView.getId());
				//par.addRule(RelativeLayout.BELOW,childImage2.getId());

				final TextView			childtextView1 = new TextView(this);
				childtextView1.setLayoutParams(par);

				childtextView1.setGravity(Gravity.CENTER);
				childtextView1.setBackgroundColor(Color.parseColor("#FFFFFF"));
				childtextView1.setTextSize(22);
				//childtextView1.getBackground().setAlpha(220);
				childtextView1.setPadding(2, 2, 2, 2);
				childtextView1.setText(allCaption.get(i).get(0).toString());
				childtextView1.setTextColor(Color.parseColor("#c71444"));

				//imgloader.DisplayImage(allThumbnails.get(i).get(0), childImage2);

				childRelative_1.addView(flipView);
				childRelative_1.addView(childtextView1);
				childLinear.addView(childRelative_1, params1);
				mainLayout.addView(childLinear);

				/*flipView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						//Toast.makeText(context, "Position " + position, 0).show();
						Intent intent = new Intent(context, NissanCarGallery.class);
						intent.putStringArrayListExtra("imgArr", allThumbnailsCopy.get(position));
						intent.putStringArrayListExtra("imgDesc", allDescription.get(position));
						intent.putStringArrayListExtra("imgCaption", allCaption.get(position));
						intent.putStringArrayListExtra("vlinksArr", allVLinks.get(position));
						startActivity(intent);

					}
				});*/



			}
		}
	}






	public void applyAnim(boolean apply){
		if(apply){

			x2=0;

			handler_pos_1 = new Handler();
			runnable_pos_1 = new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					Log.d("=============", " ========= loop "+x2+" ============");
					for(int i=0;i<countArr.size();i++){
						Log.d("===", "Count === "+countArr.get(i));
					}

					for(int i=0;i<orderArr.size()-1;i++){
						final int x1 = i;
						x = countArr.get(x1);
						int randomInt = randomGenerator.nextInt(orderArr.size());
						anim1 = animArr.get(randomInt);
						final ArrayList<String> tempArrayList = new ArrayList<String>();
						//if(i!=0){
						if(!(allThumbnails.get(i).size()<=VISIBLE_PICS)){
							for(int p=0;p<VISIBLE_PICS+1;p++){
								tempArrayList.add(allThumbnails.get(i).get(p));
							}
						}else{
							tempArrayList.addAll(allThumbnails.get(i));
						}

						//}
						Log.i("=========", "Index "+i + " Size "+tempArrayList.size() + " Cont Value "+countArr.get(x1) );
						imgloader.DisplayImage(tempArrayList.get(x), imgview_arr.get(x1));
						imgview_arr.get(x1).startAnimation(anim1);
						anim1.setFillAfter(true);
						anim1.setAnimationListener(new AnimationListener() {

							@Override
							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationEnd(Animation animation) {
								// TODO Auto-generated method stub
								/*x5++;*/
								x = countArr.get(x1);
								x = x+1;
								if(x==tempArrayList.size()||x>2){
									x=0;		
								}
								countArr.set(x1, x);
								if(x!=0){
									imgloader.DisplayImage(tempArrayList.get(x-1), imgview_arr_1.get(x1));
								}else if(x==0){
									imgloader.DisplayImage(tempArrayList.get(tempArrayList.size()-1), imgview_arr_1.get(x1));
								}
							}
						});
					}
					x2++;
					handler_pos_1.postDelayed(runnable_pos_1,3000);
				}

			};
			handler_pos_1.postDelayed(runnable_pos_1,500);
		}else{
			handler_pos_1.removeCallbacks(runnable_pos_1);
		}
	}


	void applyFlipAnimation(){

		//Toast.makeText(context, ""+flipView_arr.size(), 0).show();
		
		hanler_pos_2 = new Handler();
		flipRunnable = new Runnable() {


			final ArrayList<String> tempArrayList = allThumbnails.get(allThumbnails.size()-1);
			int p = 0 ;
			@Override
			public void run() {


				try{
					// TODO Auto-generated method stub
					if(p>=tempArrayList.size()-1) {
						/*if(flipView.getLocalVisibleRect(scrollBounds)){
						p = 0;    
						flipView.setSelection(p);
						flipView.autoFlip(); 
					}
					p = 0;    
				flipView.setSelection(p);
				flipView.autoFlip(); 
				}else{
					if(flipView.getLocalVisibleRect(scrollBounds)){

						flipView.autoFlip(); 
					}
					flipView.autoFlip();
				}*/
						p = 0; 
						flipView_arr.get(0).setSelection(p);
						flipView_arr.get(0).autoFlip();
					}else{
						flipView_arr.get(0).autoFlip();
					}
					p++;
					hanler_pos_2.postDelayed(this, 3000);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		};
		hanler_pos_2.postDelayed(flipRunnable,0);
	}

	/*void applyFlipAnimation(final ImageView imgview1,ImageView imgview2,final ArrayList<String> imgArr){
		count =0;
		final Handler handler = new Handler();
		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				final Handler handler = new Handler();
				handler.postDelayed(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						if(count>=imgArr.size()-1){
							count = 0;
							val_anim1 = ObjectAnimator.ofFloat((View)imgview1, "rotationX", 90, 0, 0);
							//target.setBackgroundResource(imageArray[count]);
							imgloader.DisplayImage(imgArr.get(count), imgview1);
							val_anim1.setDuration(flip_duration);
							val_anim1.start();
						}else{
							try {


								val_anim1 = ObjectAnimator.ofFloat((View)imgview1, "rotationX", 90, 0, 0);
								//target.setBackgroundResource(imageArray[++count]);
								imgloader.DisplayImage(imgArr.get(++count), imgview1);
								val_anim1.setDuration(flip_duration);
								val_anim1.start();

							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
						}

					}

				},411);
				handler.postDelayed(this, 3000);
			}
		};
		handler.postDelayed(runnable,0);



	}*/

	class FileFromURL extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... xmlur) {
			// TODO Auto-generated method stub
			int count;

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(xmlurl);
				httpRes		=	httpClient.execute(httpPost);
				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	= DocumentBuilderFactory.newInstance();
				DocumentBuilder			db	= dbf.newDocumentBuilder();
				InputSource				is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));

				URL url = new URL(xmlur[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				// getting file length
				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file

				try{
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
				FileOutputStream output = new FileOutputStream(NEONXML);
				byte data[] = new byte[1024];
				long total = 0;
				while (   ((count = input.read(data)) != -1)  ) {
					total += count;
					output.write(data, 0, count);
				}
				output.flush();
				output.close();
				input.close();
			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");

			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			try {
				removeDialog(progress_bar_type);
				filename = new FileInputStream(NEONXML);
				neongallery = SAXXMLParser.parse(filename);
				addData();
				setLinksAndGroups();
				
				printData();
				setLayout();
				addAnimations();
				applyAnim(true);
				applyFlipAnimation();
				mainLayout.setVisibility(View.VISIBLE);


			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(NullPointerException npx){
				npx.printStackTrace();
			}
			catch(Exception ex){
			}
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			if(adapter!=null){
				adapter = null;
			}
			try{
				if(hanler_pos_2!=null){
					hanler_pos_2.removeCallbacks(flipRunnable);
				}
				if(handler_pos_1!=null){
					handler_pos_1.removeCallbacks(runnable_pos_1);
				}
				flipView_arr.clear();
				imgview_arr.clear();
			}catch(Exception ex){
				ex.printStackTrace();
			}
			mainLayout.setVisibility(View.INVISIBLE);
			showDialog(progress_bar_type);

		}
	}


	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setIndeterminate(false);
			pDialog.setMessage("Please wait");
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub



		MenuItem subMenu2= menu.add("Refresh");
		subMenu2.setIcon(R.drawable.refresh_new1);
		subMenu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		SubMenu subMenu1 = menu.addSubMenu("Menu");


		if(selfTabVisible){
			subMenu1.add("Home").setIcon(R.drawable.home);
		}
		subMenu1.add("Features").setIcon(R.drawable.fetures_tick);
		subMenu1.add("Technical Specification").setIcon(R.drawable.tech_spec);
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);



		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Contact Us")){
			share.setAppClose(false);
			Intent intent=new Intent(this,ContactUs.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Features")){
			share.setAppClose(false);
			Intent intent=new Intent(this,Features_new.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Technical Specification")){
			share.setAppClose(false);
			Intent intent=new Intent(this,TechnicalSpecification.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Refresh")){
			
			if(netAvailable.isNetworkAvailable()){
				File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
				if(!myDirectory.exists()) {                                 
					myDirectory.mkdirs();
				}
				neongallery = null;
				File xmlFile1	=	new File(NEONXML);
				downloadFile	=	new FileFromURL();
				downloadFile.execute(xmlurl);

			}else{
				Toast.makeText(context, "Check Internet connection", Toast.LENGTH_SHORT).show();
			}
		}
		/*if(item.getTitle().toString().equalsIgnoreCase("Traffic Rules")){
			Intent intent=new Intent(this,TrafficSigns.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Find My Car")){
			Intent intent=new Intent(this,Nissan_Map.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Reminders")){
			Intent intent=new Intent(this,Remeinders.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("My Car")){
			Intent intent=new Intent(this,MycarActivity.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Tips")){
			Intent intent=new Intent(this,Tips.class);
			startActivity(intent);
			finish();
		}	
		 */

		return true;
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		share.setAppClose(true);
		this.finish();
	}
}
