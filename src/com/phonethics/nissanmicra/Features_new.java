package com.phonethics.nissanmicra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.R.integer;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.flurry.android.FlurryAgent;
import com.phonethics.adapters.FeaturesAdapterNew;
import com.phonethics.database.MicraDataBase;
import com.phonethics.pref.Share;

public class Features_new extends SherlockFragmentActivity implements OnNavigationListener {


	Context							context;
	Activity						actContext;
	MicraDataBase					database;
	ArrayList<String>				featuresArr1;
	MicraFeaturesFragment			fragment;
	ArrayList<String>				temp_data;
	ArrayList<ArrayList<String>> 	featuresArrPetrol,featuresArrDiesel,uniqueFeutresArrPetrol,uniqueFeutresArrDiesel;
	ArrayList<ArrayList<String>> 	featuresArrAct,uniqueFeaturesArrAct;
	ArrayList<ArrayList<String>> 	presenceArr;

	ArrayList<ArrayList<String>> 	petrolInfoArr,uniquePetrolArr;
	ArrayList<ArrayList<String>> 	dieselInfoArr,uniqueDieselArr;
	ArrayList<ArrayList<String>> 	activeInfoArr,uniqueActiveArr;

	ArrayList<Integer> 				startValuePetrol,uniqueStartValPetrol;
	ArrayList<Integer> 				startValueDiesel,uniqueStartValDiesel;
	ArrayList<Integer> 				startValueActive,uniqueStartValActive;

	ArrayList<Boolean> 				differSameAr;
	ActionBar 						ac;
	TextView						text_1,text_2,text_3,text_4,text_5,text_feture;
	Button							bttn_diff,bttn_similar;

	boolean							selfTabVisible 		= true;
	boolean							isCarMicra			= true;
	public static boolean			isModelPetrol		= true;
	public static boolean			isModelActive		= false;
	public static boolean			asDifference		= false;

	String					modelName				= "Model";

	private 				PagerSlidingTabStrip 	tabs_petrol;
	private 				PagerSlidingTabStrip 	tabs_diesel;
	private 				PagerSlidingTabStrip 	tabs_active;


	private 				ViewPager 					pager_petrol,pager_disel,pager_active;
	private 				ViewPager 					unique_pager_petrol,unique_pager_disel,unique_pager_active;
	private 				PetrolPagerAdapter 			adapter_petrol;
	private 				UniquePetrolPagerAdapter 	unique_adapter_petrol;
	private 				DieselPagerAdapter 			adapter_diesel;
	private 				UniqueDieselPagerAdapter 	unique_adapter_diesel;
	private 				ActivePagerAdapter 			adapter_active;
	private 				UniqueActivePagerAdapter 	unique_adapter_active;

	int						count;
	int						petrolIndex,dieselIndex,activeIndex;

	String					xl_pet 		= "XL_Pet";
	String					xl_dsl 		= "XL_Dsl";
	String					xl_0_pet 	= "XL_O_Pet";
	String					xl_0_dsl 	= "XL_O_Dsl";
	String					xv_cvt 		= "XV_CVT";
	String					xv_dsl 		= "XV_Dsl";
	String					xv_pre_dsl 	= "XV_Premium_Dsl";
	String					tempModel 	= "Petrol";

	String					car_type_micra 	= "Micra";
	String					car_type_active = "Active";

	String					model_type_petrol = "Petrol";
	String					model_type_diesel = "Diesel";
	String					model_type_active = "Active";

	String					petrol_xl		= "XL";
	String					petrol_xl_0		= "XL(0)";
	String					petrol_xv		= "XV";

	String					diesel_xl		= "XL";
	String					diesel_xl_0		= "XL(0)";
	String					diesel_xv		= "XV";
	String					diesel_xv_pre	= "XV(P)";

	String					active_e0	= "XE";
	String					active_e1	= "XL";
	String					active_e2	= "XV";
	String					active_e2_safe	= "XV(S)";

	public static String	tempValue = "Petrol";
	FeaturesAdapterNew		adap;

	Share					share;
	boolean					useFlurry,HIDE_SIMILAR = false;
	Typeface 				font;
	Map<String, String> 	articleParams;
	String					app_color="";
	ArrayAdapter<CharSequence> model_list;
	
	ArrayList<String> galallLinks, galallDescription, galallCaption, galallVLinks, galallNames;
	int counter = 0;
	
	Bundle bundle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_features_new);

		context 		= this;
		actContext		= this;

		// Getting gallery data
		
		bundle = getIntent().getExtras();
		if(bundle!=null){
			
			galallLinks = bundle.getStringArrayList("imgArr");
			galallDescription	= bundle.getStringArrayList("imgDesc");
			galallCaption = bundle.getStringArrayList("imgCaption");
			galallVLinks = bundle.getStringArrayList("vlinksArr");
			galallNames = bundle.getStringArrayList("allNames");
			//Toast.makeText(context, ""+imgLinks.size(), 0).show();
		}
		
		font 		= Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf");
		LayoutInflater inflater 		= LayoutInflater.from(this);
		View customView 				= inflater.inflate(R.layout.action_bar_title_text, null);
		TextView textTitle				= (TextView) customView.findViewById(R.id.text_action_title);
		textTitle.setText("Features");
		textTitle.setTypeface(font);

		ac 				= getSupportActionBar();
		ac.setCustomView(customView);
		ac.setDisplayShowTitleEnabled(true);
		ac.setDisplayShowCustomEnabled(false);
		ac.setTitle("Features");
		ac.show();

		Context acontext = getSupportActionBar().getThemedContext();
		model_list = ArrayAdapter.createFromResource(acontext, R.array.mdoel_type, R.layout.list_menus);
		model_list.setDropDownViewResource(R.layout.list_dropdown_item);


		//Setting Navigation Type.

		ac.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		ac.setListNavigationCallbacks(model_list, this);

		//Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		app_color = getResources().getString(R.string.app_color);
		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#"+app_color));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);

		fragment	= new MicraFeaturesFragment();
		font 		= Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf");
		text_1		= (TextView) findViewById(R.id.text_1);
		text_2		= (TextView) findViewById(R.id.text_2);
		text_3		= (TextView) findViewById(R.id.text_3);
		text_4		= (TextView) findViewById(R.id.text_4);
		text_5		= (TextView) findViewById(R.id.text_5);
		text_feture	= (TextView) findViewById(R.id.text_feature_content);
		text_1.setTypeface(font);
		text_2.setTypeface(font);
		text_3.setTypeface(font);
		text_4.setTypeface(font);
		text_5.setTypeface(font);
		text_feture.setTypeface(font);

		bttn_diff	= (Button) findViewById(R.id.bttn_diff);
		bttn_similar = (Button) findViewById(R.id.bttn_similar);
		bttn_diff.setTypeface(font);
		bttn_similar.setTypeface(font);
		bttn_diff.setVisibility(View.VISIBLE);
		bttn_similar.setVisibility(View.VISIBLE);

		tabs_petrol 		= (PagerSlidingTabStrip) findViewById(R.id.tabs_petrol);
		tabs_diesel 		= (PagerSlidingTabStrip) findViewById(R.id.tabs_diesel);
		tabs_active 		= (PagerSlidingTabStrip) findViewById(R.id.tabs_active);
		tabs_petrol.setTypeface(font, Typeface.BOLD);
		tabs_diesel.setTypeface(font, Typeface.BOLD);
		tabs_active.setTypeface(font, Typeface.BOLD);

		pager_petrol		= (ViewPager) findViewById(R.id.pager_petrol);
		unique_pager_petrol = (ViewPager) findViewById(R.id.pager_petrol_unique);
		pager_disel			= (ViewPager) findViewById(R.id.pager_diesel);
		unique_pager_disel 	= (ViewPager) findViewById(R.id.pager_diesel_unique);
		pager_active		= (ViewPager) findViewById(R.id.pager_active);
		unique_pager_active	= (ViewPager) findViewById(R.id.pager_active_unique);

		adapter_petrol 			= new PetrolPagerAdapter(getSupportFragmentManager());
		unique_adapter_petrol 	= new UniquePetrolPagerAdapter(getSupportFragmentManager());
		pager_petrol.setAdapter(adapter_petrol);
		unique_pager_petrol.setAdapter(unique_adapter_petrol);

		adapter_diesel 			= new DieselPagerAdapter(getSupportFragmentManager());
		unique_adapter_diesel	= new UniqueDieselPagerAdapter(getSupportFragmentManager());
		pager_disel.setAdapter(adapter_diesel);
		unique_pager_disel.setAdapter(unique_adapter_diesel);

		adapter_active 			= new ActivePagerAdapter(getSupportFragmentManager());
		unique_adapter_active	= new UniqueActivePagerAdapter(getSupportFragmentManager());
		pager_active.setAdapter(adapter_active);
		unique_pager_active.setAdapter(unique_adapter_active);

		useFlurry			= Boolean.parseBoolean(getResources().getString(R.string.useflurry));

		articleParams		= new HashMap<String, String>();

		final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
		pager_petrol.setPageMargin(pageMargin);

		tabs_petrol.setViewPager(pager_petrol);
		tabs_petrol.setIndicatorHeight(6);
		tabs_petrol.setIndicatorColor(Color.parseColor("#"+app_color));


		tabs_diesel.setViewPager(pager_disel);
		tabs_diesel.setIndicatorHeight(6);
		tabs_diesel.setIndicatorColor(Color.parseColor("#"+app_color));

		tabs_active.setViewPager(pager_active);
		tabs_active.setIndicatorHeight(6);
		tabs_active.setIndicatorColor(Color.parseColor("#"+app_color));

		featuresArrPetrol		= new ArrayList<ArrayList<String>>();
		featuresArrDiesel		= new ArrayList<ArrayList<String>>();
		featuresArrAct			= new ArrayList<ArrayList<String>>();

		uniqueFeutresArrPetrol		= new ArrayList<ArrayList<String>>();
		uniqueFeutresArrDiesel		= new ArrayList<ArrayList<String>>();
		uniqueFeaturesArrAct		= new ArrayList<ArrayList<String>>();


		presenceArr				= new ArrayList<ArrayList<String>>();

		activeInfoArr			= new ArrayList<ArrayList<String>>();
		petrolInfoArr			= new ArrayList<ArrayList<String>>();
		dieselInfoArr			= new ArrayList<ArrayList<String>>();

		uniquePetrolArr			= new ArrayList<ArrayList<String>>();
		uniqueDieselArr			= new ArrayList<ArrayList<String>>();
		uniqueActiveArr			= new ArrayList<ArrayList<String>>();

		differSameAr		= new ArrayList<Boolean>();
		startValuePetrol	= new ArrayList<Integer>();
		startValueActive	= new ArrayList<Integer>();

		uniqueStartValPetrol	= new ArrayList<Integer>();
		uniqueStartValDiesel	= new ArrayList<Integer>();
		uniqueStartValActive	= new ArrayList<Integer>();


		database 	= new MicraDataBase(context);
		database.create();

		adap		= new FeaturesAdapterNew(actContext);
		share		= new Share(context);

		featuresArrPetrol.add(database.getExternalFeatures());
		featuresArrPetrol.add(database.getComfortFeatures());
		featuresArrPetrol.add(database.getInteriorsFeatures());
		featuresArrPetrol.add(database.getSafetyFeatures());

		featuresArrDiesel.add(database.getExternalFeatures());
		featuresArrDiesel.add(database.getComfortFeatures());
		featuresArrDiesel.add(database.getInteriorsFeatures());
		featuresArrDiesel.add(database.getSafetyFeatures());

		featuresArrAct.add(database.getActiveExternalFeatures());
		featuresArrAct.add(database.getActiveComfortFeatures());
		featuresArrAct.add(database.getActiveSafetyFeatures());



		temp_data =  database.getModelInfo(xl_pet);

		for(int i=0;i<featuresArrPetrol.size();i++){
			ArrayList<String> tempList = new ArrayList<String>();
			for(int p=0;p<featuresArrPetrol.get(i).size();p++){
				tempList.add(temp_data.get(count));
				count++;
			}
			presenceArr.add(tempList);


		}

		int a = 0;
		for(int i=0;i<featuresArrPetrol.size();i++){

			for(int p=0;p<featuresArrPetrol.get(i).size();p++){
				if(p==0){
					startValuePetrol.add(a);
				}
				a++;
			}
		}

		a = 0;
		for(int i=0;i<featuresArrAct.size();i++){

			for(int p=0;p<featuresArrAct.get(i).size();p++){
				if(p==0){
					startValueActive.add(a);
				}
				a++;
			}
		}

		for(int i=0;i<startValuePetrol.size();i++){
			//Toast.makeText(context, "First value "+startValuePetrol.get(i), 0).show();
		}
		petrolInfoArr.add(database.getModelInfo(xl_pet));
		petrolInfoArr.add(database.getModelInfo(xl_0_pet));
		petrolInfoArr.add(database.getModelInfo(xv_cvt));

		dieselInfoArr.add(database.getModelInfo("XE_Dsl"));
		dieselInfoArr.add(database.getModelInfo(xl_dsl));
		dieselInfoArr.add(database.getModelInfo(xl_0_dsl));
		dieselInfoArr.add(database.getModelInfo(xv_dsl));
		dieselInfoArr.add(database.getModelInfo(xv_pre_dsl));

		activeInfoArr.add(database.getActiveModelInfo(active_e0));
		activeInfoArr.add(database.getActiveModelInfo(active_e1));
		activeInfoArr.add(database.getActiveModelInfo(active_e2));
		activeInfoArr.add(database.getActiveModelInfo("XV_S"));

		//Toast.makeText(context, "Size "+dieselInfoArr.size(), 0).show();
		/*for(int i=0;i<featuresArr.size();i++){
			ArrayList<String> tempList = new ArrayList<String>();
			for(int p=0;p<petrolInfoArr.size();p++){
				tempList.add(petrolInfoArr.get(i).get(p));
			}
		}*/

		setTextData();
		printData();

		setUniquePetrolData();
		setUniqueDieselData();
		setUniqueActiveData();
		setUniqueStart();

		bttn_diff.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				counter++;

				if(counter%2==0){

					
					bttn_diff.setText("Show Differences");
				}
				else{

					
					bttn_diff.setText("Hide Differences");
				}

				if(asDifference){
					asDifference = false;
				}else{
					asDifference = true;
				}
				if(isModelPetrol){	
					if(HIDE_SIMILAR){
						HIDE_SIMILAR = false;
						bttn_similar.setText("Hide Similarities");

					}else{
						/*adapter_petrol 	= new PetrolPagerAdapter(getSupportFragmentManager());
						pager_petrol.setAdapter(adapter_petrol);
						//pager_petrol.setCurrentItem(petrolIndex);
						setVisible(pager_petrol);*/
					}
					adapter_petrol 	= new PetrolPagerAdapter(getSupportFragmentManager());
					pager_petrol.setAdapter(adapter_petrol);
					//pager_petrol.setCurrentItem(petrolIndex);
					setVisible(pager_petrol);


				}else if(isModelActive){



					if(HIDE_SIMILAR){
						HIDE_SIMILAR = false;
						bttn_similar.setText("Hide Similarities");
					}else{
						/*adapter_active 	= new ActivePagerAdapter(getSupportFragmentManager());
						pager_active.setAdapter(adapter_active);
						tabs_active.setViewPager(pager_active);
						setVisible(pager_active);*/
					}
					adapter_active 	= new ActivePagerAdapter(getSupportFragmentManager());
					pager_active.setAdapter(adapter_active);
					tabs_active.setViewPager(pager_active);
					setVisible(pager_active);

				}else{

					if(HIDE_SIMILAR){
						HIDE_SIMILAR = false;
						bttn_similar.setText("Hide Similarities");
					}else{
						/*	adapter_diesel 	= new DieselPagerAdapter(getSupportFragmentManager());
						pager_disel.setAdapter(adapter_diesel);
						tabs_diesel.setViewPager(pager_disel);
						setVisible(pager_disel);*/
					}
					adapter_diesel 	= new DieselPagerAdapter(getSupportFragmentManager());
					pager_disel.setAdapter(adapter_diesel);
					tabs_diesel.setViewPager(pager_disel);
					setVisible(pager_disel);

				}
			}
		});

		bttn_similar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				asDifference = false;
				if(HIDE_SIMILAR){
					HIDE_SIMILAR = false;
					bttn_similar.setText("Hide Similarities");
				}else{
					HIDE_SIMILAR = true;
					bttn_similar.setText("Show Similarities");
				}
				if(isModelPetrol){

					if(HIDE_SIMILAR){				

						setVisible(unique_pager_petrol);
						tabs_petrol.setViewPager(unique_pager_petrol);

					}else{
						setVisible(pager_petrol);
						tabs_petrol.setViewPager(pager_petrol);
					}
				}else if(isModelActive){

					if(HIDE_SIMILAR){
						setVisible(unique_pager_active);
						tabs_active.setViewPager(unique_pager_active);
					}else{
						setVisible(pager_active);
						tabs_active.setViewPager(pager_active);
					}
				}else{
					if(HIDE_SIMILAR){		
						setVisible(unique_pager_disel);
						tabs_diesel.setViewPager(unique_pager_disel);
					}else{
						setVisible(pager_disel);
						tabs_diesel.setViewPager(pager_disel);
					}
				}
			}
		});

	}

	public void printData(){


		int q = 0;
		for(int i=0;i<petrolInfoArr.size();i++){

			for(int p=0;p<petrolInfoArr.get(i).size();p++){
				String a = petrolInfoArr.get(0).get(p);
				String b = petrolInfoArr.get(1).get(p);
				String c = petrolInfoArr.get(2).get(p);
				q++;
				Log.d("====", "== "+q+" ----- "+ a + " -- " + b+ " -- "+ c);
			}
		}
	}

	public void setTextData(){
		if(isModelPetrol){
			text_1.setText(petrol_xl);
			text_2.setText(petrol_xl_0);
			text_3.setText(petrol_xv);
			text_4.setVisibility(View.GONE);
			text_5.setVisibility(View.GONE);
		}else if(isModelActive){
			text_1.setText(active_e0);
			text_2.setText(active_e1);
			text_3.setText(active_e2);
			text_4.setText(active_e2_safe);
			text_4.setVisibility(View.VISIBLE);
			text_5.setVisibility(View.GONE);
		}else{
			text_1.setText("XE");
			text_2.setText(diesel_xl);
			text_3.setText(diesel_xl_0);
			text_4.setText(diesel_xv);
			text_5.setText(diesel_xv_pre);
			text_4.setVisibility(View.VISIBLE);
			text_5.setVisibility(View.VISIBLE);
		}
	}

	public void setUniquePetrolData(){



		uniqueFeutresArrPetrol.add(database.getUniqueExternalPetrolFeatures());
		uniqueFeutresArrPetrol.add(database.getUniqueComfortPetrolFeatures());
		uniqueFeutresArrPetrol.add(database.getUniqueInteriorsPetrolFeatures());
		uniqueFeutresArrPetrol.add(database.getUniqueSafetyPetrolFeatures());

		uniquePetrolArr.add(database.getUniquePetrolValues().get(0));
		uniquePetrolArr.add(database.getUniquePetrolValues().get(1));
		uniquePetrolArr.add(database.getUniquePetrolValues().get(2));




	}

	public void setUniqueDieselData(){




		uniqueFeutresArrDiesel.add(database.getUniqueExternaDieselFeatures());
		uniqueFeutresArrDiesel.add(database.getUniqueComfortDieselFeatures());
		uniqueFeutresArrDiesel.add(database.getUniqueInteriorsDieselFeatures());
		uniqueFeutresArrDiesel.add(database.getUniqueSafetyDieselFeatures());

		uniqueDieselArr.add(database.getUniqueDieselValues().get(0));
		uniqueDieselArr.add(database.getUniqueDieselValues().get(1));
		uniqueDieselArr.add(database.getUniqueDieselValues().get(2));
		uniqueDieselArr.add(database.getUniqueDieselValues().get(3));
		uniqueDieselArr.add(database.getUniqueDieselValues().get(4));


	}

	public void setUniqueActiveData(){



		uniqueFeaturesArrAct.add(database.getUniqueActiveExternalFeatures());
		uniqueFeaturesArrAct.add(database.getUniqueActiveComfortFeatures());
		uniqueFeaturesArrAct.add(database.getUniqueActiveSafetyFeatures());

		uniqueActiveArr.add(database.getUniqueActiveValues().get(0));
		uniqueActiveArr.add(database.getUniqueActiveValues().get(1));
		uniqueActiveArr.add(database.getUniqueActiveValues().get(2));
		uniqueActiveArr.add(database.getUniqueActiveValues().get(3));

	}

	public void setUniqueStart(){
		int a = 0;
		for(int i=0;i<uniqueFeutresArrPetrol.size();i++){

			for(int p=0;p<uniqueFeutresArrPetrol.get(i).size();p++){
				if(p==0){
					uniqueStartValPetrol.add(a);
				}
				a++;
			}
		}

		a = 0;
		for(int i=0;i<uniqueFeutresArrDiesel.size();i++){

			for(int p=0;p<uniqueFeutresArrDiesel.get(i).size();p++){
				if(p==0){
					uniqueStartValDiesel.add(a);
					Log.d("Micra", "Start val "+a);
				}
				a++;
			}
		}

		
		a = 0;
		for(int i=0;i<uniqueFeaturesArrAct.size();i++){

			for(int p=0;p<uniqueFeaturesArrAct.get(i).size();p++){
				if(p==0){
					uniqueStartValActive.add(a);
				}
				a++;
			}
		}
	}


	public void setVisible(ViewPager pager){
		if(pager == unique_pager_petrol){
			unique_pager_petrol.setVisibility(View.VISIBLE);
			unique_pager_petrol.setAdapter(new UniquePetrolPagerAdapter(getSupportFragmentManager()));

			unique_pager_disel.setVisibility(View.GONE);
			pager_petrol.setVisibility(View.GONE);
			pager_disel.setVisibility(View.GONE);
			pager_active.setVisibility(View.GONE);
			unique_pager_active.setVisibility(View.GONE);

			tabs_petrol.setViewPager(unique_pager_petrol);

		}else if(pager == unique_pager_disel){
			unique_pager_disel.setVisibility(View.VISIBLE);
			unique_pager_disel.setAdapter(new UniqueDieselPagerAdapter(getSupportFragmentManager()));

			unique_pager_petrol.setVisibility(View.GONE);
			pager_petrol.setVisibility(View.GONE);
			pager_disel.setVisibility(View.GONE);
			pager_active.setVisibility(View.GONE);
			unique_pager_active.setVisibility(View.GONE);

			tabs_diesel.setViewPager(unique_pager_disel);


		}else if(pager == pager_petrol){
			pager_petrol.setVisibility(View.VISIBLE);
			pager_petrol.setAdapter(new PetrolPagerAdapter(getSupportFragmentManager()));

			unique_pager_disel.setVisibility(View.GONE);
			unique_pager_petrol.setVisibility(View.GONE);
			pager_disel.setVisibility(View.GONE);
			pager_active.setVisibility(View.GONE);
			unique_pager_active.setVisibility(View.GONE);

			tabs_petrol.setViewPager(pager_petrol);

		}else if(pager == pager_disel){
			pager_disel.setVisibility(View.VISIBLE);
			pager_disel.setAdapter(new DieselPagerAdapter(getSupportFragmentManager()));

			pager_petrol.setVisibility(View.GONE);
			unique_pager_disel.setVisibility(View.GONE);
			unique_pager_petrol.setVisibility(View.GONE);
			pager_active.setVisibility(View.GONE);
			unique_pager_active.setVisibility(View.GONE);

			tabs_diesel.setViewPager(pager_disel);

		}else if(pager == pager_active){
			pager_active.setVisibility(View.VISIBLE);
			pager_active.setAdapter(new ActivePagerAdapter(getSupportFragmentManager()));

			pager_disel.setVisibility(View.GONE);
			pager_petrol.setVisibility(View.GONE);
			unique_pager_disel.setVisibility(View.GONE);
			unique_pager_petrol.setVisibility(View.GONE);
			unique_pager_active.setVisibility(View.GONE);

			tabs_active.setViewPager(pager_active);

		}else if(pager == unique_pager_active){
			unique_pager_active.setVisibility(View.VISIBLE);
			unique_pager_active.setAdapter(new UniqueActivePagerAdapter(getSupportFragmentManager()));

			pager_disel.setVisibility(View.GONE);
			pager_petrol.setVisibility(View.GONE);
			unique_pager_disel.setVisibility(View.GONE);
			unique_pager_petrol.setVisibility(View.GONE);
			pager_active.setVisibility(View.GONE);

			tabs_active.setViewPager(unique_pager_active);
		}

	} 
	public class PetrolPagerAdapter extends FragmentPagerAdapter {

		MicraFeaturesFragment fragment;
		private final String[] TITLES_PETROL_DIESEL = { "Exterior", "Comfort and Convenience", "Interiors", "Safety" };
		private final String[] TITLES_ACTIVE = { "Exterior", "Comfort and Convenience", "Safety" };

		public PetrolPagerAdapter(FragmentManager fm) {
			super(fm);

		}

		@Override
		public CharSequence getPageTitle(int position) {

			return TITLES_PETROL_DIESEL[position];
		}

		@Override
		public int getCount() {

			return TITLES_PETROL_DIESEL.length;
		}

		@Override
		public Fragment getItem(int position) {
			petrolIndex = position;

			/*if(!HIDE_SIMILAR){
				Toast.makeText(context, "new full", 0).show();
				fragment = new MicraFeaturesFragment(actContext,context, featuresArrPetrol.get(position),petrolInfoArr,startValuePetrol.get(position));
			}else{
				Toast.makeText(context, "new unique", Toast.LENGTH_SHORT).show();
				fragment = new MicraFeaturesFragment(actContext,context, uniqueFeutresArrPetrol.get(position),uniquePetrolArr,uniqueStartValPetrol.get(position));

			}*/
			fragment = new MicraFeaturesFragment(actContext,context, featuresArrPetrol.get(position),petrolInfoArr,startValuePetrol.get(position));

			return fragment;


		}






	}


	public class UniquePetrolPagerAdapter extends FragmentPagerAdapter {

		MicraFeaturesFragment fragment;
		private final String[] TITLES_PETROL_DIESEL = { "Exterior", "Comfort and Convenience", "Interiors", "Safety" };
		private final String[] TITLES_ACTIVE = { "Exterior", "Comfort and Convenience", "Safety" };

		public UniquePetrolPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {

			return TITLES_PETROL_DIESEL[position];
		}

		@Override
		public int getCount() {

			return TITLES_PETROL_DIESEL.length;
		}

		@Override
		public Fragment getItem(int position) {
			petrolIndex = position;

			/*if(!HIDE_SIMILAR){
				Toast.makeText(context, "new full", 0).show();
				fragment = new MicraFeaturesFragment(actContext,context, featuresArrPetrol.get(position),petrolInfoArr,startValuePetrol.get(position));
			}else{
				Toast.makeText(context, "new unique", Toast.LENGTH_SHORT).show();
				fragment = new MicraFeaturesFragment(actContext,context, uniqueFeutresArrPetrol.get(position),uniquePetrolArr,uniqueStartValPetrol.get(position));

			}*/
			fragment = new MicraFeaturesFragment(actContext,context, uniqueFeutresArrPetrol.get(position),uniquePetrolArr,uniqueStartValPetrol.get(position));
			return fragment;


		}






	}


	public class DieselPagerAdapter extends FragmentPagerAdapter {

		MicraFeaturesFragment fragment;

		private final String[] TITLES_PETROL_DIESEL = { "Exterior", "Comfort and Convenience", "Interiors", "Safety" };
		//private final String[] TITLES_ACTIVE = { "Exterior", "Comfort and Convenience", "Safety" };

		public DieselPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {


			return TITLES_PETROL_DIESEL[position];
		}

		@Override
		public int getCount() {

			return TITLES_PETROL_DIESEL.length;
		}

		@Override
		public Fragment getItem(int position) {
			dieselIndex = position;
			/*if(!HIDE_SIMILAR){
				fragment = new MicraFeaturesFragment(actContext,context, featuresArrDiesel.get(position),dieselInfoArr,startValuePetrol.get(position));
			}else{
				fragment = new MicraFeaturesFragment(actContext,context, uniqueFeutresArrDiesel.get(position),uniqueDieselArr,uniqueStartValDiesel.get(position));
			}*/
			fragment = new MicraFeaturesFragment(actContext,context, featuresArrDiesel.get(position),dieselInfoArr,startValuePetrol.get(position));
			return fragment;

		}

	}

	public class UniqueDieselPagerAdapter extends FragmentPagerAdapter {

		MicraFeaturesFragment fragment;

		private final String[] TITLES_PETROL_DIESEL = { "Exterior", "Comfort and Convenience", "Interiors", "Safety" };
		//private final String[] TITLES_ACTIVE = { "Exterior", "Comfort and Convenience", "Safety" };

		public UniqueDieselPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {


			return TITLES_PETROL_DIESEL[position];
		}

		@Override
		public int getCount() {

			return TITLES_PETROL_DIESEL.length;
		}

		@Override
		public Fragment getItem(int position) {
			dieselIndex = position;
			/*if(!HIDE_SIMILAR){
				fragment = new MicraFeaturesFragment(actContext,context, featuresArrDiesel.get(position),dieselInfoArr,startValuePetrol.get(position));
			}else{
				fragment = new MicraFeaturesFragment(actContext,context, uniqueFeutresArrDiesel.get(position),uniqueDieselArr,uniqueStartValDiesel.get(position));
			}*/

			fragment = new MicraFeaturesFragment(actContext,context, uniqueFeutresArrDiesel.get(position),uniqueDieselArr,uniqueStartValDiesel.get(position));
			return fragment;

		}

	}


	public class ActivePagerAdapter extends FragmentPagerAdapter {
		MicraFeaturesFragment fragment;

		//private final String[] TITLES_PETROL_DIESEL = { "Exterior", "Comfort and Convenience", "Interiors", "Safety" };
		private final String[] TITLES_ACTIVE = { "Exterior", "Comfort and Convenience", "Safety" };

		public ActivePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {

			return TITLES_ACTIVE[position];
		}

		@Override
		public int getCount() {

			return TITLES_ACTIVE.length;
		}

		@Override
		public Fragment getItem(int position) {
			activeIndex = position;

			/*if(!HIDE_SIMILAR){
				fragment = new MicraFeaturesFragment(actContext,context, featuresArrAct.get(position),activeInfoArr,startValueActive.get(position));
			}else{
				fragment = new MicraFeaturesFragment(actContext,context, uniqueFeaturesArrAct.get(position),uniqueActiveArr,uniqueStartValActive.get(position));
			}*/
			fragment = new MicraFeaturesFragment(actContext,context, featuresArrAct.get(position),activeInfoArr,startValueActive.get(position));
			return fragment;


		}


	}


	public class UniqueActivePagerAdapter extends FragmentPagerAdapter {
		MicraFeaturesFragment fragment;

		//private final String[] TITLES_PETROL_DIESEL = { "Exterior", "Comfort and Convenience", "Interiors", "Safety" };
		private final String[] TITLES_ACTIVE = { "Exterior", "Comfort and Convenience", "Safety" };

		public UniqueActivePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {

			return TITLES_ACTIVE[position];
		}

		@Override
		public int getCount() {

			return TITLES_ACTIVE.length;
		}

		@Override
		public Fragment getItem(int position) {
			activeIndex = position;

			/*if(!HIDE_SIMILAR){
				fragment = new MicraFeaturesFragment(actContext,context, featuresArrAct.get(position),activeInfoArr,startValueActive.get(position));
			}else{
				fragment = new MicraFeaturesFragment(actContext,context, uniqueFeaturesArrAct.get(position),uniqueActiveArr,uniqueStartValActive.get(position));
			}*/
			fragment = new MicraFeaturesFragment(actContext,context, uniqueFeaturesArrAct.get(position),uniqueActiveArr,uniqueStartValActive.get(position));
			return fragment;


		}


	}


	public void setVisible(String tempType){
		if(tempType.equalsIgnoreCase(model_type_petrol)){

			tabs_petrol.setVisibility(View.VISIBLE);
			pager_petrol.setVisibility(View.VISIBLE);

			tabs_diesel.setVisibility(View.GONE);
			pager_disel.setVisibility(View.GONE);

			tabs_active.setVisibility(View.GONE);
			pager_active.setVisibility(View.GONE);

		}else if(tempType.equalsIgnoreCase(model_type_diesel)){

			tabs_petrol.setVisibility(View.INVISIBLE);
			pager_petrol.setVisibility(View.GONE);

			tabs_diesel.setVisibility(View.VISIBLE);
			pager_disel.setVisibility(View.VISIBLE);

			tabs_active.setVisibility(View.GONE);
			pager_active.setVisibility(View.GONE);

		}else if(tempType.equalsIgnoreCase(model_type_active)){

			tabs_petrol.setVisibility(View.INVISIBLE);
			pager_petrol.setVisibility(View.GONE);

			tabs_diesel.setVisibility(View.GONE);
			pager_disel.setVisibility(View.GONE);

			tabs_active.setVisibility(View.VISIBLE);
			pager_active.setVisibility(View.VISIBLE);

		}
	}


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub


		/*	SubMenu subMenu2 = menu.addSubMenu(" "+tempModel+"");
		subMenu2.add(model_type_petrol);
		subMenu2.add(model_type_diesel);
		subMenu2.add(model_type_active);*/

		SubMenu subMenu1 = menu.addSubMenu("Menu");
		subMenu1.add("Home").setIcon(R.drawable.home);
		if(selfTabVisible){
			subMenu1.add("Features").setIcon(R.drawable.fetures_tick);
		}
		subMenu1.add("Technical Specification").setIcon(R.drawable.tech_spec);
		subMenu1.add("Price List").setIcon(R.drawable.price);
		subMenu1.add("Test Drive").setIcon(R.drawable.test_drive_new);
		subMenu1.add("Gallery").setIcon(R.drawable.gallery);
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);

		/*MenuItem menu_car_type = subMenu3.getItem();
		menu_car_type.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);*/

		/*	MenuItem menu_model = subMenu2.getItem();
		menu_model.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);*/

		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);


		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub


		if(item.getTitle().toString().equalsIgnoreCase(model_type_diesel)){
			
			isModelPetrol = false;
			isModelActive = false;
			HIDE_SIMILAR = false;
			
			setTextData();
			setVisible(pager_disel);
			bttn_similar.setText("Hide Similarities");

			tempModel = model_type_diesel;
			if(useFlurry){
				articleParams.put("Model Type", tempModel);
				FlurryAgent.logEvent("Features_Tab_Event",articleParams);
			}
			getSherlock().dispatchInvalidateOptionsMenu();
			//adapter.notifyDataSetChanged();


			setVisible(tempModel);

		}
		if(item.getTitle().toString().equalsIgnoreCase(model_type_petrol)){
			
			isModelPetrol = true;
			isModelActive = false;
			HIDE_SIMILAR = false;
			
			setTextData();
			bttn_similar.setText("Hide Similarities");
			setVisible(pager_petrol);
			tempModel = model_type_petrol;
			if(useFlurry){
				articleParams.put("Model Type", tempModel);
				FlurryAgent.logEvent("Features_Tab_Event",articleParams);
			}
			getSherlock().dispatchInvalidateOptionsMenu();
			//adapter.notifyDataSetChanged();

			setVisible(tempModel);
		}
		if(item.getTitle().toString().equalsIgnoreCase(model_type_active)){
			
			isModelActive = true;
			isModelPetrol = false;
			HIDE_SIMILAR = false;
			
			setTextData();
			bttn_similar.setText("Hide Similarities");
			setVisible(pager_active);
			tempModel = model_type_active;
			if(useFlurry){
				articleParams.put("Model Type", tempModel);
				FlurryAgent.logEvent("Features_Tab_Event",articleParams);
			}
			getSherlock().dispatchInvalidateOptionsMenu();
			//adapter.notifyDataSetChanged();


			setVisible(tempModel);
		}
		if(item.getTitle().toString().equalsIgnoreCase("Home")){
			
			isModelPetrol = true;
			isModelActive = false;
			asDifference = false;
			HIDE_SIMILAR = false;
			
			//adapter = null;
			share.setAppClose(false);
			Intent intent=new Intent(this,Home_new.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Contact Us")){
			
			isModelPetrol = true;
			isModelActive = false;
			asDifference = false;
			HIDE_SIMILAR = false;
			
			//adapter = null;
			Intent intent=new Intent(this,ContactUs.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}

		if(item.getTitle().toString().equalsIgnoreCase("Technical Specification")){
			
			isModelPetrol = true;
			isModelActive = false;
			asDifference = false;
			HIDE_SIMILAR = false;
			
			share.setAppClose(false);
			Intent intent=new Intent(this,TechnicalSpecification.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Price List")){
			
			isModelPetrol = true;
			isModelActive = false;
			asDifference = false;
			HIDE_SIMILAR = false;
			
			//share.setAppClose(false);
			Intent intent=new Intent(this,Price_list_new.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Test Drive")){
			
			isModelPetrol = true;
			isModelActive = false;
			asDifference = false;
			HIDE_SIMILAR = false;
			
			//share.setAppClose(false);
			Intent intent=new Intent(this,TestDrive.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		
		if(item.getTitle().toString().equalsIgnoreCase("Gallery")){
			//share.setAppClose(false);
			
			isModelPetrol = true;
			isModelActive = false;
			asDifference = false;
			HIDE_SIMILAR = false;
			
			FlurryAgent.logEvent("Gallery_Tab_Event");
			Intent intent=new Intent(this,NissanCarGallery.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}


		/*if(item.getTitle().toString().equalsIgnoreCase("Price List New")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Price_list_new.class);
			startActivity(intent);
			finish();
		}*/

		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		share.setAppClose(true);
		this.finish();
	}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(this, getResources().getString(R.string.flurry_id));
			if(useFlurry){
				articleParams.put("Model Type", model_type_petrol);
				FlurryAgent.logEvent("Features_Tab_Event",articleParams);
			}
		}

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(this);
		}
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub

		if(model_list.getItem(itemPosition).toString().equalsIgnoreCase(model_type_diesel)){
			isModelPetrol = false;
			isModelActive = false;
			HIDE_SIMILAR = false;
			setTextData();
			setVisible(pager_disel);
			bttn_similar.setText("Hide Similarities");

			tempModel = model_type_diesel;
			if(useFlurry){
				articleParams.put("Model Type", tempModel);
				FlurryAgent.logEvent("Features_Tab_Event",articleParams);
			}
			getSherlock().dispatchInvalidateOptionsMenu();
			//adapter.notifyDataSetChanged();


			setVisible(tempModel);

		}
		if(model_list.getItem(itemPosition).toString().equalsIgnoreCase(model_type_petrol)){
			isModelPetrol = true;
			isModelActive = false;
			HIDE_SIMILAR = false;
			setTextData();
			bttn_similar.setText("Hide Similarities");
			setVisible(pager_petrol);
			tempModel = model_type_petrol;
			if(useFlurry){
				articleParams.put("Model Type", tempModel);
				FlurryAgent.logEvent("Features_Tab_Event",articleParams);
			}
			getSherlock().dispatchInvalidateOptionsMenu();
			//adapter.notifyDataSetChanged();

			setVisible(tempModel);
		}
		if(model_list.getItem(itemPosition).toString().equalsIgnoreCase(model_type_active)){
			isModelActive = true;
			isModelPetrol = false;
			HIDE_SIMILAR = false;
			setTextData();
			bttn_similar.setText("Hide Similarities");
			setVisible(pager_active);
			tempModel = model_type_active;
			if(useFlurry){
				articleParams.put("Model Type", tempModel);
				FlurryAgent.logEvent("Features_Tab_Event",articleParams);
			}
			getSherlock().dispatchInvalidateOptionsMenu();
			//adapter.notifyDataSetChanged();


			setVisible(tempModel);
		}
		return false;
	}


}
