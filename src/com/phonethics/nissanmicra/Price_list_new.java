package com.phonethics.nissanmicra;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.InputSource;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.flurry.android.FlurryAgent;
import com.phonethics.adapters.PriceListAdapter;
import com.phonethics.adapters.StateCityListAdapter;
import com.phonethics.database.MicraDataBase;
import com.phonethics.network.NetworkCheck;

import com.phonethics.nissanmicra.PriceList.ADD_VALUES_TO_DATABASE;
import com.phonethics.nissanmicra.PriceList.FileFromURL;
import com.phonethics.pref.Share;
import com.phonethics.services.DownloadReceiver;
import com.phonethics.services.DownloadService;
import com.phonethics.services.DownloadReceiver.DownloadResult;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract.Contacts.Data;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Price_list_new extends SherlockActivity implements DownloadResult, OnNavigationListener{


	Context								context;
	Activity							actContext;
	TextView							text_state,text_city,text_price_info,text_modelText,text_priceText;
	ListView							listView;
	ListView							list_state,list_city;
	RelativeLayout						root_layout,relative_list_laLayout;
	DownloadReceiver					receiver;
	DialogThread 						thread;
	FileInputStream						filename;
	Share								sharePref;
	MicraDataBase						database;
	public static final int 			progress_bar_type = 0; 
	private ProgressDialog 				pDialog;
	String 								//xmlurl	= "http://stage.phonethics.in/proj/neon/micra_pricelist.php",
	xmlurl	= "http://stage.phonethics.in/proj/neon/micra_newpricelist.php",
	TEMP_STATE="",TEMP_CITY="";
	String								xml,
	//NEONXML ="/sdcard/neoncache/pricelist.xml";
	NEONXML ="/sdcard/neoncache/price3list_new.txt";
	boolean								SERVICE_CALLED_ONCE = false,SERVICE_COMPLETED,OPEN_FIRST_TIME,VALUE_ADDED = false,STATE_CLICKED = true;
	String								model = "Petrol";

	ArrayList<String> 					statesArr,cityArr,uniqueStateArr,uniqueCityArr,coulmnArr;

	ActionBar 							ac;
	NetworkCheck						netAvailable;
	FileFromURL 						downloadFile;
	Typeface							font;
	Map<String, String> 				articleParams;
	boolean								useFlurry;
	boolean								internetPresent = true, XE_PRESENT;
	String								app_color="";
	ArrayAdapter<CharSequence> model_list;

	TextView ext_price_footer1;

	ArrayList<String> galallLinks, galallDescription, galallCaption, galallVLinks, galallNames;

	Bundle bundle;
	boolean			update_build = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_price_list_new);



		context 		= this;
		actContext		= this;

		useFlurry			= Boolean.parseBoolean(getResources().getString(R.string.useflurry));

		font 		= Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf");
		LayoutInflater inflater 		= LayoutInflater.from(this);
		View customView 				= inflater.inflate(R.layout.action_bar_title_text, null);
		TextView textTitle				= (TextView) customView.findViewById(R.id.text_action_title);

		textTitle.setText("Price List");
		textTitle.setTypeface(font);

		ac 				= getSupportActionBar();
		ac.setCustomView(customView);
		ac.setDisplayShowTitleEnabled(true);
		ac.setDisplayShowCustomEnabled(false);
		ac.setTitle("Price List");

		ac.show();

		Context acontext = getSupportActionBar().getThemedContext();
		model_list = ArrayAdapter.createFromResource(acontext, R.array.mdoel_type, R.layout.list_menus);
		model_list.setDropDownViewResource(R.layout.list_dropdown_item);

		ext_price_footer1 = (TextView) findViewById(R.id.text_price_footer1);
		ext_price_footer1.setTypeface(font);

		//Setting Navigation Type.

		ac.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		ac.setListNavigationCallbacks(model_list, this);


		// Getting gallery data

		bundle = getIntent().getExtras();
		if(bundle!=null){

			galallLinks = bundle.getStringArrayList("imgArr");
			galallDescription	= bundle.getStringArrayList("imgDesc");
			galallCaption = bundle.getStringArrayList("imgCaption");
			galallVLinks = bundle.getStringArrayList("vlinksArr");
			galallNames = bundle.getStringArrayList("allNames");
			//Toast.makeText(context, ""+imgLinks.size(), 0).show();
		}



		//Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		app_color = getResources().getString(R.string.app_color);
		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#"+app_color));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });
		ac.setBackgroundDrawable(ld);

		context					= this;
		
		actContext				= this;
		articleParams			= new HashMap<String, String>();
		text_state				= (TextView) findViewById(R.id.text_state);
		text_city				= (TextView) findViewById(R.id.text_city);
		text_modelText			= (TextView) findViewById(R.id.text_model);
		text_priceText			= (TextView) findViewById(R.id.text_price);
		text_price_info			= (TextView) findViewById(R.id.text_price_footer);
		text_state.setTypeface(font);
		text_city.setTypeface(font);
		text_modelText.setTypeface(font);
		text_priceText.setTypeface(font);
		text_price_info.setTypeface(font);

		listView				= (ListView) findViewById(R.id.list_price);
		list_state				= (ListView) findViewById(R.id.list_state);
		list_city				= (ListView) findViewById(R.id.list_city);

		root_layout				= (RelativeLayout) findViewById(R.id.layout_root);
		relative_list_laLayout	= (RelativeLayout) findViewById(R.id.relative_list_layout);
		root_layout.setVisibility(View.VISIBLE);

		View footerView		= ((LayoutInflater) actContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_view, null, false);
		TextView textView	= (TextView) footerView.findViewById(R.id.text_list_footer);
		textView.setText(getResources().getString(R.string.text_price_condition));
		textView.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf"));
		textView.setTextSize(15);
		//listView.addFooterView(footerView);

		statesArr				= new ArrayList<String>();
		cityArr					= new ArrayList<String>();
		uniqueStateArr			= new ArrayList<String>();
		uniqueCityArr			= new ArrayList<String>();
		coulmnArr				= new ArrayList<String>();

		sharePref 				= new Share(context);
		database				= new MicraDataBase(context);
		netAvailable 			= new NetworkCheck(context);
		receiver				= new DownloadReceiver(new Handler());
		receiver.setReceiver(this);
		XE_PRESENT				= sharePref.geXEDSL();
		
		/*if(sharePref.getUpdateBuild()){
			
		}else{
			sharePref.setUpdateBuild(true);
		}
		*/
		if(savedInstanceState!=null){
			TEMP_STATE = savedInstanceState.getString("TEMP_STATE");
			TEMP_CITY = savedInstanceState.getString("TEMP_CITY");
			model  = savedInstanceState.getString("model");

			setListAdapter(TEMP_STATE, TEMP_CITY);
		}else{
			if(isFilePresent()){
				if(isDaysPassed()){
					if(isInternetPresent()){
						internetPresent = true;
						downloadFile();

					}else{
						startParsing();
						//downloadFile();
					}

				}else{
					XE_PRESENT = true;
					startParsing();
					//downloadFile();
				}


			}else{
				if(isInternetPresent()){
					internetPresent = true;
					downloadFile();

				}else{
					Toast.makeText(context, "No Internet Connection", 0).show();
					internetPresent = false;
				}
			}
		}




		text_state.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				relative_list_laLayout.setVisibility(View.VISIBLE);
				if(list_city.isShown()){
					list_city.setVisibility(View.GONE);
				}else{
					list_state.setVisibility(View.VISIBLE);
				}
				STATE_CLICKED = true;


			}
		});

		text_city.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				relative_list_laLayout.setVisibility(View.VISIBLE);
				if(list_state.isShown()){
					list_state.setVisibility(View.GONE);
				}else{
					list_city.setVisibility(View.VISIBLE);
				}
				STATE_CLICKED = false;
			}
		});


		relative_list_laLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				list_state.setVisibility(View.GONE);
				list_city.setVisibility(View.GONE);
				relative_list_laLayout.setVisibility(View.GONE);
			}
		});

		list_state.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub

				if(STATE_CLICKED){
					uniqueCityArr.clear();
					uniqueCityArr			= getCitysOfStates(uniqueStateArr.get(pos));
					StateCityListAdapter	city_adapter 			= new StateCityListAdapter(actContext, uniqueCityArr);

					//list_city.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, uniqueCityArr));
					list_city.setAdapter(city_adapter);

					TEMP_STATE 	= uniqueStateArr.get(pos);
					TEMP_CITY 	= uniqueCityArr.get(0);
					setListAdapter(TEMP_STATE,TEMP_CITY);

					text_state.setText(TEMP_STATE);
					text_city.setText(TEMP_CITY);

					ext_price_footer1.setText("* All prices shown are ex-showroom " + TEMP_CITY);

				}
				list_state.setVisibility(View.GONE);
				relative_list_laLayout.setVisibility(View.GONE);


			}
		});

		list_city.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				text_city.setText(uniqueCityArr.get(pos));
				TEMP_CITY = uniqueCityArr.get(pos);
				setListAdapter(TEMP_STATE,TEMP_CITY);

				list_city.setVisibility(View.GONE);
				relative_list_laLayout.setVisibility(View.GONE);

				ext_price_footer1.setText("* All prices shown are ex-showroom " + uniqueCityArr.get(pos));
			}
		});



	}


	public boolean isInternetPresent(){
		return netAvailable.isNetworkAvailable();
	}


	public void downloadFile(){
		downloadFile	=	new FileFromURL();
		downloadFile.execute(xmlurl);
	}


	public boolean isFilePresent(){

		File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
		if(!myDirectory.exists()) {                                 
			myDirectory.mkdirs();
		}
		File xmlFile1	=	new File(NEONXML);
		if(xmlFile1.exists()){

			return true;
		}
		return false;
	}


	public boolean isDaysPassed(){
		File xmlFile1		=	new File(NEONXML);
		Date lastModDate 	= new Date(xmlFile1.lastModified());
		Date todayDate 		= new Date();
		long fileDate 		= lastModDate.getTime();
		long currentDate 	= todayDate.getTime();
		long dateDiff 		= currentDate - fileDate;
		long diffDays 		= dateDiff / (24 * 60 * 60 * 1000);
		if(diffDays > 1){
			return true;
		}else{
			return false;	
		}




	}

	public void startService(){
		if(!SERVICE_CALLED_ONCE){
			//Toast.makeText(context, "service", 0).show();
			//xmlurl = "http://stage.phonethics.in/proj/neon/neon_gallery1.php";
			SERVICE_CALLED_ONCE = true;
			Intent intent2=new Intent(context, DownloadService.class);
			intent2.putExtra("downloadFile",receiver);
			intent2.putExtra("URL", xmlurl);
			intent2.putExtra("FILENAME", NEONXML);
			context.startService(intent2);
		}
	}

	private class DialogThread extends Thread {
		/*ProgressDialog mProgressDialog = new ProgressDialog(context);*/
		public Handler mHandler;

		@Override
		/**
		 * Sleep for some time and than start new activity.
		 */

		public void run() {
			//Looper.prepare();

			try {
				Thread.sleep(3*1000);
			} catch (Exception e) {
				e.printStackTrace();
			}

			//Looper.loop();
			removeDialog(progress_bar_type);
			tempMethod();
		}
	}	



	void tempMethod(){
		try {
			runOnUiThread(new Runnable() {
				public void run() {

					thread 		= null;	
					try {
						filename	=	new FileInputStream(NEONXML);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}


	public void startParsing(){
		coulmnArr.clear();

		coulmnArr.add(getResources().getString(R.string.new_price_id));
		coulmnArr.add(getResources().getString(R.string.new_price_state));
		coulmnArr.add(getResources().getString(R.string.new_price_city));

		coulmnArr.add(getResources().getString(R.string.new_price_pet_xl));
		coulmnArr.add(getResources().getString(R.string.new_price_pet_xl_o));
		coulmnArr.add(getResources().getString(R.string.new_price_pet_xv_cvt));

		coulmnArr.add("DL_XE");
		coulmnArr.add(getResources().getString(R.string.new_price_dl_xl));
		coulmnArr.add(getResources().getString(R.string.new_price_dl_xl_o));
		coulmnArr.add(getResources().getString(R.string.new_price_dl_xv));
		coulmnArr.add(getResources().getString(R.string.new_price_dl_xv_p));

		coulmnArr.add(getResources().getString(R.string.new_price_act_xe));
		coulmnArr.add(getResources().getString(R.string.new_price_act_xl));
		coulmnArr.add(getResources().getString(R.string.new_price_act_xv));
		coulmnArr.add(getResources().getString(R.string.new_price_act_xv_s));



		if(sharePref.isActivityOpenFirstTime()){
			/*	if(true){*/
			sharePref.setActivityOpen(false);
			ADD_VALUES_TO_DATABASE add_Values  = new ADD_VALUES_TO_DATABASE();
			add_Values.execute("");
		}else if(sharePref.getUpdateBuild()){
			ADD_VALUES_TO_DATABASE add_Values  = new ADD_VALUES_TO_DATABASE();
			add_Values.execute("");
			sharePref.setUpdateBuild(false);
		
		}else if(MicraDataBase.newDatabase){
			ADD_VALUES_TO_DATABASE add_Values  = new ADD_VALUES_TO_DATABASE();
			add_Values.execute("");
			MicraDataBase.newDatabase = false;
		}
		else{

			/*ADD_VALUES_TO_DATABASE add_Values  = new ADD_VALUES_TO_DATABASE();
			add_Values.execute("");*/
			
			try{
				root_layout.setVisibility(View.VISIBLE);
				VALUE_ADDED 			= true;
				uniqueStateArr			= getStates();
				uniqueCityArr			= getCitysOfStates(uniqueStateArr.get(0));


				/*StateCityListAdapter state_adapter 	= new StateCityListAdapter(actContext, uniqueStateArr);
			StateCityListAdapter city_adapter 	= new StateCityListAdapter(actContext, uniqueCityArr);

			list_state.setAdapter(state_adapter);
			list_city.setAdapter(city_adapter);*/

				TEMP_STATE = uniqueStateArr.get(0);
				TEMP_CITY = uniqueCityArr.get(0);


				TEMP_STATE 		= uniqueStateArr.get(getStateIndex("Delhi"));
				uniqueCityArr	= getCitysOfStates(TEMP_STATE);
				TEMP_CITY		= uniqueCityArr.get(0);

				text_state.setText(TEMP_STATE);
				text_city.setText(uniqueCityArr.get(0));

				ext_price_footer1.setText("* All prices shown are ex-showroom " + uniqueCityArr.get(0));

				StateCityListAdapter state_adapter 	= new StateCityListAdapter(actContext, uniqueStateArr);
				StateCityListAdapter city_adapter 	= new StateCityListAdapter(actContext, uniqueCityArr);

				list_state.setAdapter(state_adapter);
				list_city.setAdapter(city_adapter);

				setListAdapter(TEMP_STATE,TEMP_CITY);
			}catch(Exception ex){
				ex.printStackTrace();
			}


		}
	}





	public void setListAdapter(String state, String city){

		try{
			ArrayList<String> tempList 	= new ArrayList<String>();
			ArrayList<String> carsArr	= new ArrayList<String>();
			if(useFlurry){
				articleParams.put("State", state);
				articleParams.put("City", city);
				//articleParams.put("Model Type", "Petrol");
			}
			if(model.equalsIgnoreCase("Petrol")){
				tempList = database.getPrices("p", state, city);
				carsArr.add("XL");
				carsArr.add("XL(0)");
				carsArr.add("XL CVT");
				if(useFlurry){
					articleParams.put("Model Type", "Petrol");
					FlurryAgent.logEvent("PriceList_Tab_Event", articleParams);

				}
			}else if(model.equalsIgnoreCase("Diesel")){
				tempList = database.getPrices("d", state, city);
				//carsArr.add("XE");
				carsArr.add("XL");
				carsArr.add("XL(0)");
				carsArr.add("XV");
				carsArr.add("XV(P)");
				if(XE_PRESENT){
					carsArr.add("XE");
				}
				
				//Toast.makeText(context, "XE "+XE_PRESENT, 0).show();
				if(useFlurry){
					articleParams.put("Model Type", "Diesel");
					FlurryAgent.logEvent("PriceList_Tab_Event", articleParams);

				}
			}else{
				tempList = database.getPrices("a", state, city);
				carsArr.add("XL");
				carsArr.add("XV");
				carsArr.add("XE");
				carsArr.add("XV(S)");
				if(useFlurry){
					articleParams.put("Model Type", "Active");
					FlurryAgent.logEvent("PriceList_Tab_Event", articleParams);
				}
			}

			/*if(useFlurry){
				articleParams.put("Model Type", "Petrol");
				FlurryAgent.logEvent("PriceList_Tab_Event", articleParams);
			}*/
			
			for(int i=0;i<tempList.size();i++){
				Log.d("", "Price "+tempList.get(i));
			}

			PriceListAdapter adapter = new PriceListAdapter(actContext, carsArr, tempList);
			listView.setAdapter(adapter);
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}


	public int getStateIndex(String state){

		int i=0;
		//Toast.makeText(context, ""+state, Toast.LENGTH_SHORT).show();
		if(uniqueStateArr.contains(state)){
			//Toast.makeText(context, "Contains "+state, Toast.LENGTH_SHORT).show();
			for(int p=0;p<uniqueStateArr.size();p++){
				i = p;
				if(state.equalsIgnoreCase(uniqueStateArr.get(p).toString())){
					break;
				}
			}
		}
		return i;
	}


	public ArrayList<String> getCitysOfStates(String state){
		ArrayList<String> cityNames  = new ArrayList<String>();

		/*for(int i=0;i<cityArr.size();i++){
			if(state.equalsIgnoreCase(statesArr.get(i))){
				cityNames.add(cityArr.get(i));
			}

		}*/
		cityNames = database.getCitysOfState(state);



		return cityNames;
	}

	public ArrayList<String> getStates(){
		ArrayList<String> stateNames  = new ArrayList<String>();

		/*for(int i=0;i<statesArr.size();i++){
			if(!stateNames.contains(statesArr.get(i))){
				stateNames.add(statesArr.get(i));			
			}	
		}*/

		stateNames  = database.getStates();

		return stateNames;
	}

	class FileFromURL extends AsyncTask<String, String, String> {


		DefaultHttpClient 	httpClient;
		HttpPost 			httpPost;
		HttpResponse		httpRes;
		HttpEntity			httpEnt;


		@Override
		protected String doInBackground(String... xmlur) {
			// TODO Auto-generated method stub
			int count;

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(xmlurl);
				httpRes		=	httpClient.execute(httpPost);
				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	= DocumentBuilderFactory.newInstance();
				DocumentBuilder			db	= dbf.newDocumentBuilder();
				InputSource				is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));

				URL url = new URL(xmlur[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				// getting file length
				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file

				try{
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}

				FileOutputStream output = new FileOutputStream(NEONXML);
				byte data[] = new byte[1024];
				long total = 0;
				while (   ((count = input.read(data)) != -1)  ) {
					total += count;
					output.write(data, 0, count);
				}
				output.flush();
				output.close();
				input.close();

			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");

			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) throws NullPointerException {
			// TODO Auto-generated method stub
			try {
				removeDialog(progress_bar_type);
				startParsing();

			} catch(Exception ex){
			}
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showDialog(progress_bar_type);

		}
	}



	class ADD_VALUES_TO_DATABASE extends AsyncTask<String, String, String> {





		@Override
		protected String doInBackground(String... xmlur) {


			/*database.deleteAllRow();

			database.addDetails("Id", idArr,new ArrayList<String>());
			database.addDetails(getResources().getString(R.string.price_state), statesArr,idArr);
			database.addDetails(getResources().getString(R.string.price_city), cityArr,idArr);

			database.addDetails(getResources().getString(R.string.price_xe), xe_Arr,idArr);
			database.addDetails(getResources().getString(R.string.price_xe_plus), xe_plus_Arr,idArr);

			database.addDetails(getResources().getString(R.string.price_xl), xl_Arr,idArr);
			database.addDetails(getResources().getString(R.string.price_xl_primo), xl_primo_Arr,idArr);

			database.addDetails(getResources().getString(R.string.price_xv), xv_Arr,idArr);
			database.addDetails(getResources().getString(R.string.price_xv_primo), xv_primo_Arr,idArr);

			database.addDetails(getResources().getString(R.string.price_dl_xv), xv_dl_Arr,idArr);
			database.addDetails(getResources().getString(R.string.price_dl_xv_primo), xv_primo_dl_Arr,idArr);

			database.addDetails(getResources().getString(R.string.price_dl_xv_prem), xv_prem_dl_Arr,idArr);
			database.addDetails(getResources().getString(R.string.price_dl_xv_prem_primo), xv_prem_primo_dl_Arr,idArr);*/
			database.deleteAllRow();

			try {
				filename	=	new FileInputStream(NEONXML);

				FileChannel fc = filename.getChannel();
				MappedByteBuffer bb = null;

				bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

				String jString = Charset.defaultCharset().decode(bb).toString();

				//JSONObject json = new JSONObject(jString);

				JSONArray jArr	= new JSONArray(jString);
				for(int i =0;i< jArr.length();i++){

					JSONObject tempObj 	= jArr.getJSONObject(i);
					String id 			= tempObj.getString("ID");
					String State 		= tempObj.getString("State");
					String City 		= tempObj.getString("City");
					String PET_XL 		= tempObj.getString("PET_XL");
					String PET_XL_O 	= tempObj.getString("PET_XL_O");
					String PET_XV_CVT 	= tempObj.getString("PET_XV_CVT");
					String DL_XE = null; 
					try{
						DL_XE = tempObj.getString("DL_XE");
						XE_PRESENT = true;
						sharePref.setXEDSL(XE_PRESENT);
					}catch(Exception ex){
						ex.printStackTrace();
					}
					if(DL_XE!=null){

					}else{
						DL_XE = "DL_XE";
						XE_PRESENT = false;
					}

					String DL_XL 		= tempObj.getString("DL_XL");
					String DL_XL_O 		= tempObj.getString("DL_XL_O");
					String DL_XV 		= tempObj.getString("DL_XV");
					String DL_XV_P 		= tempObj.getString("DL_XV_P");
					String ACT_XE 		= tempObj.getString("ACT_XE");
					String ACT_XL 		= tempObj.getString("ACT_XL");
					String ACT_XV 		= tempObj.getString("ACT_XV");
					String ACT_XV_S 	= tempObj.getString("ACT_XV_S");

					ArrayList<String> tempList = new ArrayList<String>();
					tempList.add(id);
					tempList.add(State);
					tempList.add(City);

					tempList.add(PET_XL);
					tempList.add(PET_XL_O);
					tempList.add(PET_XV_CVT);

					tempList.add(DL_XE);
					tempList.add(DL_XL);
					tempList.add(DL_XL_O);
					tempList.add(DL_XV);
					tempList.add(DL_XV_P);

					tempList.add(ACT_XE);
					tempList.add(ACT_XL);
					tempList.add(ACT_XV);
					tempList.add(ACT_XV_S);


					database.addDetails(coulmnArr, tempList);
				}

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 




			return null;
		}

		@Override
		protected void onPostExecute(String result) throws NullPointerException {
			// TODO Auto-generated method stub
			try {
				removeDialog(progress_bar_type);
				root_layout.setVisibility(View.VISIBLE);
				VALUE_ADDED = true;
				uniqueStateArr			= getStates();
				uniqueCityArr			= getCitysOfStates(uniqueStateArr.get(0));


				/*	StateCityListAdapter state_adapter 	= new StateCityListAdapter(actContext, uniqueStateArr);
				StateCityListAdapter city_adapter 	= new StateCityListAdapter(actContext, uniqueCityArr);

				list_state.setAdapter(state_adapter);
				list_city.setAdapter(city_adapter);*/

				TEMP_STATE 		= uniqueStateArr.get(getStateIndex("Delhi"));
				uniqueCityArr	= getCitysOfStates(TEMP_STATE);
				TEMP_CITY		= uniqueCityArr.get(0);

				text_state.setText(TEMP_STATE);
				text_city.setText(uniqueCityArr.get(0));

				ext_price_footer1.setText("* All prices shown are ex-showroom " + uniqueCityArr.get(0));

				StateCityListAdapter state_adapter 	= new StateCityListAdapter(actContext, uniqueStateArr);
				StateCityListAdapter city_adapter 	= new StateCityListAdapter(actContext, uniqueCityArr);

				list_state.setAdapter(state_adapter);
				list_city.setAdapter(city_adapter);

				setListAdapter(TEMP_STATE,TEMP_CITY);


			} catch(Exception ex){
				ex.printStackTrace();
			}
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showDialog(progress_bar_type);

		}
	}




	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setIndeterminate(false);
			pDialog.setMessage("Please wait");
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setCancelable(false);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}



	@Override
	public void onReceiveFileResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		SERVICE_COMPLETED = resultData.getBoolean("downloadCompleteFromService");
	}


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub



		if(SERVICE_COMPLETED){
			MenuItem subMenu2= menu.add("Refresh");
			subMenu2.setIcon(R.drawable.refresh_new1);
			subMenu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}

		/*SubMenu subMenu2 = menu.addSubMenu(" "+model+"");
		subMenu2.add("Petrol");
		subMenu2.add("Diesel");
		subMenu2.add("Active");

		MenuItem menu_model = subMenu2.getItem();
		menu_model.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);*/

		//		Context acontext = getSupportActionBar().getThemedContext();
		//		model_list = ArrayAdapter.createFromResource(acontext, R.array.mdoel_type, R.layout.list_menus);
		//		model_list.setDropDownViewResource(R.layout.list_dropdown_item);
		//
		//
		//		//Setting Navigation Type.
		//
		//		ac.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		//		
		//		ac.setListNavigationCallbacks(model_list, this);

		SubMenu subMenu1 = menu.addSubMenu("Menu");



		subMenu1.add("Home").setIcon(R.drawable.home);

		subMenu1.add("Features").setIcon(R.drawable.fetures_tick);
		subMenu1.add("Technical Specification").setIcon(R.drawable.tech_spec);
		subMenu1.add("Price List").setIcon(R.drawable.price);
		subMenu1.add("Test Drive").setIcon(R.drawable.test_drive_new);
		subMenu1.add("Gallery").setIcon(R.drawable.gallery);
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);




		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Contact Us")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,ContactUs.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Features")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Features_new.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		/*if(item.getTitle().toString().equalsIgnoreCase("Price List")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Price_list_new.class);

			startActivity(intent);
			finish();
		}*/
		if(item.getTitle().toString().equalsIgnoreCase("Technical Specification")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,TechnicalSpecification.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Home")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Home_new.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Petrol")){
			if(internetPresent){
				model = "Petrol";
				setListAdapter(TEMP_STATE, TEMP_CITY);
				getSherlock().dispatchInvalidateOptionsMenu();
			}
		}
		if(item.getTitle().toString().equalsIgnoreCase("Diesel")){
			if(internetPresent){
				model = "Diesel";
				setListAdapter(TEMP_STATE, TEMP_CITY);
				getSherlock().dispatchInvalidateOptionsMenu();
			}
		}
		if(item.getTitle().toString().equalsIgnoreCase("Active")){
			if(internetPresent){
				model = "Active";
				setListAdapter(TEMP_STATE, TEMP_CITY);
				getSherlock().dispatchInvalidateOptionsMenu();
			}
		}
		if(item.getTitle().toString().equalsIgnoreCase("Test Drive")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,TestDrive.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Gallery")){
			//share.setAppClose(false);

			FlurryAgent.logEvent("Gallery_Tab_Event");
			Intent intent=new Intent(this,NissanCarGallery.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		return true;
	}



	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		if(list_city.isShown()){
			list_city.setVisibility(View.GONE);
			//root_layout.setVisibility(View.GONE);
			relative_list_laLayout.setVisibility(View.GONE);

		}else if(list_state.isShown()){
			list_state.setVisibility(View.GONE);
			//root_layout.setVisibility(View.GONE);
			relative_list_laLayout.setVisibility(View.GONE);
		}else{
			super.onBackPressed();	
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		if(outState.isEmpty()){		
			outState.putString("model", model);
			outState.putString("TEMP_STATE", TEMP_STATE);
			outState.putString("TEMP_CITY", TEMP_CITY);
		}
	}


	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		if(savedInstanceState!=null){
			model 		= savedInstanceState.getString("model");
			TEMP_STATE 	= savedInstanceState.getString("TEMP_STATE");
			TEMP_CITY 	= savedInstanceState.getString("TEMP_CITY");

		}
	}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){

			FlurryAgent.onStartSession(this, getResources().getString(R.string.flurry_id));
			FlurryAgent.logEvent("PriceList_Tab_Event");
		}

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(this);

		}
	}


	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		String tempType  = model_list.getItem(itemPosition).toString();
		//Toast.makeText(context, ""+tempType, 0).show();
		model = tempType;
		setListAdapter(TEMP_STATE, TEMP_CITY);
		return true;
	}
}
