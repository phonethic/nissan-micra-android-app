package com.phonethics.nissanmicra;


import com.phonethics.imageloader.ImageLoader;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

public class HomePagerFragment extends Fragment {

	Activity 		context;
	String			imgUrl;
	ImageLoader		imageLoader;
	ImageView		imgView;


	public HomePagerFragment(){

	}
	public HomePagerFragment(Activity context,String imgUrl){
		this.context = context;
		this.imgUrl	 = imgUrl;
		imageLoader = new ImageLoader(context);
	}




	@Override
	public View getView() {
		// TODO Auto-generated method stub
		return super.getView();
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		if(savedInstanceState != null){
			context = (Activity) getActivity();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if(savedInstanceState != null){
			context = (Activity) getActivity().getApplicationContext();
		}
		View view	=	inflater.inflate(R.layout.home_pager_fragemnt_layout, container,false);
		imgView		=	(ImageView) view.findViewById(R.id.img_pager);
		imageLoader.DisplayImage(imgUrl, imgView);
		/*listView.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, data));*/		
		return view;

	}

}
