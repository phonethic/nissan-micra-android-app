package com.phonethics.nissanmicra;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Window;
import com.flurry.android.FlurryAgent;
import com.phonethics.customviewpager.JazzyViewPager;
import com.phonethics.customviewpager.JazzyViewPager.TransitionEffect;
import com.phonethics.imageloader.ImageLoader;
import com.phonethics.network.NetworkCheck;
import com.phonethics.nissanmicra.SplashScreen.FileFromURL;
import com.phonethics.nissanmicra.SplashScreen.PageAdapter;
import com.phonethics.nissanmicra.SplashScreen.PageFragment;
import com.phonethics.parser.SplashImageLinkParser;
import com.phonethics.parser.SplashImageLinksPojo;
import com.phonethics.pref.Share;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;


import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.ImageView.ScaleType;

public class SplashNew extends SherlockFragmentActivity {

	static ImageLoader 					imageLoader;

	//static DisplayImageOptions 			options;
	//ImageLoaderConfiguration 			config;
	File 								cacheDir;
	Activity 							context;
	ImageView 							splashImage,transparenImage;
	ImageView 							open,logoImage,img_flip,img_flip_1;
	TextView							text_play_hard;
		
	Share								sharePref;
	HorizontalScrollView 				hsSplashScroll;

	private int 					scrollMax;
	private int 					scrollPos =	0;
	private TimerTask 				clickSchedule;
	private TimerTask 				scrollerSchedule;
	private TimerTask 				faceAnimationSchedule;

	private Timer scrollTimer		=	null;
	private Timer clickTimer		=	null;

	String 			test			=	"closed,07:00,07:00,07:00,07:00,07:00,closed";
	String 			xmlurl			= 	"http://nissanmicra.co.in/neon-micra/splash.xml";
	String 			xml,NEONXML		=	"/sdcard/neoncache/micra_splash.xml";
	//String 			MICRA_GALL		= 	"/sdcard/neoncache/micra_new.xml";
	//String 			MICRA_GALL_URL  =  	"http://stage.phonethics.in/proj/neon/micra_gallery1.php";
	public static final int 		progress_bar_type = 0; 
	private ProgressDialog 			pDialog;
	FileInputStream					filename;
	ArrayList<SplashImageLinksPojo> 	neongallery;

	JazzyViewPager 					mPager;
	ArrayList<String> 				urls	=	new ArrayList<String>();

	NetworkCheck 					network;

	String 							IMAGE_URL="";
	RelativeLayout					layout;

	ImageView						appSplash,appSplas_1;
	Handler							handler;
	Runnable						runnable;
	Animation 						fade_in_anim,fade_out_anim;
	int								i=0;
	ViewFlipper						flipper;
	boolean							isAppFirstTimeOpen;
	RelativeLayout					layout_img;
	boolean 						PAGER_TOUCHED = false;
	ImageView						tempImageView;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//setContentView(R.layout.activity_splash_screen);
		setContentView(R.layout.activity_splash_new);

		FlurryAgent.logEvent("Nissan Micra App Started");

		context			=	this;
	

		open			=	(ImageView)findViewById(R.id.open);
		transparenImage = 	(ImageView) findViewById(R.id.transparenImage);
		logoImage		=	(ImageView) findViewById(R.id.logoImage);
		img_flip		=	(ImageView) findViewById(R.id.img_flip);
		img_flip_1		=	(ImageView) findViewById(R.id.img_flip_1);
		tempImageView	=	(ImageView) findViewById(R.id.img_temp);
		tempImageView.setBackgroundColor(Color.BLACK);
		tempImageView.getBackground().setAlpha(20);
		layout_img		=	(RelativeLayout) findViewById(R.id.relative_img_layout);
		logoImage.setVisibility(View.INVISIBLE);
		mPager			=	(JazzyViewPager)findViewById(R.id.viewPagerSplash);
		text_play_hard	= 	(TextView) findViewById(R.id.text_play_hard);
		text_play_hard.setTextColor(Color.WHITE);
		text_play_hard.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf"));
		layout			= 	(RelativeLayout) findViewById(R.id.layout_play_hard);
		layout.setBackgroundColor(Color.BLACK);
		layout.getBackground().setAlpha(125);
		
		Animation anim 	= AnimationUtils.loadAnimation(context, R.anim.slide_right);
		fade_in_anim 	= AnimationUtils.loadAnimation(context, R.anim.fade_in);
		fade_out_anim 	= AnimationUtils.loadAnimation(context, R.anim.fade_out);

		transparenImage.startAnimation(anim);

		network			=	new NetworkCheck(context);

		IMAGE_URL		=	getResources().getString(R.string.imageurl);
		neongallery 	= new ArrayList<SplashImageLinksPojo>();
		sharePref 		= new Share(context);
		isAppFirstTimeOpen = sharePref.isAppOpenFirstTime();
		if(isAppFirstTimeOpen){
			sharePref.setAppOpenFirstTime(false);
		}
		File file  		= new File(NEONXML);

		//imageLoader=ImageLoader.getInstance();

		imageLoader = new ImageLoader(context);




		if(file.exists()){



			try {
				filename = new FileInputStream(NEONXML);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block 
				e.printStackTrace();
			}
			neongallery = SplashImageLinkParser.parse(filename);

			if(neongallery.size()!=0){
				for(int i=0;i<neongallery.size();i++){
					urls.add(neongallery.get(i).getLink());
					//setFlipperImage(neongallery.get(i).getLink());
				}


				for(int i=0;i<urls.size();i++){
					//setFlipperImage(urls.get(i));
				}
				PageAdapter adapter	=	new PageAdapter(getSupportFragmentManager(), context, urls);
				mPager.setAdapter(adapter);
				mPager.setOffscreenPageLimit(urls.size()-1);
				mPager.setTransitionEffect(TransitionEffect.ZoomIn);


			}else{
				urls.add(IMAGE_URL+"slide1.jpg");
				//urls.add(IMAGE_URL+"slide2.jpg");


				//mPager	=	(JazzyViewPager)findViewById(R.id.viewPagerSplash);
				PageAdapter adapter	=	new PageAdapter(getSupportFragmentManager(), context, urls);
				mPager.setAdapter(adapter);
				mPager.setOffscreenPageLimit(urls.size()-1);
				mPager.setTransitionEffect(TransitionEffect.ZoomIn);
			}

			if(network.isNetworkAvailable()){
				FileFromURL downloadFile = new FileFromURL();
				downloadFile.execute(xmlurl);
			}

		}else if(network.isNetworkAvailable()){
			FileFromURL downloadFile = new FileFromURL();
			downloadFile.execute(xmlurl);
		}else{
			urls.add(IMAGE_URL+"slide1.jpg");
			//urls.add(IMAGE_URL+"slide2.jpg");


			//mPager	=	(JazzyViewPager)findViewById(R.id.viewPagerSplash);
			PageAdapter adapter	=	new PageAdapter(getSupportFragmentManager(), context, urls);
			mPager.setAdapter(adapter);
			mPager.setOffscreenPageLimit(urls.size()-1);
			mPager.setTransitionEffect(TransitionEffect.ZoomIn);
		}



		/*img_flip_1.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				logoImage.setVisibility(View.VISIBLE);
				img_flip_1.setVisibility(View.GONE);
				img_flip.setVisibility(View.GONE);
				//mPager.setVisibility(View.VISIBLE);
				handler.removeCallbacks(runnable);
				return false;
			}
		});
		img_flip.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				logoImage.setVisibility(View.VISIBLE);
				img_flip_1.setVisibility(View.GONE);
				img_flip.setVisibility(View.GONE);
				//mPager.setVisibility(View.VISIBLE);
				handler.removeCallbacks(runnable);
				return false;
			}
		});*/
		
		layout_img.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				logoImage.startAnimation(fade_in_anim);
				//logoImage.setVisibility(View.VISIBLE);
				fade_in_anim.setFillAfter(true);
				layout_img.setVisibility(View.GONE);
				handler.removeCallbacks(runnable);
				return false;
			}
		});
		
		mPager.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				int action = event.getAction();
				switch (action) {
				case MotionEvent.ACTION_DOWN:
					//Toast.makeText(context, "you TOUCH the screen", 0).show();
					PAGER_TOUCHED = true;
					handler.removeCallbacks(runnable);

					break;
				case MotionEvent.ACTION_UP:
					//Toast.makeText(context, "you REMOVED the finger from screen", 0).show();
					//handler.removeCallbacks(runnable);
					handler.postDelayed(runnable, 2000);
					break;
				case MotionEvent.ACTION_MOVE:
					//Toast.makeText(context, "you MOVING the finger on screen", 0).show();
				
					break;

				default:
					break;
				}
				return false;


			}
		});
		
		
		mPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				imageLoader.DisplayImage(urls.get(arg0), img_flip_1);
				i = arg0;
				Log.d("=====", "Page No : " +i);
				
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});


		img_flip_1.setImageResource(R.drawable.slide1);
		handler = new Handler();
		runnable = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try{
					//mPager.setVisibility(View.GONE);
					/*if(!img_flip.isShown()){
						img_flip.setVisibility(View.VISIBLE);
					}
					if(!img_flip_1.isShown()){
						img_flip_1.setVisibility(View.VISIBLE);
					}
					*/
					if(!layout_img.isShown()){
						layout_img.setVisibility(View.VISIBLE);
						logoImage.startAnimation(fade_out_anim);
						fade_out_anim.setFillAfter(true);
						//logoImage.setVisibility(View.GONE);
						
					}
					//if(!PAGER_TOUCHED){
						imageLoader.DisplayImage(urls.get(i), img_flip);	
					//}
					
					img_flip.startAnimation(fade_in_anim);
					fade_in_anim.setFillAfter(true);
					fade_in_anim.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub
							

						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							PAGER_TOUCHED = false;
							imageLoader.DisplayImage(urls.get(i), img_flip_1);
							mPager.setCurrentItem(i);
							i++;
							if(i==urls.size()){
								i=0;
							}
						}
					});

					handler.postDelayed(this, 2500);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		};
		handler.postDelayed(runnable,1000);


		layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				imageLoader.clearCache();
				sharePref.setAppClose(true);
				Intent intent=new Intent(context, Home_new.class);
				startActivity(intent);
				finish();
			}
		});



		/*mPager.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				int action = event.getAction();
				switch (action) {
				case MotionEvent.ACTION_DOWN:
					//Toast.makeText(context, "you TOUCH the screen", 0).show();
					handler.removeCallbacks(runnable);

					break;
				case MotionEvent.ACTION_UP:
					//Toast.makeText(context, "you REMOVED the finger from screen", 0).show();
					img_flip_1.setVisibility(View.VISIBLE);
					img_flip.setVisibility(View.VISIBLE);
					handler.removeCallbacks(runnable);
					
					handler.postDelayed(runnable, 4000);
					break;
				case MotionEvent.ACTION_MOVE:
					//Toast.makeText(context, "you MOVING the finger on screen", 0).show();
						handler.removeCallbacks(runnable);
					handler.postDelayed(runnable, 5000);
					break;

				default:
					break;
				}
				return false;


			}
		});*/


		/*mPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				//flipper.setDisplayedChild(arg0);
				i = arg0;
				//Log.d("----", "mPager page "+mPager.getCurrentItem()+" arg0 "+arg0+" flipper pos "+flipper.getDisplayedChild());
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});*/





	}
	
	
	private void setFlipperImage(String res) {

		ImageView image = new ImageView(context);
		RelativeLayout.LayoutParams params =new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT);

		image.setLayoutParams(params);
		image.setScaleType(ScaleType.FIT_XY);
		//image.setAdjustViewBounds(true);
		imageLoader.DisplayImage(res, image);
		//flipper.addView(image);
	}

	//Creating Pages with PageAdapter.
	public class PageAdapter extends FragmentStatePagerAdapter
	{
		Activity context;
		ArrayList<String> pages;
		public PageAdapter(FragmentManager fm,Activity context,ArrayList<String> pages) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.pages=pages;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return new PageFragment(pages.get(position), context);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return pages.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Object obj = super.instantiateItem(container, position);
			mPager.setObjectForPosition(obj, position);
			return obj;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			if(object != null){
				return ((Fragment)object).getView() == view;
			}else{
				return false;
			}
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub

			FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commit();

			super.destroyItem(container, position, object);
		}
	}

	@SuppressLint("ValidFragment")
	public static class PageFragment extends Fragment 
	{
		String url="";
		Activity context;
		View view;
		ImageView splashImage;
		ProgressBar prog;
		TextView txtPlaceText;
		Animation anim;
		public PageFragment()
		{

		}
		public PageFragment(String url,Activity context)
		{
			this.url=url;
			this.context=context;
			anim=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
			anim.setDuration(1200);
		}
		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
		}

		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup container, Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			view=inflater.inflate(R.layout.splashimagelayout, null);
			splashImage=(ImageView)view.findViewById(R.id.splImage);
			prog=(ProgressBar)view.findViewById(R.id.prog);
			txtPlaceText=(TextView)view.findViewById(R.id.txtPlaceText);
			txtPlaceText.setShadowLayer(5, 1, 3, Color.parseColor("#112e73"));

			imageLoader.DisplayImage(url, splashImage);
			/*			txtPlaceText.startAnimation(anim);*/

			/*imageLoader.displayImage(url, splashImage,options, new ImageLoadingListener() {

				@Override
				public void onLoadingStarted(String imageUri, View view) {
					// TODO Auto-generated method stub
					prog.setVisibility(View.VISIBLE);
					txtPlaceText.setVisibility(View.VISIBLE);

				}

				@Override
				public void onLoadingFailed(String imageUri, View view,
						FailReason failReason) {
					// TODO Auto-generated method stub
					prog.setVisibility(View.INVISIBLE);
					txtPlaceText.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					// TODO Auto-generated method stub
					prog.setVisibility(View.INVISIBLE);
					txtPlaceText.setVisibility(View.INVISIBLE);
				}

				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					// TODO Auto-generated method stub
					prog.setVisibility(View.INVISIBLE);
					txtPlaceText.setVisibility(View.VISIBLE);
				}
			});*/


			return view;
		}

	}


	void DeleteRecursive(File file) {
		/*if (fileOrDirectory.isDirectory())
			for (File child : fileOrDirectory.listFiles())
			{

				Log.i("Delete:", "File "+child.getAbsolutePath());
				DeleteRecursive(child);
			}

		fileOrDirectory.delete();*/

		if(file.isDirectory()){

			//directory is empty, then delete it
			if(file.list().length==0){

				file.delete();
				System.out.println("Directory is deleted : " 
						+ file.getAbsolutePath());

			}else{

				//list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					//construct the file structure
					File fileDelete = new File(file, temp);

					//recursive delete
					DeleteRecursive(fileDelete);
				}

				//check the directory again, if empty then delete it
				if(file.list().length==0){
					file.delete();
					System.out.println("Directory is deleted : " 
							+ file.getAbsolutePath());
				}
			}

		}else{
			//if file, then delete it
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}

	}


	class FileFromURL extends AsyncTask<String, String, String> {


		DefaultHttpClient 	httpClient;
		HttpPost 			httpPost;
		HttpResponse		httpRes;
		HttpEntity			httpEnt;


		@Override
		protected String doInBackground(String... xmlur) {
			// TODO Auto-generated method stub
			int count;

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(xmlurl);
				httpRes		=	httpClient.execute(httpPost);
				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	= DocumentBuilderFactory.newInstance();
				DocumentBuilder			db	= dbf.newDocumentBuilder();
				InputSource				is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));

				URL url = new URL(xmlur[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				// getting file length
				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file

				try{
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
				FileOutputStream output = new FileOutputStream(NEONXML);
				byte data[] = new byte[1024];
				long total = 0;
				while (   ((count = input.read(data)) != -1)  ) {
					total += count;
					output.write(data, 0, count);
				}
				output.flush();
				output.close();
				input.close();
			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");

			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			//			 TODO Auto-generated method stub
			try {
				if(pDialog.isShowing()){
					removeDialog(progress_bar_type);
				}
				urls.clear();
				filename = new FileInputStream(NEONXML);
				neongallery = SplashImageLinkParser.parse(filename);


				for(int i=0;i<neongallery.size();i++){
					urls.add(neongallery.get(i).getLink());
				}
				for(int i=0;i<urls.size();i++){
					//setFlipperImage(urls.get(i));
				}

				//appSplash.setVisibility(View.GONE);
				handler.removeCallbacks(runnable);
				handler.postDelayed(runnable, 1000);
				mPager	=	(JazzyViewPager)findViewById(R.id.viewPagerSplash);
				PageAdapter adapter	=	new PageAdapter(getSupportFragmentManager(), context, urls);
				mPager.setAdapter(adapter);
				mPager.setOffscreenPageLimit(urls.size()-1);
				mPager.setTransitionEffect(TransitionEffect.ZoomIn);



			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(NullPointerException npx){
				npx.printStackTrace();
			}
			catch(Exception ex){
			}
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			File file  		= new File(NEONXML);
			if(isAppFirstTimeOpen){
				showDialog(progress_bar_type);
			}else if(!file.exists()){
				showDialog(progress_bar_type);
			}

		}
	}

	/*class GallryFileFromURL extends AsyncTask<String, String, String> {


		DefaultHttpClient 	httpClient;
		HttpPost 			httpPost;
		HttpResponse		httpRes;
		HttpEntity			httpEnt;


		@Override
		protected String doInBackground(String... xmlur) {
			// TODO Auto-generated method stub
			int count;

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(MICRA_GALL_URL);
				httpRes		=	httpClient.execute(httpPost);
				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	= DocumentBuilderFactory.newInstance();
				DocumentBuilder			db	= dbf.newDocumentBuilder();
				InputSource				is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));

				URL url = new URL(xmlur[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				// getting file length
				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file

				try{
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
				FileOutputStream output = new FileOutputStream(MICRA_GALL);
				byte data[] = new byte[1024];
				long total = 0;
				while (   ((count = input.read(data)) != -1)  ) {
					total += count;
					output.write(data, 0, count);
				}
				output.flush();
				output.close();
				input.close();
			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");

			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			try {
				removeDialog(progress_bar_type);
			} catch(NullPointerException npx){
				npx.printStackTrace();
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showDialog(progress_bar_type);

		}
	}*/
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setIndeterminate(false);
			pDialog.setMessage("Please wait");
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setCancelable(false);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}

	public void applyAnimOnImageView(boolean apply){}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		FlurryAgent.onStartSession(this, getResources().getString(R.string.flurry_id));

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		handler.removeCallbacks(runnable);
		FlurryAgent.onEndSession(this);

	}


	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		handler.postDelayed(runnable, 3000);
	}


	private Animation outToLeftAnimation() {


		Animation fadiIn = new AlphaAnimation(0, 1);
		fadiIn.setDuration(2000);
		//fadiIn.setInterpolator(new DecelerateInterpolator());
		return fadiIn;
	}

	private Animation inFromRightAnimation() {

		Animation fadOut = new AlphaAnimation(1,0);
		fadOut.setDuration(2000);
		//fadOut.setInterpolator(new DecelerateInterpolator());
		return fadOut;
	}

}


