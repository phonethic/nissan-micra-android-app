package com.phonethics.nissanmicra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.flurry.android.FlurryAgent;

public class TestDrive extends SherlockActivity {

	Context							context;
	Activity						actContext;
	static Typeface 				font;
	String							app_color="";
	ListView						contactList;
	ActionBar 						ac;
	ArrayList<Integer> 				imagesArr;
	ArrayList<String> 				contactListData;
	boolean							useFlurry;
	Map<String, String> 			articleParams;


	ArrayList<String> galallLinks, galallDescription, galallCaption, galallVLinks, galallNames;

	Bundle bundle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_drive);

		context 		= this;
		actContext		= this;

		FlurryAgent.logEvent("TestDrive_Tab_Event");
		
		font 							= Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf");
		LayoutInflater inflater 		= LayoutInflater.from(this);
		View customView 				= inflater.inflate(R.layout.action_bar_title_text, null);
		TextView textTitle				= (TextView) customView.findViewById(R.id.text_action_title);
		textTitle.setText("Test Drive");
		textTitle.setTypeface(font);

		ac 								= getSupportActionBar();
		ac.setCustomView(customView);
		ac.setDisplayShowTitleEnabled(true);
		ac.setDisplayShowCustomEnabled(false);
		ac.setTitle("Test Drive");
		ac.show();

		// Getting gallery data

		bundle = getIntent().getExtras();
		if(bundle!=null){

			galallLinks = bundle.getStringArrayList("imgArr");
			galallDescription	= bundle.getStringArrayList("imgDesc");
			galallCaption = bundle.getStringArrayList("imgCaption");
			galallVLinks = bundle.getStringArrayList("vlinksArr");
			galallNames = bundle.getStringArrayList("allNames");
			//Toast.makeText(context, ""+imgLinks.size(), 0).show();
		}


		//Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		app_color 						= getResources().getString(R.string.app_color);
		Drawable colorDrawable 			= new ColorDrawable(Color.parseColor("#"+app_color));
		Drawable bottomDrawable 		= getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld 				= new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);

		contactList 					= (ListView) findViewById(R.id.listView_testdrive_data);
		
		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		contactList.setLayoutAnimation(controller);
		
		useFlurry						= Boolean.parseBoolean(getResources().getString(R.string.useflurry));
		imagesArr 						= new ArrayList<Integer>();
		articleParams					= new HashMap<String, String>();

		imagesArr.add(R.drawable.cont);
		imagesArr.add(R.drawable.req_callback_new);


		contactListData 				= new ArrayList<String>();
		contactListData.add("Call to book");
		contactListData.add("Request a test drive");

		ConatcAdapter adap = new ConatcAdapter(this, R.drawable.ic_launcher,  R.drawable.ic_launcher, contactListData, imagesArr);
		contactList.setAdapter(adap);


		contactList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				if(pos==0){
					AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.setTitle("Nissan Micra");
					alertDialog.setMessage("Do you want to call on this number?");
					alertDialog.setCancelable(true);
					alertDialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							
							FlurryAgent.logEvent("TestDrive_Call_Event");
							Intent call = new Intent(android.content.Intent.ACTION_DIAL);
							call.setData(Uri.parse(getResources().getString(R.string.car_customer)));
							startActivity(call);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});	
					alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});

					AlertDialog alert=alertDialog.show();

				}else if(pos==1){
					
					FlurryAgent.logEvent("TestDrive_Request_Event");
					startActivity(new Intent(context, TestDriveRequest.class));
				}


			}
		});
	}




	static class ConatcAdapter extends ArrayAdapter<String>
	{
		ArrayList<String> store_name,distance;
		ArrayList<Integer> logo;
		Activity context;
		LayoutInflater inflate;

		public ConatcAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> store_name,ArrayList<Integer>logo) {
			super(context, resource, textViewResourceId, store_name);
			// TODO Auto-generated constructor stub
			this.store_name=store_name;

			this.logo=logo;
			this.context=context;
			inflate=context.getLayoutInflater();

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return store_name.size();
		}
		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return store_name.get(position);
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.contactlayout,null);
				holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.txtContact=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.txtContact.setTypeface(font);
				convertView.setTag(holder);

			}
			ViewHolder hold=(ViewHolder)convertView.getTag();

			hold.imgStoreLogo.setImageResource(logo.get(position));
			hold.txtContact.setText(store_name.get(position));
			hold.txtContact.setTextSize(18);




			return convertView;
		}
		static class ViewHolder
		{
			ImageView imgStoreLogo;
			TextView txtContact;
			TextView txtDistance;
		}


	}



	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub


		SubMenu subMenu1 = menu.addSubMenu("Menu");

		subMenu1.add("Home").setIcon(R.drawable.home);
		subMenu1.add("Features").setIcon(R.drawable.fetures_tick);
		subMenu1.add("Technical Specification").setIcon(R.drawable.tech_spec);
		subMenu1.add("Price List").setIcon(R.drawable.price);
		subMenu1.add("Test Drive").setIcon(R.drawable.test_drive_new);
		subMenu1.add("Gallery").setIcon(R.drawable.gallery);
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);

		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Contact Us")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,ContactUs.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Features")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Features_new.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Technical Specification")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,TechnicalSpecification.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Price List")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Price_list_new.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Home")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Home_new.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Gallery")){
			//share.setAppClose(false);
			
			FlurryAgent.logEvent("Gallery_Tab_Event");
			Intent intent=new Intent(this,NissanCarGallery.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		return true;
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){


			FlurryAgent.onStartSession(this, getResources().getString(R.string.flurry_id));
			
		}

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		if(useFlurry){
			FlurryAgent.onEndSession(this);
		}

	
	}
}
