package com.phonethics.nissanmicra;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.InputSource;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.phonethics.adapters.PriceListAdapter;
import com.phonethics.adapters.StateCityListAdapter;
import com.phonethics.database.MicraDataBase;
import com.phonethics.network.NetworkCheck;
import com.phonethics.parser.PriceListParser;
import com.phonethics.parser.PriceListPojo;
import com.phonethics.pref.Share;
import com.phonethics.services.DownloadReceiver;
import com.phonethics.services.DownloadReceiver.DownloadResult;
import com.phonethics.services.DownloadService;

public class PriceList extends SherlockActivity implements DownloadResult{


	Context								context;
	Activity							actContext;
	TextView							text_state,text_city;
	Spinner								spinner_state,spinner_city;

	PriceListPojo						priceList;
	PriceListParser						priceListParser;
	ActionBar 							ac;
	NetworkCheck						netAvailable;
	FileFromURL 						downloadFile;
	DownloadReceiver					receiver;
	DialogThread 						thread;
	FileInputStream						filename;
	Share								sharePref;
	PriceListAdapter					price_adapter;
	StateCityListAdapter				state_adapter,city_adapter;
	MicraDataBase						database;


	ArrayList<String> 					statesArr,cityArr,uniqueStateArr,uniqueCityArr,coulmnArr,idArr,xe_Arr,modelArr;
	ArrayList<String> 					xe_plus_Arr	,xl_Arr,xl_primo_Arr,xv_Arr,xv_primo_Arr,xv_dl_Arr,xv_primo_dl_Arr,xv_prem_dl_Arr,xv_prem_primo_dl_Arr;
	ArrayList<PriceListPojo>			priceListArr;

	public static final int 			progress_bar_type = 0; 
	private ProgressDialog 				pDialog;
	String 								//xmlurl	= "http://stage.phonethics.in/proj/neon/micra_pricelist.php",
	xmlurl	= "http://stage.phonethics.in/proj/neon/micra_newpricelist.php",
	TEMP_STATE="",TEMP_CITY="";
	String								xml,
	//NEONXML ="/sdcard/neoncache/pricelist.xml";
	NEONXML ="/sdcard/neoncache/pricelist_new.txt";
	String								model_xe = "XE", model_xe_plus = "XE Plus", model_xl = "XL",model_xl_primo = "XL Primo";
	String								model_xv = "XV", model_xv_primo = "XV Primo", model_xv_prem = "XV Prem",model_xv_prem_primo="XV Primo Prem";
	boolean								SERVICE_CALLED_ONCE = false,SERVICE_COMPLETED,OPEN_FIRST_TIME,VALUE_ADDED = false,STATE_CLICKED = true;
	private static long 				SLEEP_TIME = (long) 1;
	ListView							listView;
	ListView							list_state,list_city;
	RelativeLayout						root_layout,relative_list_laLayout;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_price_list);

		ac 				= getSupportActionBar();
		ac.setTitle("Price List");
		ac.show();

		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });
		ac.setBackgroundDrawable(ld);

		context					= this;
		actContext				= this;
		text_state				= (TextView) findViewById(R.id.text_state);
		text_city				= (TextView) findViewById(R.id.text_city);

		spinner_state			= (Spinner) findViewById(R.id.spinner_states);
		spinner_city			= (Spinner) findViewById(R.id.spinner_citys);

		listView				= (ListView) findViewById(R.id.list_price);
		list_state				= (ListView) findViewById(R.id.list_state);
		list_city				= (ListView) findViewById(R.id.list_city);

		root_layout				= (RelativeLayout) findViewById(R.id.layout_root);
		relative_list_laLayout	= (RelativeLayout) findViewById(R.id.relative_list_layout);
		root_layout.setVisibility(View.VISIBLE);

		View footerView		= ((LayoutInflater) actContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_view, null, false);
		TextView textView	= (TextView) footerView.findViewById(R.id.text_list_footer);
		textView.setText(getResources().getString(R.string.text_footer_condition));
		textView.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf"));
		listView.addFooterView(footerView);
		
		statesArr				= new ArrayList<String>();
		cityArr					= new ArrayList<String>();
		modelArr				= new ArrayList<String>();

		idArr					= new ArrayList<String>();

		xe_Arr					= new ArrayList<String>();
		xe_plus_Arr				= new ArrayList<String>();

		xl_Arr					= new ArrayList<String>();
		xl_primo_Arr			= new ArrayList<String>();

		xv_Arr					= new ArrayList<String>();
		xv_primo_Arr			= new ArrayList<String>();

		xv_dl_Arr				= new ArrayList<String>();
		xv_primo_dl_Arr			= new ArrayList<String>();

		xv_prem_dl_Arr			= new ArrayList<String>();
		xv_prem_primo_dl_Arr	= new ArrayList<String>();
		uniqueStateArr			= new ArrayList<String>();
		uniqueCityArr			= new ArrayList<String>();
		coulmnArr				= new ArrayList<String>();
		priceListArr			= new ArrayList<PriceListPojo>();

		sharePref 				= new Share(context);
		database				= new MicraDataBase(context);
		priceList				= new PriceListPojo(context);
		priceListParser			= new PriceListParser(context);
		netAvailable 			= new NetworkCheck(context);
		receiver				= new DownloadReceiver(new Handler());
		receiver.setReceiver(this);

		if(isFilePresent()){

			startParsing();

		}else{
			if(isInternetPresent()){

				downloadFile();

			}else{
				Toast.makeText(context, "No Internet Connection", 0).show();
			}
		}




		spinner_state.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int pos, long arg3) {
				// TODO Auto-generated method stub

				if(VALUE_ADDED){
					uniqueCityArr.clear();
					uniqueCityArr			= getCitysOfStates(uniqueStateArr.get(pos));

					text_state.setText(uniqueStateArr.get(pos));
					text_city.setText(uniqueCityArr.get(0));

					TEMP_STATE = uniqueStateArr.get(pos);
					TEMP_CITY = uniqueCityArr.get(0);

					spinner_city.setAdapter(new ArrayAdapter<String>(context, R.id.text_city_name, uniqueCityArr));
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});

		spinner_city.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int pos, long arg3) {
				// TODO Auto-generated method stub
				if(VALUE_ADDED){
					text_city.setText(uniqueCityArr.get(pos));
					TEMP_CITY = uniqueCityArr.get(pos);
					setListAdapter(TEMP_STATE,TEMP_CITY);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});


		text_state.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				relative_list_laLayout.setVisibility(View.VISIBLE);
				list_state.setVisibility(View.VISIBLE);
				STATE_CLICKED = true;


			}
		});

		text_city.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				relative_list_laLayout.setVisibility(View.VISIBLE);
				list_city.setVisibility(View.VISIBLE);
				STATE_CLICKED = false;
			}
		});


		list_state.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub

				if(STATE_CLICKED){
					uniqueCityArr.clear();
					uniqueCityArr			= getCitysOfStates(uniqueStateArr.get(pos));
					city_adapter = new StateCityListAdapter(actContext, uniqueCityArr);

					//list_city.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, uniqueCityArr));
					list_city.setAdapter(city_adapter);

					TEMP_STATE 	= uniqueStateArr.get(pos);
					TEMP_CITY 	= uniqueCityArr.get(0);
					setListAdapter(TEMP_STATE,TEMP_CITY);

					text_state.setText(TEMP_STATE);
					text_city.setText(TEMP_CITY);


				}
				list_state.setVisibility(View.GONE);
				relative_list_laLayout.setVisibility(View.GONE);


			}
		});

		list_city.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				text_city.setText(uniqueCityArr.get(pos));
				TEMP_CITY = uniqueCityArr.get(pos);
				setListAdapter(TEMP_STATE,TEMP_CITY);

				list_city.setVisibility(View.GONE);
				relative_list_laLayout.setVisibility(View.GONE);
			}
		});

	}



	public boolean isInternetPresent(){
		return netAvailable.isNetworkAvailable();
	}


	public void downloadFile(){
		downloadFile	=	new FileFromURL();
		downloadFile.execute(xmlurl);
	}



	public boolean isFilePresent(){

		File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
		if(!myDirectory.exists()) {                                 
			myDirectory.mkdirs();
		}
		File xmlFile1	=	new File(NEONXML);
		if(xmlFile1.exists()){

			return true;
		}
		return false;
	}


	public void setSharePrefrences(){
		OPEN_FIRST_TIME = sharePref.isActivityOpenFirstTime();
		if(OPEN_FIRST_TIME){
			sharePref.setLaunchFirstTime(false);
		}
	}


	public void startService(){
		if(!SERVICE_CALLED_ONCE){
			//Toast.makeText(context, "service", 0).show();
			//xmlurl = "http://stage.phonethics.in/proj/neon/neon_gallery1.php";
			SERVICE_CALLED_ONCE = true;
			Intent intent2=new Intent(context, DownloadService.class);
			intent2.putExtra("downloadFile",receiver);
			intent2.putExtra("URL", xmlurl);
			intent2.putExtra("FILENAME", NEONXML);
			context.startService(intent2);
		}
	}

	private class DialogThread extends Thread {
		/*ProgressDialog mProgressDialog = new ProgressDialog(context);*/
		public Handler mHandler;

		@Override
		/**
		 * Sleep for some time and than start new activity.
		 */

		public void run() {
			//Looper.prepare();

			try {
				Thread.sleep(SLEEP_TIME*1000);
			} catch (Exception e) {
				e.printStackTrace();
			}

			//Looper.loop();
			removeDialog(progress_bar_type);
			tempMethod();
		}
	}	



	void tempMethod(){
		try {
			runOnUiThread(new Runnable() {
				public void run() {

					thread 		= null;	
					try {
						filename	=	new FileInputStream(NEONXML);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}


	public void startParsing(){


		coulmnArr.clear();

		coulmnArr.add(getResources().getString(R.string.new_price_id));
		coulmnArr.add(getResources().getString(R.string.new_price_state));
		coulmnArr.add(getResources().getString(R.string.new_price_city));

		coulmnArr.add(getResources().getString(R.string.new_price_pet_xl));
		coulmnArr.add(getResources().getString(R.string.new_price_pet_xl_o));
		coulmnArr.add(getResources().getString(R.string.new_price_pet_xv_cvt));

		coulmnArr.add(getResources().getString(R.string.new_price_dl_xl));
		coulmnArr.add(getResources().getString(R.string.new_price_dl_xl_o));
		coulmnArr.add(getResources().getString(R.string.new_price_dl_xv));
		coulmnArr.add(getResources().getString(R.string.new_price_dl_xv_p));

		coulmnArr.add(getResources().getString(R.string.new_price_act_xe));
		coulmnArr.add(getResources().getString(R.string.new_price_act_xl));
		coulmnArr.add(getResources().getString(R.string.new_price_act_xv));
		coulmnArr.add(getResources().getString(R.string.new_price_act_xv_s));



		if(sharePref.isActivityOpenFirstTime()){
			/*	if(true){*/
			sharePref.setActivityOpen(false);
			ADD_VALUES_TO_DATABASE add_Values  = new ADD_VALUES_TO_DATABASE();
			add_Values.execute("");
		}else{

			root_layout.setVisibility(View.VISIBLE);
			VALUE_ADDED = true;
			uniqueStateArr			= getStates();
			uniqueCityArr			= getCitysOfStates(uniqueStateArr.get(0));


			state_adapter 	= new StateCityListAdapter(actContext, uniqueStateArr);
			city_adapter 	= new StateCityListAdapter(actContext, uniqueCityArr);

			list_state.setAdapter(state_adapter);
			list_city.setAdapter(city_adapter);

			TEMP_STATE = uniqueStateArr.get(0);
			TEMP_CITY = uniqueCityArr.get(0);


			text_state.setText(uniqueStateArr.get(0));
			text_city.setText(uniqueCityArr.get(0));

			setListAdapter(uniqueStateArr.get(0),uniqueCityArr.get(0));
		}


		/*try {
			filename	=	new FileInputStream(NEONXML);

			FileChannel fc = filename.getChannel();
			MappedByteBuffer bb = null;

			bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

			String jString = Charset.defaultCharset().decode(bb).toString();

			JSONObject json = new JSONObject(jString);

			JSONArray jArr	= json.getJSONArray("JSON");
			for(int i =0;i< jArr.length();i++){

				JSONObject tempObj = jArr.getJSONObject(i);
				String id 			= tempObj.getString("ID");
				String State 		= tempObj.getString("State");
				String City 		= tempObj.getString("City");
				String PET_XL 		= tempObj.getString("PET_XL");
				String PET_XL_O 	= tempObj.getString("PET_XL_O");
				String PET_XV_CVT 	= tempObj.getString("PET_XV_CVT");
				String DL_XL 		= tempObj.getString("DL_XL");
				String DL_XL_O 		= tempObj.getString("DL_XL_O");
				String DL_XV 		= tempObj.getString("DL_XV");
				String DL_XV_P 		= tempObj.getString("DL_XV_P");
				String ACT_XE 		= tempObj.getString("ACT_XE");
				String ACT_XL 		= tempObj.getString("ACT_XL");
				String ACT_XV 		= tempObj.getString("ACT_XV");
				String ACT_XV_S 	= tempObj.getString("ACT_XV_S");
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		 */
		//priceListArr = PriceListParser.parse(filename);




		/*for(int i=0;i<priceListArr.size();i++){

			idArr.add(priceListArr.get(i).getId());

			statesArr.add(priceListArr.get(i).getState());
			cityArr.add(priceListArr.get(i).getCity());

			xe_Arr.add(priceListArr.get(i).getXe());
			xe_plus_Arr.add(priceListArr.get(i).getXe_plus());

			xl_Arr.add(priceListArr.get(i).getXl());
			xl_primo_Arr.add(priceListArr.get(i).getXl_primo());

			xv_Arr.add(priceListArr.get(i).getXv());
			xv_primo_Arr.add(priceListArr.get(i).getXv_primo());

			xv_dl_Arr.add(priceListArr.get(i).getXv_dl());
			xv_primo_dl_Arr.add(priceListArr.get(i).getXv_primo_dl());

			xv_prem_dl_Arr.add(priceListArr.get(i).getXv_prem_dl());
			xv_prem_primo_dl_Arr.add(priceListArr.get(i).getXv_prem_primo_dl());


		}*/




		/*int Size =  idArr.size() + statesArr.size() + cityArr.size() + xe_Arr.size() +xe_plus_Arr.size() +xl_Arr.size()+xl_primo_Arr.size()+
				xv_Arr.size() + xv_primo_Arr.size() +  xv_dl_Arr.size() + xv_primo_dl_Arr.size() + xv_prem_dl_Arr.size() + xv_prem_primo_dl_Arr.size();

		Log.d("---", "Micra Database size ========= " +Size);*/



		/*uniqueStateArr			= getStates();
		uniqueCityArr			= getCitysOfStates(uniqueStateArr.get(0));*/


		/*spinner_state.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, uniqueStateArr));
		spinner_city.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, uniqueCityArr));*/

		/*list_state.setAdapter(new ArrayAdapter<String>(context, R.id.text_city_name, uniqueStateArr));
		list_city.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, uniqueCityArr));*/


		/*	state_adapter 	= new StateCityListAdapter(actContext, uniqueStateArr);
		city_adapter 	= new StateCityListAdapter(actContext, uniqueCityArr);

		list_state.setAdapter(state_adapter);
		list_city.setAdapter(city_adapter);


		text_state.setText(uniqueStateArr.get(0));
		text_city.setText(uniqueCityArr.get(0));*/


		/*ADD_VALUES_TO_DATABASE add_Values  = new ADD_VALUES_TO_DATABASE();
		add_Values.execute("");
		 */
		/*setListAdapter(uniqueStateArr.get(0),uniqueCityArr.get(0));*/



	}





	public void setListAdapter(String state, String city){

		modelArr.clear();

		/*modelArr.add(model_xe);
		modelArr.add(model_xe_plus);
		modelArr.add(model_xl);
		modelArr.add(model_xl_primo);
		modelArr.add(model_xv);
		modelArr.add(model_xv_primo);
		modelArr.add(model_xv_prem);
		modelArr.add(model_xv_prem_primo);*/

		modelArr.add("XL");
		modelArr.add("XL(O)");
		modelArr.add("XV CVT");

		modelArr.add("XV");
		modelArr.add("XV(P)");
		modelArr.add("XE");

		modelArr.add("XV(S)");

		//ArrayList<String> tempList = getPetrolPrices(state, city);
		//ArrayList<String> petrolArr = getPetrolPrices(state, city);
		//ArrayList<String> dieselArr = getDieselPrices(state, city);

		ArrayList<String> petrolArr = getPrices("p",state, city);
		ArrayList<String> dieselArr = getPrices("d",state, city);
		ArrayList<String> actArr 	= getPrices("a",state, city);


		price_adapter =  new PriceListAdapter(actContext, modelArr, petrolArr, dieselArr,actArr);
		listView.setAdapter(price_adapter);


	}


	public ArrayList<String> getCitysOfStates(String state){
		ArrayList<String> cityNames  = new ArrayList<String>();

		/*for(int i=0;i<cityArr.size();i++){
			if(state.equalsIgnoreCase(statesArr.get(i))){
				cityNames.add(cityArr.get(i));
			}

		}*/
		cityNames = database.getCitysOfState(state);



		return cityNames;
	}

	public ArrayList<String> getStates(){
		ArrayList<String> stateNames  = new ArrayList<String>();

		/*for(int i=0;i<statesArr.size();i++){
			if(!stateNames.contains(statesArr.get(i))){
				stateNames.add(statesArr.get(i));			
			}	
		}*/

		stateNames  = database.getStates();

		return stateNames;
	}


	public ArrayList<String> getPetrolPrices(String state,String city){
		ArrayList<String> tempArrayList = new ArrayList<String>();
		int num = 0;
		for(int i=0;i<statesArr.size();i++){
			if(   (state.equalsIgnoreCase(statesArr.get(i)))   && (city.equalsIgnoreCase(cityArr.get(i)))  ){
				num = i;
				break;
			}
		}


		tempArrayList.add(xe_Arr.get(num));
		tempArrayList.add(xe_plus_Arr.get(num));
		tempArrayList.add(xl_Arr.get(num));
		tempArrayList.add(xl_primo_Arr.get(num));
		tempArrayList.add(xv_Arr.get(num));
		tempArrayList.add(xv_primo_Arr.get(num));


		for(int i=0;i<2;i++){
			tempArrayList.add("-");
		}


		return tempArrayList;
	}

	public ArrayList<String> getDieselPrices(String state,String city){
		ArrayList<String> tempArrayList = new ArrayList<String>();
		int num = 0;
		for(int i=0;i<statesArr.size();i++){
			if(   (state.equalsIgnoreCase(statesArr.get(i)))   && (city.equalsIgnoreCase(cityArr.get(i)))  ){
				num = i;
				break;
			}
		}

		for(int i=0;i<4;i++){
			tempArrayList.add("-");
		}
		tempArrayList.add(xv_dl_Arr.get(num));
		tempArrayList.add(xv_primo_dl_Arr.get(num));	
		tempArrayList.add(xv_prem_dl_Arr.get(num));
		tempArrayList.add(xv_prem_primo_dl_Arr.get(num));


		return tempArrayList;
	}


	public ArrayList<String> getPrices(String modelType,String stateName, String cityName){
		ArrayList<String> tempPtList = new ArrayList<String>();
		ArrayList<String> tempDlList = new ArrayList<String>();
		ArrayList<String> tempActList = new ArrayList<String>();
		ArrayList<String> tempList = new ArrayList<String>();

		if(modelType.equalsIgnoreCase("p")){
			tempList = database.getPrices("p", stateName, cityName);
			int count = 0;
			for(int i=0;i<7;i++){
				if(i>2){
					tempPtList.add("--");
				}else{
					tempPtList.add(tempList.get(count));
					count = count+1;
				}
			}
			return tempPtList;

		}else if(modelType.equalsIgnoreCase("d")){
			tempList = database.getPrices("d", stateName, cityName);
			int count = 0;
			for(int i=0;i<7;i++){
				if((i==2) || (i>4)){
					tempDlList.add("--");
				}else{
					tempDlList.add(tempList.get(count));
					count = count+1;
				}
			}
			return tempDlList;
		}else{
			tempList = database.getPrices("a", stateName, cityName);
			int count = 0;
			for(int i=0;i<7;i++){
				if((i==1)||(i==2)||(i==4)){
					tempActList.add("--");
				}else{
					tempActList.add(tempList.get(count));
					count = count+1;
				}
			}
			return tempActList;
		}


	}


	class FileFromURL extends AsyncTask<String, String, String> {


		DefaultHttpClient 	httpClient;
		HttpPost 			httpPost;
		HttpResponse		httpRes;
		HttpEntity			httpEnt;


		@Override
		protected String doInBackground(String... xmlur) {
			// TODO Auto-generated method stub
			int count;

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(xmlurl);
				httpRes		=	httpClient.execute(httpPost);
				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	= DocumentBuilderFactory.newInstance();
				DocumentBuilder			db	= dbf.newDocumentBuilder();
				InputSource				is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));

				URL url = new URL(xmlur[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				// getting file length
				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file

				try{
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
				FileOutputStream output = new FileOutputStream(NEONXML);
				byte data[] = new byte[1024];
				long total = 0;
				while (   ((count = input.read(data)) != -1)  ) {
					total += count;
					output.write(data, 0, count);
				}
				output.flush();
				output.close();
				input.close();
			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");

			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) throws NullPointerException {
			// TODO Auto-generated method stub
			try {
				removeDialog(progress_bar_type);
				startParsing();

			} catch(Exception ex){
			}
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showDialog(progress_bar_type);

		}
	}



	class ADD_VALUES_TO_DATABASE extends AsyncTask<String, String, String> {





		@Override
		protected String doInBackground(String... xmlur) {


			/*database.deleteAllRow();

			database.addDetails("Id", idArr,new ArrayList<String>());
			database.addDetails(getResources().getString(R.string.price_state), statesArr,idArr);
			database.addDetails(getResources().getString(R.string.price_city), cityArr,idArr);

			database.addDetails(getResources().getString(R.string.price_xe), xe_Arr,idArr);
			database.addDetails(getResources().getString(R.string.price_xe_plus), xe_plus_Arr,idArr);

			database.addDetails(getResources().getString(R.string.price_xl), xl_Arr,idArr);
			database.addDetails(getResources().getString(R.string.price_xl_primo), xl_primo_Arr,idArr);

			database.addDetails(getResources().getString(R.string.price_xv), xv_Arr,idArr);
			database.addDetails(getResources().getString(R.string.price_xv_primo), xv_primo_Arr,idArr);

			database.addDetails(getResources().getString(R.string.price_dl_xv), xv_dl_Arr,idArr);
			database.addDetails(getResources().getString(R.string.price_dl_xv_primo), xv_primo_dl_Arr,idArr);

			database.addDetails(getResources().getString(R.string.price_dl_xv_prem), xv_prem_dl_Arr,idArr);
			database.addDetails(getResources().getString(R.string.price_dl_xv_prem_primo), xv_prem_primo_dl_Arr,idArr);*/
			database.deleteAllRow();

			try {
				filename	=	new FileInputStream(NEONXML);

				FileChannel fc = filename.getChannel();
				MappedByteBuffer bb = null;

				bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

				String jString = Charset.defaultCharset().decode(bb).toString();

				//JSONObject json = new JSONObject(jString);

				JSONArray jArr	= new JSONArray(jString);
				for(int i =0;i< jArr.length();i++){

					JSONObject tempObj 	= jArr.getJSONObject(i);
					String id 			= tempObj.getString("ID");
					String State 		= tempObj.getString("State");
					String City 		= tempObj.getString("City");
					String PET_XL 		= tempObj.getString("PET_XL");
					String PET_XL_O 	= tempObj.getString("PET_XL_O");
					String PET_XV_CVT 	= tempObj.getString("PET_XV_CVT");
					String DL_XL 		= tempObj.getString("DL_XL");
					String DL_XL_O 		= tempObj.getString("DL_XL_O");
					String DL_XV 		= tempObj.getString("DL_XV");
					String DL_XV_P 		= tempObj.getString("DL_XV_P");
					String ACT_XE 		= tempObj.getString("ACT_XE");
					String ACT_XL 		= tempObj.getString("ACT_XL");
					String ACT_XV 		= tempObj.getString("ACT_XV");
					String ACT_XV_S 	= tempObj.getString("ACT_XV_S");

					ArrayList<String> tempList = new ArrayList<String>();
					tempList.add(id);
					tempList.add(State);
					tempList.add(City);

					tempList.add(PET_XL);
					tempList.add(PET_XL_O);
					tempList.add(PET_XV_CVT);

					tempList.add(DL_XL);
					tempList.add(DL_XL_O);
					tempList.add(DL_XV);
					tempList.add(DL_XV_P);

					tempList.add(ACT_XE);
					tempList.add(ACT_XL);
					tempList.add(ACT_XV);
					tempList.add(ACT_XV_S);


					database.addDetails(coulmnArr, tempList);
				}

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 




			return null;
		}

		@Override
		protected void onPostExecute(String result) throws NullPointerException {
			// TODO Auto-generated method stub
			try {
				removeDialog(progress_bar_type);
				root_layout.setVisibility(View.VISIBLE);
				VALUE_ADDED = true;
				uniqueStateArr			= getStates();
				uniqueCityArr			= getCitysOfStates(uniqueStateArr.get(0));


				state_adapter 	= new StateCityListAdapter(actContext, uniqueStateArr);
				city_adapter 	= new StateCityListAdapter(actContext, uniqueCityArr);

				list_state.setAdapter(state_adapter);
				list_city.setAdapter(city_adapter);


				text_state.setText(uniqueStateArr.get(0));
				text_city.setText(uniqueCityArr.get(0));

				setListAdapter(uniqueStateArr.get(0),uniqueCityArr.get(0));

			} catch(Exception ex){
			}
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showDialog(progress_bar_type);

		}
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setIndeterminate(false);
			pDialog.setMessage("Please wait");
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}



	@Override
	public void onReceiveFileResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		SERVICE_COMPLETED = resultData.getBoolean("downloadCompleteFromService");
	}


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub



		if(SERVICE_COMPLETED){
			MenuItem subMenu2= menu.add("Refresh");
			subMenu2.setIcon(R.drawable.refresh_new1);
			subMenu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}


		SubMenu subMenu1 = menu.addSubMenu("Menu");



		subMenu1.add("Home").setIcon(R.drawable.home);

		subMenu1.add("Features").setIcon(R.drawable.fetures_tick);
		subMenu1.add("Technical Specification").setIcon(R.drawable.tech_spec);
		subMenu1.add("Price List").setIcon(R.drawable.price);
		subMenu1.add("Price List New").setIcon(R.drawable.price);
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);




		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Contact Us")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,ContactUs.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Features")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Features_new.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Technical Specification")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,TechnicalSpecification.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Home")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Home_new.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Price List New")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Price_list_new.class);
			startActivity(intent);
			finish();
		}
		return true;
	}



	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		if(list_city.isShown()){
			list_city.setVisibility(View.GONE);
			//root_layout.setVisibility(View.GONE);
			relative_list_laLayout.setVisibility(View.GONE);

		}else if(list_state.isShown()){
			list_state.setVisibility(View.GONE);
			//root_layout.setVisibility(View.GONE);
			relative_list_laLayout.setVisibility(View.GONE);
		}else{
			super.onBackPressed();	
		}
	}




}
