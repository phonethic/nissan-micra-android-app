package com.phonethics.nissanmicra;

import java.util.ArrayList;

import com.phonethics.adapters.FeaturesAdapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MicraFeaturesFragmentNew extends Fragment {
	
	
	Activity 			context;
	ArrayList<String> 	data;
	ArrayList<String> 	presence;
	ListView			listView;
	FeaturesAdapter		adapter;
	
	public MicraFeaturesFragmentNew(){
		
	}
	
	
	public MicraFeaturesFragmentNew(Activity context, ArrayList<String> data,ArrayList<String> presence){
		this.context = context;
		this.data = data;
		this.presence = presence;
	}
	
	@Override
	public View getView() {
		// TODO Auto-generated method stub
		return super.getView();
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view	=	inflater.inflate(R.layout.fragment_layout_new, container,false);
		listView	= 	(ListView) view.findViewById(R.id.list_featues);
		
		adapter		= 	new FeaturesAdapter(context, data,presence);
		listView.setAdapter(adapter);
		/*listView.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, data));*/		
		return view;
		
	}

	
	
}
