package com.phonethics.nissanmicra;

import java.util.ArrayList;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;

import com.phonethics.database.MicraDataBase;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Features extends SherlockFragmentActivity {

	ListView						list;
	Context							context;
	Activity						actContext;
	MicraDataBase					database;
	ArrayList<String>				featuresArr1;
	ArrayList<String>				temp_data;
	ArrayList<ArrayList<String>> 	featuresArr;
	ArrayList<ArrayList<String>> 	presenceArr;
	ActionBar 						ac;

	boolean					selfTabVisible 		= true;
	String					modelName	=	"Model";
	private 				PagerSlidingTabStrip tabs;
	private 				ViewPager pager;
	private 				MyPagerAdapter adapter;
	int						count;
	
	String					xl_pet 		= "XL_Pet";
	String					xl_dsl 		= "XL_Dsl";
	String					xl_0_pet 	= "XL_O_Pet";
	String					xl_0_dsl 	= "XL_O_Dsl";
	String					xv_cvt 		= "XV_CVT";
	String					xv_dsl 		= "XV_Dsl";
	String					xv_pre_dsl 	= "XV_Premium_Dsl";
	String					tempModel = "";
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_features);


		ac = getSupportActionBar();
		ac.setTitle("Features");

		ac.show();

		context		= this;
		actContext = this;
		tabs 		= (PagerSlidingTabStrip) findViewById(R.id.tabs);
		pager		= (ViewPager) findViewById(R.id.pager);
		list		= (ListView) findViewById(R.id.list_featues);
		
		adapter 	= new MyPagerAdapter(getSupportFragmentManager());
		pager.setAdapter(adapter);

		final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
		pager.setPageMargin(pageMargin);
		tabs.setViewPager(pager);
		
		featuresArr	= new ArrayList<ArrayList<String>>();
		presenceArr	= new ArrayList<ArrayList<String>>();
		
		database 	= new MicraDataBase(context);
		database.create();

		featuresArr.add(database.getExternalFeatures());
		featuresArr.add(database.getComfortFeatures());
		featuresArr.add(database.getInteriorsFeatures());
		featuresArr.add(database.getSafetyFeatures());
		
		temp_data =  database.getModelInfo(xl_pet);
		
		for(int i=0;i<featuresArr.size();i++){
			 ArrayList<String> tempList = new ArrayList<String>();
			for(int p=0;p<featuresArr.get(i).size();p++){
				tempList.add(temp_data.get(count));
				count++;
			}
			presenceArr.add(tempList);
			
		}

		//list.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, featuresArr));		

	}

	
	
	public class MyPagerAdapter extends FragmentPagerAdapter {

		private final String[] TITLES = { "Exterior", "Comfort and Convenience", "Interiors", "Safety" };

		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return TITLES[position];
		}

		@Override
		public int getCount() {
			return TITLES.length;
		}

		@Override
		public Fragment getItem(int position) {
			MicraFeaturesFragment fragment = new MicraFeaturesFragment(actContext, context,featuresArr.get(position),presenceArr,position);
			return fragment;
		}

	}

	void getRefreshedData(){
		count = 0;
		presenceArr = null;
		presenceArr = new ArrayList<ArrayList<String>>();
		for(int i=0;i<featuresArr.size();i++){
			 ArrayList<String> tempList = new ArrayList<String>();
			for(int p=0;p<featuresArr.get(i).size();p++){
				tempList.add(temp_data.get(count));
				count++;
			}
			presenceArr.add(tempList);
		}
		adapter		=	null;
		adapter 	= new MyPagerAdapter(getSupportFragmentManager());
		pager.setAdapter(adapter);
		pager.setCurrentItem(0,true);
	}
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		
		
		

		SubMenu subMenu2 = menu.addSubMenu(modelName + " " + "XL Pet");
		subMenu2.add("XL Pet");
		subMenu2.add("XL Dsl");
		subMenu2.add("XL (O) Pet");
		subMenu2.add("XL (O) Dsl");
		subMenu2.add("XV CVT");
		subMenu2.add("XV Dsl");
		subMenu2.add("XV Premium Dsl");
		
		
	
		MenuItem menu_model = subMenu2.getItem();
		menu_model.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		
		
		
		SubMenu subMenu1 = menu.addSubMenu("Action Item");
		subMenu1.add("Home").setIcon(R.drawable.ic_launcher);
		if(selfTabVisible){
			subMenu1.add("Features").setIcon(R.drawable.ic_launcher);
		}
		subMenu1.add("Technical Specification").setIcon(R.drawable.ic_launcher);


		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		


		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub


		if(item.getTitle().toString().equalsIgnoreCase("Home")){
			Intent intent=new Intent(this,Home.class);
			startActivity(intent);
			finish();
		}
		
		if(item.getTitle().toString().equalsIgnoreCase("XL Dsl")){
			tempModel = xl_dsl;
			temp_data.clear();
			temp_data =  database.getModelInfo(tempModel);
			Toast.makeText(context, "size ", 0).show();
			getRefreshedData();
		}
		if(item.getTitle().toString().equalsIgnoreCase("XL Pet")){
			tempModel = xl_pet;
			temp_data.clear();
			temp_data =  database.getModelInfo(tempModel);
			getRefreshedData();
		}
		if(item.getTitle().toString().equalsIgnoreCase("XL (O) Pet")){
			tempModel = xl_0_pet;
			temp_data.clear();
			temp_data =  database.getModelInfo(tempModel);
			getRefreshedData();
		}
		if(item.getTitle().toString().equalsIgnoreCase("XL (O) Dsl")){
			tempModel = xl_0_dsl;
			temp_data.clear();
			temp_data =  database.getModelInfo(tempModel);
			getRefreshedData();
		}
		if(item.getTitle().toString().equalsIgnoreCase("XV CVT")){
			tempModel = xv_cvt;
			temp_data.clear();
			temp_data =  database.getModelInfo(tempModel);
			getRefreshedData();
		}
		if(item.getTitle().toString().equalsIgnoreCase("XV Dsl")){
			tempModel = xv_dsl;
			temp_data.clear();
			temp_data =  database.getModelInfo(tempModel);
			getRefreshedData();
		}
		if(item.getTitle().toString().equalsIgnoreCase("XV Premium Dsl")){
			tempModel = xv_pre_dsl;
			temp_data.clear();
			temp_data =  database.getModelInfo(tempModel);
			getRefreshedData();
		}
		


		return true;
	}



}
