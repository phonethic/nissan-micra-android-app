package com.phonethics.nissanmicra;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Bitmap.Config;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;

import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.aphidmobile.flip.FlipViewController;
import com.flurry.android.FlurryAgent;
import com.phonethics.adapters.FlipAdapter;
import com.phonethics.customclass.NeonGalleryLinks;
import com.phonethics.customclass.NissanGroupsAndLinks;
import com.phonethics.imageloader.ImageLoader;
import com.phonethics.network.NetworkCheck;
import com.phonethics.nissanmicra.Home.FileFromURL;
import com.phonethics.parser.SAXXMLParser;
import com.phonethics.pref.Share;
import com.phonethics.services.DownloadReceiver;
import com.phonethics.services.DownloadReceiver.DownloadResult;
import com.phonethics.services.DownloadService;

public class Home_new extends SherlockFragmentActivity implements DownloadResult {

	Context			context;
	Activity		actContext;

	Intent intent2;


	boolean			LAUNCH_FIRST_TIME, SERVICE_COMPLETED = false,SERVICE_CALLED_ONCE = false,NEW_DATA = false;;
	String 			xml,NEONXML		="/sdcard/neoncache/micra_new.xml";
	//String 			xmlurl			= "http://stage.phonethics.in/proj/neon/micra_gallery.php";
	String 			xmlurl			= "http://stage.phonethics.in/proj/neon/micra_gallery1.php";
	FileInputStream	filename;

	int             SCREEN_WIDTH,IMAGE_HIGHT,randomInt;
	int             ANIMATE_DURATION = 3000,x=0,x0=0,x1=0,x2=0,x5=0,x6=0,x7=0,x8=0,x9=0,x10=0,x11=0,y=0,y1=0;
	int 			VISIBLE_PICS = 2;
	private static long SLEEP_TIME = (long) 1;

	ArrayList<NeonGalleryLinks> 	neongallery;
	ArrayList<String> 				link,caption,description,carName,vlinks,groups,groupsData,captionData,tempGroup,thumbnailArr,orderArr;
	ArrayList<Integer> 				countArr;
	ArrayList<ArrayList<String>> 	allLinks,allNames,allCaption,allThumbnails,allThumbnailsCopy,allDescription,allVLinks,allGrps;
	ArrayList<ImageView>			imgview_arr,imgview_arr_1;
	ArrayList<Animation> 			animArr;
	ArrayList<FlipViewController> 	flipView_arr;
	Random 							randomGenerator;
	//ArrayList<Animation> 			animArr;	

	Animation 						anim_push_up,anim_push_down,anim_from_right,anim_from_left,anim_fade_in,anim_fade_out,anim1;
	Animation						temp_fade_in,temp_fade_out;

	LinearLayout					mainLayout;

	Handler							handler_pos_1,hanler_pos_2;
	Runnable						runnable_pos_1,flipRunnable;

	Share					sharePref;
	NetworkCheck			netAvailable;
	ActionBar 				ac;
	FileFromURL 			downloadFile;
	NissanGroupsAndLinks	nissanGrpsLinks;
	ImageLoader				imgloader;
	DownloadReceiver		receiver;
	FlipAdapter				adapter;
	DialogThread 			thread;


	public static final int 		progress_bar_type = 0; 
	private ProgressDialog 			pDialog;
	boolean							useFlurry;

	Typeface 				font;
	TypefaceSpan			typeFaceSpan;
	SpannableString 		sHome,sFeatures,sTechnicalSpecification,sPriceList,sContact;
	boolean					usePager = false;
	Map<String, String> 	articleParams;
	String					app_color="";

	ArrayList<String> galallLinks, galallDescription, galallCaption, galallVLinks, galallNames;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		context 		= this;
		actContext		= this;

		FlurryAgent.logEvent("Home_Tab_Event");

		// Service

		/*	intent2=new Intent(context, DownloadService.class);
		intent2.putExtra("downloadFile",receiver);
		intent2.putExtra("URL", xmlurl);
		intent2.putExtra("FILENAME", NEONXML);*/

		font 		= Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf");
		LayoutInflater inflater 		= LayoutInflater.from(this);
		View customView 				= inflater.inflate(R.layout.action_bar_title_text, null);
		TextView textTitle				= (TextView) customView.findViewById(R.id.text_action_title);
		textTitle.setText("Nissan Micra");
		textTitle.setTypeface(font);

		ac 				= getSupportActionBar();
		ac.setCustomView(customView);
		ac.setDisplayShowTitleEnabled(true);
		ac.setDisplayShowCustomEnabled(false);
		ac.setTitle("NISSAN MICRA");
		ac.show();

		//Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		app_color = getResources().getString(R.string.app_color);
		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#"+app_color));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });
		ac.setBackgroundDrawable(ld);

		articleParams = new HashMap<String, String>();

		sHome						= new SpannableString("Home");
		sFeatures					= new SpannableString("Features");
		sTechnicalSpecification		= new SpannableString("TechnicalSpecification");
		sPriceList					= new SpannableString("PriceList");
		sContact					= new SpannableString("Contact");



		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.phonethics.nissanmicra", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.i("MY KEY HASH:",
						"MY KEY HASH:"+Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

			e.printStackTrace();

		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		/*	sHome.setSpan(new TypefaceSpan(context, "n_ex____.ttf"), 0, sHome.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		sFeatures.setSpan(new TypefaceSpan(context, "n_ex____.ttf"), 0, sFeatures.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		sTechnicalSpecification.setSpan(new TypefaceSpan(context, "n_ex____.ttf"), 0, sTechnicalSpecification.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		sPriceList.setSpan(new TypefaceSpan(context, "n_ex____.ttf"), 0, sPriceList.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		sContact.setSpan(new TypefaceSpan(context, "n_ex____.ttf"), 0, sContact.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);*/


		sharePref 		= new Share(context);
		netAvailable 	= new NetworkCheck(context);
		receiver		= new DownloadReceiver(new Handler());
		receiver.setReceiver(this);


		mainLayout			= (LinearLayout) findViewById(R.id.main_layout);

		link				= new ArrayList<String>();
		description			= new ArrayList<String>();
		carName				= new ArrayList<String>();
		vlinks				= new ArrayList<String>();
		groups				= new ArrayList<String>();
		groupsData			= new ArrayList<String>();
		caption				= new ArrayList<String>();
		captionData			= new ArrayList<String>();
		tempGroup			= new ArrayList<String>();
		thumbnailArr		= new ArrayList<String>();
		orderArr			= new ArrayList<String>();
		countArr			= new ArrayList<Integer>();

		allLinks 			= new ArrayList<ArrayList<String>>();
		allNames 			= new ArrayList<ArrayList<String>>();
		allCaption			= new ArrayList<ArrayList<String>>();
		allThumbnails		= new ArrayList<ArrayList<String>>();
		allThumbnailsCopy	= new ArrayList<ArrayList<String>>();
		allDescription		= new ArrayList<ArrayList<String>>();
		allVLinks			= new ArrayList<ArrayList<String>>();
		allGrps				= new ArrayList<ArrayList<String>>();

		imgview_arr			= new ArrayList<ImageView>();
		imgview_arr_1		= new ArrayList<ImageView>();
		animArr				= new ArrayList<Animation>();


		// arraylist for gallery specific 

		galallLinks = new ArrayList<String>();
		galallDescription = new ArrayList<String>();
		galallCaption = new ArrayList<String>();
		galallVLinks = new ArrayList<String>();
		galallNames  = new ArrayList<String>();

		pDialog 			= new ProgressDialog(this);

		nissanGrpsLinks 	= new NissanGroupsAndLinks();
		imgloader 			= new ImageLoader(context);
		flipView_arr		= new ArrayList<FlipViewController>();

		flipView_arr.add(new FlipViewController(context, FlipViewController.VERTICAL));


		randomGenerator 	= new Random();	

		anim_push_up		= AnimationUtils.loadAnimation(context, R.anim.push_up_in);
		anim_push_down		= AnimationUtils.loadAnimation(context, R.anim.push_down_in);
		anim_from_left		= AnimationUtils.loadAnimation(context, R.anim.slide_anim_from_left);
		anim_from_right		= AnimationUtils.loadAnimation(context, R.anim.slide_anim_from_right);

		anim_fade_in		= AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
		anim_fade_out		= AnimationUtils.loadAnimation(context, R.anim.shrink_fade_out_center);

		temp_fade_in = anim_fade_in;
		temp_fade_out = anim_fade_out;

		useFlurry			= Boolean.parseBoolean(getResources().getString(R.string.useflurry));

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		SCREEN_WIDTH = metrics.widthPixels;
		if(SCREEN_WIDTH == 480){
			IMAGE_HIGHT = (int) ((SCREEN_WIDTH/1.5)-5);
		}else if(SCREEN_WIDTH == 800){
			IMAGE_HIGHT = (int) ((SCREEN_WIDTH/1.5));
		}else{
			IMAGE_HIGHT = (int) ((SCREEN_WIDTH/1.5));
			IMAGE_HIGHT = IMAGE_HIGHT;
		}

		setSharePrefrences();
		if(LAUNCH_FIRST_TIME && isInternetPresent()){

			downloadFile();
			if(usePager){
				setPagerLayout();
			}else{
				setLayout();
			}

		}else if(isFilePresent()){
			//Toast.makeText(context, "File present", 0).show();
			try {
				filename	=	new FileInputStream(NEONXML);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			neongallery	=	SAXXMLParser.parse(filename);

			addData();	
			setLinksAndGroups();
			printData();
			if(usePager){
				setPagerLayout();

			}else{
				setLayout();
				if(getAppOpen()){
					sharePref.setAppClose(false);
					startService();
				}
				addAnimations();
				applyAnim(true);
			}

			//applyFlipAnimation();

		}else if(isInternetPresent()){
			downloadFile();
			if(usePager){
				setPagerLayout();
			}else{
				setLayout();
			}
		}else{

			Toast.makeText(context, "No Internet Connection", 0).show();
		}


		for(int i=0;i<allGrps.size();i++){

			for(int j=0;j<allGrps.get(i).size();j++){

				if(allGrps.get(i).get(j).toString().equalsIgnoreCase("Gallery")){
					//Log.d("ALLGRPS","ALLGRPS " + allLinks.get(i).get(i));



					galallCaption.add(allCaption.get(i).get(j));
					galallDescription.add(allDescription.get(i).get(j));
					galallLinks.add(allLinks.get(i).get(j));
					galallVLinks.add(allVLinks.get(i).get(j));
					galallNames.add(allNames.get(i).get(j));

				}

			}

		}
	}


	public void setSharePrefrences(){
		LAUNCH_FIRST_TIME = sharePref.isAppLaunchFirstTime();
		if(LAUNCH_FIRST_TIME){
			sharePref.setLaunchFirstTime(false);
		}
	}

	public boolean getAppOpen(){
		return sharePref.isAppOpen();
	}


	public boolean isInternetPresent(){
		return netAvailable.isNetworkAvailable();
	}


	public void downloadFile(){
		downloadFile	=	new FileFromURL();
		downloadFile.execute(xmlurl);
	}

	public void addData(){

		link.clear();
		description.clear();
		vlinks.clear();
		carName.clear();
		caption.clear();
		thumbnailArr.clear();
		groupsData.clear();
		orderArr.clear();



		try{
			for(int i=0; i<neongallery.size();i++){
				link.add(neongallery.get(i).getLINK());
				description.add(neongallery.get(i).getDESCRIPTION());
				vlinks.add(neongallery.get(i).getVLINK());
				carName.add(neongallery.get(i).getCARRNAME());
				caption.add(neongallery.get(i).getCAPTION());
				thumbnailArr.add(neongallery.get(i).getThumbnail());
				groupsData.add(neongallery.get(i).getGROUP());
				orderArr.add(neongallery.get(i).getOrder());
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Log.d("!!!!!!!!!!!!!!!", "Exception - add data");
		}

	}

	public void setLinksAndGroups(){

		nissanGrpsLinks = new NissanGroupsAndLinks();
		try{
			for(int i=0; i<orderArr.size();i++){

				String	grpStr 			= groupsData.get(i);
				String	carNameStr		= carName.get(i);



				//nissanGrpsLinks.setGroup(grpStr);
				nissanGrpsLinks.setOrder(orderArr.get(i));

				if(i==groupsData.size()-1){
					nissanGrpsLinks.setLastLink(true);
				}
				nissanGrpsLinks.setGroup(grpStr);
				nissanGrpsLinks.setCaption(caption.get(i));	
				nissanGrpsLinks.setLink(link.get(i));
				nissanGrpsLinks.setCarName(carNameStr);
				nissanGrpsLinks.setThumbnail(thumbnailArr.get(i));
				nissanGrpsLinks.setDescription(description.get(i));
				nissanGrpsLinks.setvLink(vlinks.get(i));




			}

			for(int i=0;i<carName.size();i++){

				Log.d("CARNAME", "CARNAME" + carName.get(i));
			}

		}catch(Exception ex){
			ex.printStackTrace();
			Log.d("!!!!!!!!!!!!!!!", "Exception - setLinksAndGroups");
		}
	}

	public void printData(){
		try{
			allThumbnails.clear();
			allCaption.clear();
			allNames.clear();
			allDescription.clear();
			allVLinks.clear();
			allLinks.clear();
			orderArr.clear();
			allGrps.clear();
			allThumbnailsCopy.clear();
			countArr.clear();
			imgview_arr.clear();
			imgview_arr_1.clear();
			flipView_arr.clear();

			//flipView_arr.add(new FlipViewController(context, FlipViewController.VERTICAL));
			mainLayout.removeAllViews();

			allThumbnails 	= nissanGrpsLinks.getAllThumbnails();
			allCaption 		= nissanGrpsLinks.getAllCaption();
			allNames		= nissanGrpsLinks.getAllNames();
			allDescription  = nissanGrpsLinks.getAlldescription();
			allVLinks		= nissanGrpsLinks.getAllvlink();
			allLinks		= nissanGrpsLinks.getAllLinks();
			allGrps			= nissanGrpsLinks.getAllGrp();



			//			for(int j=0;j<orderArr.size();j++){
			//			
			for(int i=0;i<allNames.size();i++){

				Log.d("allNames", "allNames " + allNames.get(i) + "\n");
			}
			//}


			orderArr.clear();
			orderArr = nissanGrpsLinks.getOrderArr();
			Log.d("Message", "==== order arr "+ orderArr.size() );
			Log.d("Message", "==== thumbnail arr "+ allThumbnails.size() );
			for(int i=0;i<allThumbnails.size();i++){
				ArrayList<String> tempList = allThumbnails.get(i);
				for(int p=0;p<tempList.size();p++){
					Log.d(">>>>", ">>>> url "+i+"--"+p+" >>> " +tempList.get(p));
				}
			}
			allThumbnails.remove(0);
			allCaption.remove(0);
			allNames.remove(0);
			allDescription.remove(0);
			allVLinks.remove(0);
			allLinks.remove(0);
			allGrps.remove(0);
			/*allThumbnailsCopy = allThumbnails;*/
			/*for(int i =0;i<allThumbnails.size();i++){
			allThumbnailsCopy.add(allThumbnails.get(i));
		}*/
			allThumbnailsCopy.addAll(allThumbnails);
			Log.d("Message", "==== thumbnail arr size "+ allThumbnails.size() +" copu arr size "+allThumbnailsCopy.size() );
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	public void addAnimations(){
		for(int i=0;i<orderArr.size();i++){
			animArr.add(anim_push_up);
			animArr.add(anim_push_down);
			animArr.add(anim_from_right);
			animArr.add(anim_from_left);
			animArr.add(anim_fade_in);
			//animArr.add(anim_fade_out);
		}
	}


	public void setLayout(){

		try{
			LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT,1f);
			params1.setMargins(2, 2, 2, 0);


			for(int i=0;i<allGrps.size();i++){

				final int position = i;

				LinearLayout 	childLinear		= new LinearLayout(this);
				RelativeLayout 	childRelative_1 = new RelativeLayout(this);
				childRelative_1.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT));

				//if(i!=orderArr.size()-1){
				countArr.add(i, 0);
				ImageView	 	childImage1	= new ImageView(this);
				ImageView 		childImage2	= new ImageView(this);

				childImage1.setId(1);
				childImage2.setId(2);
				childImage1.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT));
				childImage2.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT));
				childImage1.setScaleType(ScaleType.FIT_XY);
				childImage2.setScaleType(ScaleType.FIT_XY);
				imgview_arr_1.add(childImage1);
				imgview_arr.add(childImage2);

				RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
				//par.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
				par.addRule(RelativeLayout.BELOW,childImage1.getId());
				par.addRule(RelativeLayout.BELOW,childImage2.getId());

				final TextView			childtextView1 = new TextView(this);
				childtextView1.setLayoutParams(par);

				childtextView1.setGravity(Gravity.CENTER);
				childtextView1.setBackgroundColor(Color.parseColor("#555555"));
				childtextView1.setTextSize(22);
				String text = allGrps.get(i).get(0).toString().toUpperCase();
				//childtextView1.getBackground().setAlpha(220);
				childtextView1.setPadding(2, 2, 2, 2);
				childtextView1.setText(text);
				childtextView1.setTextColor(Color.parseColor("#FFFFFF"));
				Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf");
				childtextView1.setTypeface(font);

				imgloader.DisplayImage(allThumbnails.get(i).get(0), childImage2);

				childRelative_1.addView(childImage1);
				childRelative_1.addView(childImage2);
				childRelative_1.addView(childtextView1);

				childLinear.addView(childRelative_1, params1);
				mainLayout.addView(childLinear);

				childImage2.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						//Toast.makeText(context, "Position " + position, 0).show();
						if(useFlurry){
							Map<String, String> articleParams1 = new HashMap<String, String>();
							articleParams1.put("Section", allGrps.get(position).toString().toUpperCase());
							FlurryAgent.logEvent("Home_Tab_Detail_Event",articleParams1);
						}


						Intent intent = new Intent(context, NissanCarGallery.class);
						sharePref.setAppClose(false);
						ArrayList<String> thumbnails = allLinks.get(position);
						//Toast.makeText(context, "Size "+thumbnails.size(), 0).show();
						intent.putStringArrayListExtra("imgArr", allLinks.get(position));
						intent.putStringArrayListExtra("imgDesc", allDescription.get(position));
						intent.putStringArrayListExtra("imgCaption", allCaption.get(position));
						intent.putStringArrayListExtra("vlinksArr", allVLinks.get(position));
						intent.putStringArrayListExtra("allNames", allNames.get(position));
						startActivity(intent);
						finish();

					}
				});
				//		//	}else{
				//				ArrayList<String> tempArrayList = allThumbnails.get(i);
				//				FlipViewController flipView = new FlipViewController(context, FlipViewController.VERTICAL);
				//				flipView.setAnimationBitmapFormat(Config.RGB_565);
				//				flipView.setId(i);
				//				flipView.setLayoutParams(new RelativeLayout.LayoutParams(720,IMAGE_HIGHT));
				//				adapter = new FlipAdapter(actContext, context, tempArrayList,allDescription.get(position), allCaption.get(position),allVLinks.get(position));
				//				flipView.setAdapter(adapter);
				//
				//				flipView_arr.add(flipView);
				//				//Toast.makeText(actContext, text, duration)
				//				Log.d("-----------", "Micra Flip View Arr Size "+flipView_arr.size() );
				//				RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
				//				//par.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
				//				par.addRule(RelativeLayout.BELOW,flipView.getId());
				//				//par.addRule(RelativeLayout.BELOW,childImage2.getId());
				//
				//				final TextView			childtextView1 = new TextView(this);
				//				childtextView1.setLayoutParams(par);
				//
				//				childtextView1.setGravity(Gravity.CENTER);
				//				childtextView1.setBackgroundColor(Color.parseColor("#FFFFFF"));
				//				childtextView1.setTextSize(22);
				//				//childtextView1.getBackground().setAlpha(220);
				//				childtextView1.setPadding(2, 2, 2, 2);
				//				childtextView1.setText(allCaption.get(i).get(0).toString());
				//				childtextView1.setTextColor(Color.parseColor("#c71444"));
				//
				//				//imgloader.DisplayImage(allThumbnails.get(i).get(0), childImage2);
				//
				//				childRelative_1.addView(flipView);
				//				childRelative_1.addView(childtextView1);
				//				childLinear.addView(childRelative_1, params1);
				//				mainLayout.addView(childLinear);
				//
				//
				//
				//
				//			}
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}



	public void setPagerLayout(){
		LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT,1f);
		params1.setMargins(2, 2, 2, 0);
		for(int i=0;i<orderArr.size();i++){
			LinearLayout 	childLinear		= new LinearLayout(this);
			RelativeLayout 	childRelative_1 = new RelativeLayout(this);
			childRelative_1.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT));

			//if(i!=orderArr.size()-1){
			countArr.add(i, 0);
			ImageView	img = new ImageView(context);
			img.setId(0);
			img.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT));
			//img.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT));


			RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
			//par.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			par.addRule(RelativeLayout.BELOW,img.getId());
			//par.addRule(RelativeLayout.BELOW,img.getId());

			final TextView			childtextView1 = new TextView(this);
			childtextView1.setLayoutParams(par);

			childtextView1.setGravity(Gravity.CENTER);
			childtextView1.setBackgroundColor(Color.parseColor("#555555"));
			childtextView1.setTextSize(22);
			String text = allCaption.get(i).get(0).toString().toUpperCase();
			//childtextView1.getBackground().setAlpha(220);
			childtextView1.setPadding(2, 2, 2, 2);
			childtextView1.setText(text);
			childtextView1.setTextColor(Color.parseColor("#FFFFFF"));
			Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf");
			childtextView1.setTypeface(font);


			LinearLayout 	childLinear1		= new LinearLayout(this);
			childLinear1.setOrientation(LinearLayout.HORIZONTAL);
			ImageView	img1 = new ImageView(context);
			ImageView	img2 = new ImageView(context);

			img1.setLayoutParams(new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT/2,1f));
			img2.setLayoutParams(new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,IMAGE_HIGHT/2,1f));

			imgloader.DisplayImage(allThumbnails.get(i).get(0), img);
			imgloader.DisplayImage(allThumbnails.get(i).get(0), img1);
			imgloader.DisplayImage(allThumbnails.get(i).get(0), img2);

			childRelative_1.addView(img);
			childRelative_1.addView(childLinear1);
			childRelative_1.addView(childtextView1);

			childLinear.addView(childRelative_1, params1);
			mainLayout.addView(childLinear);
		}
	}


	public boolean isFilePresent(){

		try{
			File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
			if(!myDirectory.exists()) {                                 
				myDirectory.mkdirs();
			}
			File xmlFile1	=	new File(NEONXML);
			if(xmlFile1.exists()){

				return true;
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}
		return false;
	}


	public void startService(){

		int DELAY = 3000;


		if(isInternetPresent()){
			if(!SERVICE_CALLED_ONCE){
				//Toast.makeText(context, "service", 0).show();
				//xmlurl = "http://stage.phonethics.in/proj/neon/neon_gallery1.php";
				SERVICE_CALLED_ONCE = true;
				/*	if(pDialog!=null){
					try{
						showDialog(progress_bar_type);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}*/
				Intent intent2=new Intent(context, DownloadService.class);
				intent2.putExtra("downloadFile",receiver);
				intent2.putExtra("URL", xmlurl);
				intent2.putExtra("FILENAME", NEONXML);
				context.startService(intent2);
			}
		}










	}


	public void applyAnim(boolean apply){

		if(apply){

			x2=0;


			handler_pos_1 = new Handler();
			runnable_pos_1 = new Runnable() {

				@Override
				public void run() {

					// TODO Auto-generated method stub
					try{
						Log.d("=============", " ========= loop "+x2+" ============");
						for(int i=0;i<countArr.size();i++){
							Log.d("===", "Count === "+countArr.get(i));
						}

						for(int i=0;i<allGrps.size();i++){
							final int x1 = i;
							x = countArr.get(x1);
							randomInt = randomGenerator.nextInt(animArr.size());

							anim1 = animArr.get(i);
							final ArrayList<String> tempArrayList = new ArrayList<String>();
							//if(i!=0){
							//							if(!(allThumbnails.get(i).size()<=VISIBLE_PICS)){
							//								for(int p=0;p<VISIBLE_PICS+1;p++){
							//									tempArrayList.add(allThumbnails.get(i).get(p));
							//								}
							//							}else{
							//								tempArrayList.addAll(allThumbnails.get(i));
							//							}

							for(int p=0;p<allThumbnails.get(i).size();p++){
								tempArrayList.add(allThumbnails.get(i).get(p));
							}

							//}
							Log.i("=========", "Index "+i + " Size "+tempArrayList.size() + " Cont Value "+countArr.get(x1) );
							imgloader.DisplayImage(tempArrayList.get(x), imgview_arr.get(x1));
							imgview_arr.get(x1).startAnimation(anim1);
							anim1.setFillAfter(true);
							anim1.setAnimationListener(new AnimationListener() {

								@Override
								public void onAnimationStart(Animation animation) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onAnimationRepeat(Animation animation) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onAnimationEnd(Animation animation) {
									// TODO Auto-generated method stub
									x5++;
									x = countArr.get(x1);
									x = x+1;
									if(x==tempArrayList.size()){
										x=0;		
									}
									countArr.set(x1, x);
									if(x!=0){
										imgloader.DisplayImage(tempArrayList.get(x-1), imgview_arr_1.get(x1));
									}else if(x==0){
										imgloader.DisplayImage(tempArrayList.get(tempArrayList.size()-1), imgview_arr_1.get(x1));
									}
								}
							});
						}
						x2++;
						handler_pos_1.postDelayed(runnable_pos_1,3000);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}


			};
			handler_pos_1.postDelayed(runnable_pos_1,2500);
		}else{
			handler_pos_1.removeCallbacks(runnable_pos_1);
		}

		if(apply){
			hanler_pos_2 = new Handler();
			flipRunnable =  new Runnable() {
				public void run() {
					Animation anim = anim_push_up;
					final ArrayList<String> tempArrayList = new ArrayList<String>();
					tempArrayList.addAll(allThumbnails.get(0));

					imgloader.DisplayImage(tempArrayList.get(y1), imgview_arr.get(0));
					imgview_arr.get(0).startAnimation(anim);
					anim.setFillAfter(true);
					anim.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							y1++;
							if(y1==tempArrayList.size()){
								y1=0;
							}
							if(y1!=0){
								imgloader.DisplayImage(tempArrayList.get(y1-1), imgview_arr_1.get(0));
							}else if(y1==0){
								imgloader.DisplayImage(tempArrayList.get(tempArrayList.size()-1), imgview_arr_1.get(0));
							}

						}
					});
					hanler_pos_2.postDelayed(flipRunnable, 3000);
				}
				
			};
			hanler_pos_2.postDelayed(flipRunnable,2500);
		}else{
			hanler_pos_2.removeCallbacks(flipRunnable);
		}
	}


	void applyFlipAnimation(){

		//Toast.makeText(context, ""+flipView_arr.size(), 0).show();

		hanler_pos_2 = new Handler();
		flipRunnable = new Runnable() {


			final ArrayList<String> tempArrayList = allThumbnails.get(allThumbnails.size()-1);
			int p = 0 ;
			@Override
			public void run() {


				try{
					// TODO Auto-generated method stub
					if(p>=tempArrayList.size()-1) {
						/*if(flipView.getLocalVisibleRect(scrollBounds)){
						p = 0;    
						flipView.setSelection(p);
						flipView.autoFlip(); 
					}
					p = 0;    
				flipView.setSelection(p);
				flipView.autoFlip(); 
				}else{
					if(flipView.getLocalVisibleRect(scrollBounds)){

						flipView.autoFlip(); 
					}
					flipView.autoFlip();
				}*/
						p = 0; 
						try{
							if(!SERVICE_CALLED_ONCE){
								flipView_arr.get(0).setSelection(p);
								flipView_arr.get(0).autoFlip();
							}else{
								flipView_arr.get(1).setSelection(p);
								flipView_arr.get(1).autoFlip();
							}


							//flipView_arr.get(1).setSelection(p);
							//flipView_arr.get(1).autoFlip();


						}catch(Exception ex){
							ex.printStackTrace();
						}
					}else{
						try{
							if(!SERVICE_CALLED_ONCE){
								flipView_arr.get(0).autoFlip();
							}else{
								flipView_arr.get(1).autoFlip();
							}


							//flipView_arr.get(1).autoFlip();

						}catch(Exception ex){
							ex.printStackTrace();
						}
					}
					p++;
					hanler_pos_2.postDelayed(this, 3000);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		};
		hanler_pos_2.postDelayed(flipRunnable,0);
	}

	private class DialogThread extends Thread {
		/*ProgressDialog mProgressDialog = new ProgressDialog(context);*/
		public Handler mHandler;

		@Override
		/**
		 * Sleep for some time and than start new activity.
		 */

		public void run() {
			//Looper.prepare();

			/*try {
				Thread.sleep(SLEEP_TIME*1000);
			} catch (Exception e) {
				e.printStackTrace();
			}*/

			//Looper.loop();
			if(pDialog!=null){
				pDialog.dismiss();
				/*removeDialog(progress_bar_type);*/
				//pDialog = null;
			}
			tempMethod();
		}
	}	



	void tempMethod(){
		try {
			runOnUiThread(new Runnable() {
				public void run() {

					thread 		= null;	
					try {
						filename	=	new FileInputStream(NEONXML);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					neongallery	=	SAXXMLParser.parse(filename);

					addData();	
					setLinksAndGroups();
					printData();
					if(usePager){
						setPagerLayout();
					}else{
						setLayout();
						applyAnim(true);
					}
					//startService();
					//addAnimations();

					//Toast.makeText(context, "New Data", 0).show();
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}


	public void cancelHandlers(){

		if(handler_pos_1!=null){


			handler_pos_1.removeCallbacks(runnable_pos_1);
			imgloader.clearCache();
		}
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		//applyAnimation(true);
		applyAnim(true);
	}	


	class FileFromURL extends AsyncTask<String, String, String> {


		DefaultHttpClient 	httpClient;
		HttpPost 			httpPost;
		HttpResponse		httpRes;
		HttpEntity			httpEnt;


		@Override
		protected String doInBackground(String... xmlur) {
			// TODO Auto-generated method stub
			int count;

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(xmlurl);
				httpRes		=	httpClient.execute(httpPost);
				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	= DocumentBuilderFactory.newInstance();
				DocumentBuilder			db	= dbf.newDocumentBuilder();
				InputSource				is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));

				URL url = new URL(xmlur[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				// getting file length
				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file

				try{
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
				FileOutputStream output = new FileOutputStream(NEONXML);
				byte data[] = new byte[1024];
				long total = 0;
				while (   ((count = input.read(data)) != -1)  ) {
					total += count;
					output.write(data, 0, count);
				}
				output.flush();
				output.close();
				input.close();
			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");

			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			try {
				removeDialog(progress_bar_type);
				filename = new FileInputStream(NEONXML);
				neongallery = SAXXMLParser.parse(filename);

				addData();	
				setLinksAndGroups();
				printData();
				if(usePager){
					setPagerLayout();
				}else{
					setLayout();
					addAnimations();
					applyAnim(true);
				}

				//applyFlipAnimation();

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(NullPointerException npx){
				npx.printStackTrace();
			}
			catch(Exception ex){
			}
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showDialog(progress_bar_type);

		}
	}


	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			if(!((Activity) context).isFinishing()){
				try{
					pDialog = new ProgressDialog(this);
					pDialog.setIndeterminate(false);
					pDialog.setMessage("Please wait");
					pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
					pDialog.setCancelable(false);
					pDialog.show();
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
			return pDialog;
		default:
			return null;
		}
	}


	@Override
	public void onReceiveFileResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		SERVICE_COMPLETED = resultData.getBoolean("downloadCompleteFromService");
		//Toast.makeText(context, "service completed "+ SERVICE_COMPLETED , 0).show();

		try{
			if(isFilePresent()){
				//Toast.makeText(context, "File present", 0).show();
				//			try {
				//				showDialog(progress_bar_type);
				//				Toast.makeText(context, "Show Dialog", 0).show();
				//				filename	=	new FileInputStream(NEONXML);
				//
				//				//neongallery.clear();
				//				neongallery	=	SAXXMLParser.parse(filename);
				//
				//				addData();	
				//				setLinksAndGroups();
				//				printData();
				//				setLayout();
				//				//startService();
				//				//addAnimations();
				//				applyAnim(true);
				//				removeDialog(progress_bar_type);
				//				//applyFlipAnimation();
				//			} catch (FileNotFoundException e) {
				//				// TODO Auto-generated catch block
				//				e.printStackTrace();
				//			}
				//			
				//mainLayout.removeAllViews();

				thread = new DialogThread();
				thread.start();




			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		//getSherlock().dispatchInvalidateOptionsMenu();
	}


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub




		if(SERVICE_COMPLETED){
			MenuItem subMenu2= menu.add("Refresh");
			subMenu2.setIcon(R.drawable.refresh_new1);
			subMenu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}


		SubMenu subMenu1 = menu.addSubMenu("Menu");


		if(true){


			subMenu1.add("Home").setIcon(R.drawable.home);


		}
		subMenu1.add("Features").setIcon(R.drawable.fetures_tick);


		subMenu1.add("Technical Specification").setIcon(R.drawable.tech_spec);
		//subMenu1.add("Price List").setIcon(R.drawable.price);
		subMenu1.add("Price List").setIcon(R.drawable.price);
		subMenu1.add("Test Drive").setIcon(R.drawable.test_drive_new);
		subMenu1.add("Gallery").setIcon(R.drawable.gallery);
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);
		//subMenu1.add("Splash").setIcon(R.drawable.contact);
		//subMenu1.add("Home Pager").setIcon(R.drawable.home);




		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Contact Us")){
			sharePref.setAppClose(false);
			Intent intent=new Intent(this,ContactUs.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();


			/*Intent intent = new Intent(context, SplashNew.class);
			startActivity(intent);
			finish();*/
		}
		if(item.getTitle().toString().equalsIgnoreCase("Features")){
			sharePref.setAppClose(false);
			Intent intent=new Intent(this,Features_new.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Technical Specification")){
			sharePref.setAppClose(false);
			Intent intent=new Intent(this,TechnicalSpecification.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Price List")){
			sharePref.setAppClose(false);
			Intent intent=new Intent(this,Price_list_new.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}

		if(item.getTitle().toString().equalsIgnoreCase("Home Pager")){
			sharePref.setAppClose(false);
			Intent intent=new Intent(this,HomePager.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Test Drive")){
			sharePref.setAppClose(false);
			Intent intent=new Intent(this,TestDrive.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Gallery")){
			sharePref.setAppClose(false);

			FlurryAgent.logEvent("Gallery_Tab_Event");
			Intent intent=new Intent(this,NissanCarGallery.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}

		/*if(item.getTitle().toString().equalsIgnoreCase(sHome.g)){
			Toast.makeText(context, "Well Done", 0);
		}*/
		if(item.getTitle().toString().equalsIgnoreCase("Refresh")){
			/*if(netAvailable.isNetworkAvailable()){
				File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
				if(!myDirectory.exists()) {                                 
					myDirectory.mkdirs();
				}

				File xmlFile1	=	new File(NEONXML);
				downloadFile	=	new FileFromURL();
				downloadFile.execute(xmlurl);

			}else{
				Toast.makeText(context, "Check Internet connection", Toast.LENGTH_SHORT).show();
			}*/


		}
		/*if(item.getTitle().toString().equalsIgnoreCase("Traffic Rules")){
			Intent intent=new Intent(this,TrafficSigns.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Find My Car")){
			Intent intent=new Intent(this,Nissan_Map.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Reminders")){
			Intent intent=new Intent(this,Remeinders.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("My Car")){
			Intent intent=new Intent(this,MycarActivity.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Tips")){
			Intent intent=new Intent(this,Tips.class);
			startActivity(intent);
			finish();
		}	
		 */

		return true;
	}

	public class PagerAdapter extends FragmentPagerAdapter {


		ArrayList<String> imgUrls;

		public PagerAdapter(FragmentManager fm, ArrayList<String> imgUrls) {
			super(fm);
			this.imgUrls = imgUrls;
		}

		@Override
		public CharSequence getPageTitle(int position) {

			return "";
		}

		@Override
		public int getCount() {
			return imgUrls.size();
		}

		@Override
		public Fragment getItem(int position) {

			HomePagerFragment fragment= new HomePagerFragment(actContext, imgUrls.get(position));
			return fragment;

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		//share.setAppClose(true);
		if(pDialog.isShowing()){
			removeDialog(progress_bar_type);
		}
		sharePref.setAppClose(true);
		this.finish();
	}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){


			FlurryAgent.onStartSession(this, getResources().getString(R.string.flurry_id));

		}

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		cancelHandlers();
		if(useFlurry){
			FlurryAgent.onEndSession(this);
		}


	}


	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		//		stopService(intent2);
	}











}
