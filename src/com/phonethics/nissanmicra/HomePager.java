package com.phonethics.nissanmicra;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.phonethics.adapters.FlipAdapter;
import com.phonethics.adapters.HomePagerAdapter;
import com.phonethics.customclass.NeonGalleryLinks;
import com.phonethics.customclass.NissanGroupsAndLinks;
import com.phonethics.imageloader.ImageLoader;
import com.phonethics.network.NetworkCheck;

import com.phonethics.nissanmicra.Home_new.FileFromURL;

import com.phonethics.parser.SAXXMLParser;
import com.phonethics.pref.Share;
import com.phonethics.services.DownloadReceiver;
import com.phonethics.services.DownloadService;
import com.phonethics.services.DownloadReceiver.DownloadResult;

public class HomePager extends SherlockFragmentActivity implements DownloadResult {
	
	Context					context;
	Activity				actContext;
	
	NetworkCheck			netAvailable;
	ActionBar 				ac;
	FileFromURL 			downloadFile;
	NissanGroupsAndLinks	nissanGrpsLinks;
	ImageLoader				imgloader;
	DownloadReceiver		receiver;
	FlipAdapter				adapter;
	DialogThread 			thread;
	Share					sharePref;
	
	ListView				listView;
	
	ArrayList<NeonGalleryLinks> 	neongallery;
	ArrayList<ArrayList<String>> 	allLinks,allNames,allCaption,allThumbnails,allThumbnailsCopy,allDescription,allVLinks;
	ArrayList<String> 				link,caption,description,carName,vlinks,groups,groupsData,captionData,tempGroup,thumbnailArr,orderArr;
	boolean							LAUNCH_FIRST_TIME, SERVICE_COMPLETED = false,SERVICE_CALLED_ONCE = false,NEW_DATA = false;;
	String 							xml,NEONXML		="/sdcard/neoncache/micra.xml";
	String 							xmlurl			= "http://stage.phonethics.in/proj/neon/micra_gallery.php";
	FileInputStream					filename;
	public static final int 		progress_bar_type = 0; 
	private ProgressDialog 			pDialog;
	boolean							useFlurry;
	FragmentManager					 fm;
	HomePagerAdapter				pagerListAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_pager);
		
		context 		= this;
		actContext		= this;
		
		LayoutInflater inflater 		= LayoutInflater.from(this);
	    View customView 				= inflater.inflate(R.layout.action_bar_title_text, null);
		TextView textTitle				= (TextView) customView.findViewById(R.id.text_action_title);
		textTitle.setText("Nissan Micra");
		
		
		ac 				= getSupportActionBar();
		ac.setCustomView(customView);
		ac.setDisplayShowTitleEnabled(false);
		ac.setDisplayShowCustomEnabled(true);
		ac.show();

		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });
		ac.setBackgroundDrawable(ld);
		
		sharePref 		= new Share(context);
		netAvailable 	= new NetworkCheck(context);
		receiver		= new DownloadReceiver(new Handler());
		receiver.setReceiver(this);
		
		listView			= (ListView) findViewById(R.id.list_homepager);
		fm					= getSupportFragmentManager();
		
		link				= new ArrayList<String>();
		description			= new ArrayList<String>();
		carName				= new ArrayList<String>();
		vlinks				= new ArrayList<String>();
		groups				= new ArrayList<String>();
		groupsData			= new ArrayList<String>();
		caption				= new ArrayList<String>();
		captionData			= new ArrayList<String>();
		tempGroup			= new ArrayList<String>();
		thumbnailArr		= new ArrayList<String>();
		orderArr			= new ArrayList<String>();

		allLinks 			= new ArrayList<ArrayList<String>>();
		allNames 			= new ArrayList<ArrayList<String>>();
		allCaption			= new ArrayList<ArrayList<String>>();
		allThumbnails		= new ArrayList<ArrayList<String>>();
		allThumbnailsCopy	= new ArrayList<ArrayList<String>>();
		allDescription		= new ArrayList<ArrayList<String>>();
		allVLinks			= new ArrayList<ArrayList<String>>();
		
		nissanGrpsLinks 	= new NissanGroupsAndLinks();
		
		useFlurry			= Boolean.parseBoolean(getResources().getString(R.string.useflurry));
		
		setSharePrefrences();
		/*if(LAUNCH_FIRST_TIME && isInternetPresent()){

			downloadFile();
			//setLayout();
			setAdapter();

		}else */if(isFilePresent()){
			//Toast.makeText(context, "File present", 0).show();
			try {
				filename	=	new FileInputStream(NEONXML);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			neongallery	=	SAXXMLParser.parse(filename);

			addData();	
			setLinksAndGroups();
			printData();
			//setLayout();
			setAdapter();
			//startService();
			//addAnimations();
			//applyAnim(true);
			//applyFlipAnimation();

		}else if(isInternetPresent()){
			downloadFile();
			//setLayout();
			setAdapter();
		}else{

			Toast.makeText(context, "No Internet Connection", 0).show();
		}
	}
	
	
	public void setSharePrefrences(){
		LAUNCH_FIRST_TIME = sharePref.isAppLaunchFirstTime();
		if(LAUNCH_FIRST_TIME){
			sharePref.setLaunchFirstTime(false);
		}
	}

	public boolean getAppOpen(){
		return sharePref.isAppOpen();
	}


	public boolean isInternetPresent(){
		return netAvailable.isNetworkAvailable();
	}


	public void downloadFile(){
		downloadFile	=	new FileFromURL();
		downloadFile.execute(xmlurl);
	}
	
	public void addData(){

		link.clear();
		description.clear();
		vlinks.clear();
		carName.clear();
		caption.clear();
		thumbnailArr.clear();
		groupsData.clear();
		orderArr.clear();


		try{
			for(int i=0; i<neongallery.size();i++){
				link.add(neongallery.get(i).getLINK());
				description.add(neongallery.get(i).getDESCRIPTION());
				vlinks.add(neongallery.get(i).getVLINK());
				carName.add(neongallery.get(i).getCARRNAME());
				caption.add(neongallery.get(i).getCAPTION());
				thumbnailArr.add(neongallery.get(i).getThumbnail());
				groupsData.add(neongallery.get(i).getGROUP());
				orderArr.add(neongallery.get(i).getOrder());
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Log.d("!!!!!!!!!!!!!!!", "Exception - add data");
		}

	}
	
	public void setLinksAndGroups(){

		nissanGrpsLinks = new NissanGroupsAndLinks();
		try{
			for(int i=0; i<orderArr.size();i++){

				String	grpStr 			= groupsData.get(i);
				String	carNameStr		= carName.get(i);

				//nissanGrpsLinks.setGroup(grpStr);
				nissanGrpsLinks.setOrder(orderArr.get(i));

				if(i==groupsData.size()-1){
					nissanGrpsLinks.setLastLink(true);
				}
				nissanGrpsLinks.setCaption(caption.get(i));	
				nissanGrpsLinks.setLink(link.get(i));
				nissanGrpsLinks.setCarName(carNameStr);
				nissanGrpsLinks.setThumbnail(thumbnailArr.get(i));
				nissanGrpsLinks.setDescription(description.get(i));
				nissanGrpsLinks.setvLink(vlinks.get(i));




			}
		}catch(Exception ex){
			ex.printStackTrace();
			Log.d("!!!!!!!!!!!!!!!", "Exception - setLinksAndGroups");
		}
	}
	
	
	public void printData(){
		try{
			allThumbnails.clear();
			allCaption.clear();
			allNames.clear();
			allDescription.clear();
			allVLinks.clear();
			allLinks.clear();
			orderArr.clear();
			allThumbnailsCopy.clear();
			
			//flipView_arr.add(new FlipViewController(context, FlipViewController.VERTICAL));
			

			allThumbnails 	= nissanGrpsLinks.getAllThumbnails();
			allCaption 		= nissanGrpsLinks.getAllCaption();
			allNames		= nissanGrpsLinks.getAllNames();
			allDescription  = nissanGrpsLinks.getAlldescription();
			allVLinks		= nissanGrpsLinks.getAllvlink();
			allLinks		= nissanGrpsLinks.getAllLinks();


			orderArr.clear();
			orderArr = nissanGrpsLinks.getOrderArr();
			Log.d("Message", "==== order arr "+ orderArr.size() );
			Log.d("Message", "==== thumbnail arr "+ allThumbnails.size() );
			for(int i=0;i<allThumbnails.size();i++){
				ArrayList<String> tempList = allThumbnails.get(i);
				for(int p=0;p<tempList.size();p++){
					Log.d(">>>>", ">>>> url "+i+"--"+p+" >>> " +tempList.get(p));
				}
			}
			allThumbnails.remove(0);
			allCaption.remove(0);
			allNames.remove(0);
			allDescription.remove(0);
			allVLinks.remove(0);
			allLinks.remove(0);
			/*allThumbnailsCopy = allThumbnails;*/
			/*for(int i =0;i<allThumbnails.size();i++){
			allThumbnailsCopy.add(allThumbnails.get(i));
		}*/
			allThumbnailsCopy.addAll(allThumbnails);
			Log.d("Message", "==== thumbnail arr size "+ allThumbnails.size() +" copu arr size "+allThumbnailsCopy.size() );
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}
	
	public void startService(){

		int DELAY = 3000;

	
		if(isInternetPresent()){
			if(!SERVICE_CALLED_ONCE){
				//Toast.makeText(context, "service", 0).show();
				
				SERVICE_CALLED_ONCE = true;
				Intent intent2=new Intent(context, DownloadService.class);
				intent2.putExtra("downloadFile",receiver);
				intent2.putExtra("URL", xmlurl);
				intent2.putExtra("FILENAME", NEONXML);
				context.startService(intent2);
			}
		}
	}
	
	public boolean isFilePresent(){

		File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
		if(!myDirectory.exists()) {                                 
			myDirectory.mkdirs();
		}
		File xmlFile1	=	new File(NEONXML);
		if(xmlFile1.exists()){

			return true;
		}
		return false;
	}

	
	public void setAdapter(){
		pagerListAdapter = new HomePagerAdapter(actContext, fm, orderArr, allLinks, allNames, allCaption, allThumbnails, allThumbnailsCopy, allDescription, allVLinks);
		listView.setAdapter(pagerListAdapter);
	}
	
	
	private class DialogThread extends Thread {
		/*ProgressDialog mProgressDialog = new ProgressDialog(context);*/
		public Handler mHandler;

		@Override
		/**
		 * Sleep for some time and than start new activity.
		 */

		public void run() {
			//Looper.prepare();

			try {
				Thread.sleep(3*1000);
			} catch (Exception e) {
				e.printStackTrace();
			}

			//Looper.loop();
			removeDialog(progress_bar_type);
			tempMethod();
		}
	}	



	void tempMethod(){
		try {
			runOnUiThread(new Runnable() {
				public void run() {

					thread 		= null;	
					try {
						filename	=	new FileInputStream(NEONXML);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					neongallery	=	SAXXMLParser.parse(filename);

					addData();	
					setLinksAndGroups();
					printData();
					//setLayout();
					setAdapter();
					//startService();
					//addAnimations();
					//applyAnim(true);
					//Toast.makeText(context, "New Data", 0).show();
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	
	class FileFromURL extends AsyncTask<String, String, String> {


		DefaultHttpClient 	httpClient;
		HttpPost 			httpPost;
		HttpResponse		httpRes;
		HttpEntity			httpEnt;


		@Override
		protected String doInBackground(String... xmlur) {
			// TODO Auto-generated method stub
			int count;

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(xmlurl);
				httpRes		=	httpClient.execute(httpPost);
				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	= DocumentBuilderFactory.newInstance();
				DocumentBuilder			db	= dbf.newDocumentBuilder();
				InputSource				is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));

				URL url = new URL(xmlur[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				// getting file length
				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file

				try{
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
				FileOutputStream output = new FileOutputStream(NEONXML);
				byte data[] = new byte[1024];
				long total = 0;
				while (   ((count = input.read(data)) != -1)  ) {
					total += count;
					output.write(data, 0, count);
				}
				output.flush();
				output.close();
				input.close();
			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");

			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			try {
				removeDialog(progress_bar_type);
				filename = new FileInputStream(NEONXML);
				neongallery = SAXXMLParser.parse(filename);

				addData();	
				setLinksAndGroups();
				printData();
				//setLayout();
				setAdapter();
				//addAnimations();
				//applyAnim(true);
				//applyFlipAnimation();

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(NullPointerException npx){
				npx.printStackTrace();
			}
			catch(Exception ex){
			}
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showDialog(progress_bar_type);

		}
	}


	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setIndeterminate(false);
			pDialog.setMessage("Please wait");
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}



	@Override
	public void onReceiveFileResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		SERVICE_COMPLETED = resultData.getBoolean("downloadCompleteFromService");
		if(isFilePresent()){
			showDialog(progress_bar_type);
			thread = new DialogThread();
			thread.start();
		}
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub


		
		
		if(SERVICE_COMPLETED){
			MenuItem subMenu2= menu.add("Refresh");
			subMenu2.setIcon(R.drawable.refresh_new1);
			subMenu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}


		SubMenu subMenu1 = menu.addSubMenu("Menu");


		if(true){
		
			
			subMenu1.add("Home").setIcon(R.drawable.home);
			
			
		}
		subMenu1.add("Features").setIcon(R.drawable.fetures_tick);
		
		
		subMenu1.add("Technical Specification").setIcon(R.drawable.tech_spec);
		//subMenu1.add("Price List").setIcon(R.drawable.price);
		subMenu1.add("Price List").setIcon(R.drawable.price);
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);
		subMenu1.add("Home Pager").setIcon(R.drawable.home);
		



		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Contact Us")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,ContactUs.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Features")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Features_new.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Technical Specification")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,TechnicalSpecification.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Price List")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Price_list_new.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Home Pager")){
			//share.setAppClose(false);
			
		}
		/*if(item.getTitle().toString().equalsIgnoreCase("Price List New")){
			//share.setAppClose(false);
			Intent intent=new Intent(this,Price_list_new.class);
			startActivity(intent);
			finish();
		}*/
		/*if(item.getTitle().toString().equalsIgnoreCase(sHome.g)){
			Toast.makeText(context, "Well Done", 0);
		}*/
		if(item.getTitle().toString().equalsIgnoreCase("Refresh")){
			/*if(netAvailable.isNetworkAvailable()){
				File myDirectory = new File(Environment.getExternalStorageDirectory(), "neoncache");
				if(!myDirectory.exists()) {                                 
					myDirectory.mkdirs();
				}

				File xmlFile1	=	new File(NEONXML);
				downloadFile	=	new FileFromURL();
				downloadFile.execute(xmlurl);

			}else{
				Toast.makeText(context, "Check Internet connection", Toast.LENGTH_SHORT).show();
			}*/


		}
		/*if(item.getTitle().toString().equalsIgnoreCase("Traffic Rules")){
			Intent intent=new Intent(this,TrafficSigns.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Find My Car")){
			Intent intent=new Intent(this,Nissan_Map.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Reminders")){
			Intent intent=new Intent(this,Remeinders.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("My Car")){
			Intent intent=new Intent(this,MycarActivity.class);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Tips")){
			Intent intent=new Intent(this,Tips.class);
			startActivity(intent);
			finish();
		}	
		 */

		return true;
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		//share.setAppClose(true);
		this.finish();
	}

	

}
