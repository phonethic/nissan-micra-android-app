package com.phonethics.nissanmicra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.flurry.android.FlurryAgent;
import com.phonethics.adapters.TechnicalListAdapter;
import com.phonethics.database.MicraDataBase;

public class TechnicalSpecification extends SherlockActivity implements OnNavigationListener {

	Context							context;
	Activity						actContext;
	MicraDataBase					database;
	ArrayList<String>				featuresArr1PetrolDiesel,featuresArrActive,car1Data,car2Data,car3Data,car4Data,car5Data;
	TextView						text_1,text_2,text_3,text_4,text_5,text_feture;
	ListView						tech_list;
	Button							bttn_diff,bttn_similaraties;

	TechnicalListAdapter			adapter;
	public static boolean			isTechModelPetrol		= true;
	public static boolean			isTechModelActive		= false;
	public static boolean			showDifference = false;
	public static boolean			hideSimilaraties = false;

	String					xl_pet 		= "XL_Pet";
	String					xl_dsl 		= "XL_Dsl";
	String					xl_0_pet 	= "XL_O_Pet";
	String					xl_0_dsl 	= "XL_O_Dsl";
	String					xv_cvt 		= "XV_CVT";
	String					xv_dsl 		= "XV_Dsl";
	String					xv_pre_dsl 	= "XV_Premium_Dsl";
	String					tempModel 	= "Petrol";

	String					model_type_petrol = "Petrol";
	String					model_type_diesel = "Diesel";
	String					model_type_active = "Active";

	String					petrol_xl		= "XL";
	String					petrol_xl_0		= "XL(0)";
	String					petrol_xv		= "XV";

	String					diesel_xe		= "XE";
	String					diesel_xl		= "XL";
	String					diesel_xl_0		= "XL(0)";
	String					diesel_xv		= "XV";
	String					diesel_xv_pre	= "XV(P)";

	String					active_e0	= "XE";
	String					active_e1	= "XL";
	String					active_e2	= "XV";
	String					active_e2_safe	= "XV(S)";

	ActionBar 				ac;
	boolean					useFlurry;
	Typeface 				font;
	Map<String, String> 	articleParams;
	String					app_color="";
	ArrayAdapter<CharSequence> model_list;
	
	int count = 0;
	
	ArrayList<String> galallLinks, galallDescription, galallCaption, galallVLinks, galallNames;
	
	Bundle bundle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Nissan_style);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_technical_specification);


		context 		= this;
		actContext		= this;

		font 		= Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf");
		LayoutInflater inflater 		= LayoutInflater.from(this);
		View customView 				= inflater.inflate(R.layout.action_bar_title_text, null);
		TextView textTitle				= (TextView) customView.findViewById(R.id.text_action_title);
		textTitle.setText("Technical Specification");
		textTitle.setTypeface(font);

		ac 				= getSupportActionBar();
		ac.setCustomView(customView);
		ac.setDisplayShowTitleEnabled(true);
		ac.setDisplayShowCustomEnabled(false);
		ac.setTitle("Technical Specification");
		ac.show();
		
		
	// Getting gallery data
		
		bundle = getIntent().getExtras();
		if(bundle!=null){
			
			galallLinks = bundle.getStringArrayList("imgArr");
			galallDescription	= bundle.getStringArrayList("imgDesc");
			galallCaption = bundle.getStringArrayList("imgCaption");
			galallVLinks = bundle.getStringArrayList("vlinksArr");
			galallNames = bundle.getStringArrayList("allNames");
			//Toast.makeText(context, ""+imgLinks.size(), 0).show();
		}
		
		Context acontext = getSupportActionBar().getThemedContext();
		model_list = ArrayAdapter.createFromResource(acontext, R.array.mdoel_type, R.layout.list_menus);
		model_list.setDropDownViewResource(R.layout.list_dropdown_item);


		//Setting Navigation Type.

		ac.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		
		ac.setListNavigationCallbacks(model_list, this);

		//Drawable colorDrawable = new ColorDrawable(Color.parseColor("#c71444"));
		app_color = getResources().getString(R.string.app_color);
		Drawable colorDrawable = new ColorDrawable(Color.parseColor("#"+app_color));
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		ac.setBackgroundDrawable(ld);

		text_1		= (TextView) findViewById(R.id.text_1);
		text_2		= (TextView) findViewById(R.id.text_2);
		text_3		= (TextView) findViewById(R.id.text_3);
		text_4		= (TextView) findViewById(R.id.text_4);
		text_5		= (TextView) findViewById(R.id.text_5);
		text_feture	= (TextView) findViewById(R.id.text_feature_content);

		text_1.setTypeface(font);
		text_2.setTypeface(font);
		text_3.setTypeface(font);
		text_4.setTypeface(font);
		text_5.setTypeface(font);
		text_feture.setTypeface(font);


		tech_list			= (ListView) findViewById(R.id.list_technical);
		View footerView		= ((LayoutInflater) actContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_view, null, false);
		TextView textView	= (TextView) footerView.findViewById(R.id.text_list_footer);
		textView.setText(getResources().getString(R.string.text_footer_condition));
		textView.setTypeface(font);
		tech_list.addFooterView(footerView);
		
		
		

		setTextData();
		car4Data 	= new ArrayList<String>();
		car5Data 	= new ArrayList<String>();
		database	= new MicraDataBase(context);
		articleParams		= new HashMap<String, String>();
		bttn_diff			= (Button) findViewById(R.id.bttn_diff);
		bttn_similaraties 	= (Button) findViewById(R.id.bttn_similar);
		bttn_similaraties.setTypeface(font);
		bttn_diff.setTypeface(font);
		useFlurry			= Boolean.parseBoolean(getResources().getString(R.string.useflurry));

		if(savedInstanceState!=null){
			car1Data = savedInstanceState.getStringArrayList("car1");
			car2Data = savedInstanceState.getStringArrayList("car2");
			car3Data = savedInstanceState.getStringArrayList("car3");
			car4Data = savedInstanceState.getStringArrayList("car4");
			car5Data = savedInstanceState.getStringArrayList("car5");
			featuresArr1PetrolDiesel = savedInstanceState.getStringArrayList("fetures_petrol");
			featuresArrActive = savedInstanceState.getStringArrayList("fetures_active");
		}else{
			featuresArr1PetrolDiesel 	= database.getTechFetures();
			featuresArrActive			= database.getActiveTechFetures();
			car1Data	= database.getTechModelInfo(xl_pet);
			car2Data	= database.getTechModelInfo(xl_0_pet);
			car3Data	= database.getTechModelInfo(xv_cvt);
		}
		adapter = new TechnicalListAdapter(actContext, featuresArr1PetrolDiesel, car1Data, car2Data, car3Data, car4Data,car5Data,showDifference);
		tech_list.setAdapter(adapter);

		

		bttn_diff.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*if(showDifference){
					showDifference = false;
				}else{
					showDifference = true;
				}*/
				
				count++;
				
				if(count%2==0){
					
					showDifference = false;
					bttn_diff.setText("Show Differences");
				}
				else{
					
					showDifference = true;
					bttn_diff.setText("Hide Differences");
				}
				
				hideSimilaraties = false;
				adapter.notifyDataSetChanged();


			}
		});

		bttn_similaraties.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDifference = false;
				if(hideSimilaraties){
					hideSimilaraties = false;
					bttn_similaraties.setText("Hide Similarities");
					getNewData(false);
					//adapter.notifyDataSetChanged();
				}else{
					hideSimilaraties = true;
					bttn_similaraties.setText("Show Similarities");
					chechkForSimilaraties();
					adapter.notifyDataSetChanged();
				}

			}
		});

	}

	public void setTextData(){
		if(isTechModelPetrol){
			text_1.setText(petrol_xl);
			text_2.setText(petrol_xl_0);
			text_3.setText(petrol_xv);
			text_4.setVisibility(View.GONE);
			text_5.setVisibility(View.GONE);
		}else if(isTechModelActive){
			text_1.setText(active_e0);
			text_2.setText(active_e1);
			text_3.setText(active_e2);
			text_4.setText(active_e2_safe);
			text_4.setVisibility(View.VISIBLE);
			text_5.setVisibility(View.GONE);
		}else{
			text_1.setText(diesel_xe);
			text_2.setText(diesel_xl);
			text_3.setText(diesel_xl_0);
			text_4.setText(diesel_xv);
			text_5.setText(diesel_xv_pre);
			text_4.setVisibility(View.VISIBLE);
			text_5.setVisibility(View.VISIBLE);
		}
	}


	public void getNewData(boolean animApply){

		car1Data.clear();
		car2Data.clear();
		car3Data.clear();
		car4Data.clear();
		car5Data.clear();
		featuresArr1PetrolDiesel.clear();
		featuresArrActive.clear();

		if(isTechModelPetrol){
			car1Data	= database.getTechModelInfo(xl_pet);
			car2Data	= database.getTechModelInfo(xl_0_pet);
			car3Data	= database.getTechModelInfo(xv_cvt);
			car4Data	= new ArrayList<String>();
			car5Data	= new ArrayList<String>();
			featuresArr1PetrolDiesel 	= database.getTechFetures();

			adapter = new TechnicalListAdapter(actContext, featuresArr1PetrolDiesel, car1Data, car2Data, car3Data, car4Data,car5Data,animApply);
			tech_list.setAdapter(adapter);


		}else if(isTechModelActive){
			car1Data	= database.getActiveTechModelInfo(active_e0);
			car2Data	= database.getActiveTechModelInfo(active_e1);
			car3Data	= database.getActiveTechModelInfo(active_e2);
			car4Data	= database.getActiveTechModelInfo("XV_S");
			car5Data	= new ArrayList<String>();
			featuresArrActive			= database.getActiveTechFetures();

			adapter = new TechnicalListAdapter(actContext, featuresArrActive, car1Data, car2Data, car3Data, car4Data,car5Data,animApply);
			tech_list.setAdapter(adapter);

		}else{

			car1Data	= database.getTechModelInfo("XE_Dsl");
			car2Data	= database.getTechModelInfo(xl_dsl);
			car3Data	= database.getTechModelInfo(xl_0_dsl);
			car4Data	= database.getTechModelInfo(xv_dsl);
			car5Data	= database.getTechModelInfo(xv_pre_dsl);
			featuresArr1PetrolDiesel 	= database.getTechFetures();

			adapter = new TechnicalListAdapter(actContext, featuresArr1PetrolDiesel, car1Data, car2Data, car3Data, car4Data,car5Data,animApply);
			tech_list.setAdapter(adapter);

		}
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub

	/*	SubMenu subMenu2 = menu.addSubMenu(" "+tempModel+"");
		subMenu2.add(model_type_petrol);
		subMenu2.add(model_type_diesel);
		subMenu2.add(model_type_active);

		MenuItem menu_model = subMenu2.getItem();
		menu_model.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);*/

		SubMenu subMenu1 = menu.addSubMenu("Menu");
		subMenu1.add("Home").setIcon(R.drawable.home);
		subMenu1.add("Features").setIcon(R.drawable.fetures_tick);
		if(true){
			subMenu1.add("Technical Specification").setIcon(R.drawable.tech_spec);
		}
		subMenu1.add("Price List").setIcon(R.drawable.price);
		subMenu1.add("Test Drive").setIcon(R.drawable.test_drive_new);
		subMenu1.add("Gallery").setIcon(R.drawable.gallery);
		subMenu1.add("Contact Us").setIcon(R.drawable.contact);


		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.ic_action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);



		return true;
	}


	void chechkForSimilaraties(){
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";

		ArrayList<String> tempCar1Data = new ArrayList<String>();
		ArrayList<String> tempCar2Data = new ArrayList<String>();
		ArrayList<String> tempCar3Data = new ArrayList<String>();
		ArrayList<String> tempCar4Data = new ArrayList<String>();
		ArrayList<String> tempCar5Data = new ArrayList<String>();
		ArrayList<String> listData		 = new ArrayList<String>();


		for(int i=0;i<car1Data.size();i++){

			if(isTechModelPetrol){	// for model petrol 

				a = car1Data.get(i);
				b = car2Data.get(i);
				c = car3Data.get(i);

				if(   (a.equalsIgnoreCase(b)) && (a.equalsIgnoreCase(c))  ){
					Log.d("--", "Micra Petrol Same " + a +" " + b + " "+c);
				}else{
					tempCar1Data.add(a);
					tempCar2Data.add(b);
					tempCar3Data.add(c);

					listData.add(featuresArr1PetrolDiesel.get(i));
				}

			}else if(isTechModelActive){ // for model active

				a = car1Data.get(i);
				b = car2Data.get(i);
				c = car3Data.get(i);
				d = car4Data.get(i);

				if(   (a.equalsIgnoreCase(b)) && (a.equalsIgnoreCase(c)) && (a.equalsIgnoreCase(d))   ){
					Log.d("--", "Micra Active Same " + a +" " + b + " "+c);
				}else{ 

					tempCar1Data.add(a);
					tempCar2Data.add(b);
					tempCar3Data.add(c);
					tempCar4Data.add(d);
					listData.add(featuresArrActive.get(i));
				}

			}else{ // for model diesel

				a = car1Data.get(i);
				b = car2Data.get(i);
				c = car3Data.get(i);
				d = car4Data.get(i);
				e = car5Data.get(i);


				if(   (a.equalsIgnoreCase(b)) && (a.equalsIgnoreCase(c)) && (a.equalsIgnoreCase(d)) && (a.equalsIgnoreCase(e))   ){
					Log.d("--", "Micra Diesel Same " + a +" " + b + " "+c+" "+e);
				}else{
					tempCar1Data.add(a);
					tempCar2Data.add(b);
					tempCar3Data.add(c);
					tempCar4Data.add(d);
					tempCar5Data.add(e);
					listData.add(featuresArr1PetrolDiesel.get(i));
				}

			}
		}

		car1Data.clear();
		car2Data.clear();
		car3Data.clear();
		car4Data.clear();
		car5Data.clear();

		car1Data.addAll(tempCar1Data);
		car2Data.addAll(tempCar2Data);
		car3Data.addAll(tempCar3Data);
		car4Data.addAll(tempCar4Data);
		car5Data.addAll(tempCar5Data);

		if(isTechModelPetrol){
			featuresArr1PetrolDiesel.clear();
			featuresArr1PetrolDiesel.addAll(listData);			
		}else if(isTechModelActive){
			featuresArrActive.clear();
			featuresArrActive.addAll(listData);
		}else{
			featuresArr1PetrolDiesel.clear();
			featuresArr1PetrolDiesel.addAll(listData);
		}



	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		if(outState.isEmpty()){
			outState.putStringArrayList("car1", car1Data);
			outState.putStringArrayList("car2", car2Data);
			outState.putStringArrayList("car3", car3Data);
			outState.putStringArrayList("car4", car4Data);
			outState.putStringArrayList("car5", car5Data);
			outState.putStringArrayList("fetures_petrol", featuresArr1PetrolDiesel);
			outState.putStringArrayList("fetures_active", featuresArrActive);

			//context = getApplicationContext();
		}
	}


	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		if(savedInstanceState!=null){
			car1Data = savedInstanceState.getStringArrayList("car1");
			car2Data = savedInstanceState.getStringArrayList("car2");
			car3Data = savedInstanceState.getStringArrayList("car3");
			car4Data = savedInstanceState.getStringArrayList("car4");
			car5Data = savedInstanceState.getStringArrayList("car5");
			featuresArr1PetrolDiesel = savedInstanceState.getStringArrayList("fetures_petrol");
			featuresArrActive = savedInstanceState.getStringArrayList("fetures_active");
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Contact Us")){
			//share.setAppClose(false);
			isTechModelPetrol		= true;
			isTechModelActive		= false;
			showDifference 			= false;
			hideSimilaraties		= false;
			Intent intent=new Intent(this,ContactUs.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Features")){
			//share.setAppClose(false);
			isTechModelPetrol		= true;
			isTechModelActive		= false;
			showDifference 			= false;
			hideSimilaraties		= false;
			Intent intent=new Intent(this,Features_new.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Home")){
			//share.setAppClose(false);
			isTechModelPetrol		= true;
			isTechModelActive		= false;
			showDifference 			= false;
			hideSimilaraties		= false;
			Intent intent=new Intent(this,Home_new.class);
			startActivity(intent);
			finish();
		}

		if(item.getTitle().toString().equalsIgnoreCase(model_type_diesel)){
			tempModel = "Diesel";
			if(useFlurry){
				articleParams.put("Model Type", tempModel);
				FlurryAgent.logEvent("Technical_Specifications_Tab_Event", articleParams);
			}
			isTechModelPetrol		= false;
			isTechModelActive		= false;
			setTextData();
			getNewData(true);

			getSherlock().dispatchInvalidateOptionsMenu();
		}
		if(item.getTitle().toString().equalsIgnoreCase(model_type_petrol)){
			tempModel = "Petrol";
			if(useFlurry){
				articleParams.put("Model Type", tempModel);
				FlurryAgent.logEvent("Technical_Specifications_Tab_Event", articleParams);
			}
			isTechModelPetrol		= true;
			isTechModelActive		= false;
			setTextData();
			getNewData(true);
			getSherlock().dispatchInvalidateOptionsMenu();
		}
		if(item.getTitle().toString().equalsIgnoreCase(model_type_active)){
			tempModel = "Active";
			if(useFlurry){
				articleParams.put("Model Type", tempModel);
				FlurryAgent.logEvent("Technical_Specifications_Tab_Event", articleParams);
			}
			isTechModelPetrol		= false;
			isTechModelActive		= true;
			setTextData();
			getNewData(true);
			getSherlock().dispatchInvalidateOptionsMenu();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Price List")){
			//share.setAppClose(false);
			showDifference 			= false;
			hideSimilaraties		= false;
			Intent intent=new Intent(this,Price_list_new.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Test Drive")){
			//share.setAppClose(false);
			showDifference 			= false;
			hideSimilaraties		= false;
			Intent intent=new Intent(this,TestDrive.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		if(item.getTitle().toString().equalsIgnoreCase("Gallery")){
			//share.setAppClose(false);
			showDifference 			= false;
			hideSimilaraties		= false;
			FlurryAgent.logEvent("Gallery_Tab_Event");
			Intent intent=new Intent(this,NissanCarGallery.class);
			intent.putStringArrayListExtra("imgArr", galallLinks);
			intent.putStringArrayListExtra("imgDesc", galallDescription);
			intent.putStringArrayListExtra("imgCaption", galallCaption);
			intent.putStringArrayListExtra("vlinksArr", galallVLinks);
			intent.putStringArrayListExtra("allNames", galallNames);
			startActivity(intent);
			finish();
		}
		return true;
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(useFlurry){
			FlurryAgent.onStartSession(this, getResources().getString(R.string.flurry_id));
			articleParams.put("Model Type", tempModel);
			FlurryAgent.logEvent("Technical_Specifications_Tab_Event", articleParams);
		}

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(useFlurry){
			FlurryAgent.onEndSession(this);

		}
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		if(model_list.getItem(itemPosition).toString().equalsIgnoreCase(model_type_diesel)){
			tempModel = "Diesel";
			if(useFlurry){
				articleParams.put("Model Type", tempModel);
				FlurryAgent.logEvent("Technical_Specifications_Tab_Event", articleParams);
			}
			isTechModelPetrol		= false;
			isTechModelActive		= false;
			setTextData();
			getNewData(true);

			//getSherlock().dispatchInvalidateOptionsMenu();
		}
		if(model_list.getItem(itemPosition).toString().equalsIgnoreCase(model_type_petrol)){
			tempModel = "Petrol";
			if(useFlurry){
				articleParams.put("Model Type", tempModel);
				FlurryAgent.logEvent("Technical_Specifications_Tab_Event", articleParams);
			}
			isTechModelPetrol		= true;
			isTechModelActive		= false;
			setTextData();
			getNewData(true);
			//getSherlock().dispatchInvalidateOptionsMenu();
		}
		if(model_list.getItem(itemPosition).toString().equalsIgnoreCase(model_type_active)){
			tempModel = "Active";
			if(useFlurry){
				articleParams.put("Model Type", tempModel);
				FlurryAgent.logEvent("Technical_Specifications_Tab_Event", articleParams);
			}
			isTechModelPetrol		= false;
			isTechModelActive		= true;
			setTextData();
			getNewData(true);
			//getSherlock().dispatchInvalidateOptionsMenu();
		}
		return true;
	}

}
