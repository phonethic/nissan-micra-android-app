package com.phonethics.nissanmicra;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class NotificationCallBack extends Activity {
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		context = this;
		
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);

		String packageName = activityManager.getRunningTasks(1).get(0).topActivity.getPackageName();
		String class_running = activityManager.getRunningTasks(1).get(0).topActivity.getClassName();
		String previos = activityManager.getRunningTasks(1).get(0).baseActivity.getClassName();
		
		/*Toast.makeText(context, "class_running" +  previos, Toast.LENGTH_LONG).show();*/
		if(previos.equalsIgnoreCase("com.phonethics.nissanmicra.Home_new")){
			finish();
		}else if(previos.equalsIgnoreCase("com.phonethics.nissanmicra.NotificationCallBack")){
			Intent intent = new Intent(context, SplashScreen.class);
			/*startActivity(intent);*/
			startActivity(intent);
			finish();
			
			overridePendingTransition(0, 0);
		}else{
			finish();
		}
		
		
		
	}
	

}
