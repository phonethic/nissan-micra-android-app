package com.phonethics.parser;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.content.Context;
import android.util.Log;

import com.phonethics.customclass.NeonGalleryLinks;
import com.phonethics.nissanmicra.R;


public class SplashImageLinkHandler extends DefaultHandler {

	SplashImageLinksPojo				tempSplashImageLinksPojo;
	ArrayList<SplashImageLinksPojo>		priclistArr;
	Context								context;
	

	public SplashImageLinkHandler(Context context){
		this.context = context;
	}
	@Override
	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub
		
		this.priclistArr = new ArrayList<SplashImageLinksPojo>();
		super.startDocument();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		super.startElement(uri, localName, qName, attributes);
		if(qName.equalsIgnoreCase("photo")){
			
			tempSplashImageLinksPojo = new SplashImageLinksPojo();
			
			String link = attributes.getValue("Link");
			tempSplashImageLinksPojo.setLink(link);
			
			
			
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
		super.characters(ch, start, length);
	}
	
	
	@Override
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub
		super.endDocument();
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// TODO Auto-generated method stub
		super.endElement(uri, localName, qName);
		if(qName.equalsIgnoreCase("photo")){
			priclistArr.add(tempSplashImageLinksPojo);
		}
	}
	
	
	public ArrayList<SplashImageLinksPojo> getParsedData(){
		return priclistArr;
	}
	
	
	
}
