package com.phonethics.parser;



import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.phonethics.customclass.DealerLocationAndAddress;

import android.util.Log;




public class DealerLocatorParser {
	public static ArrayList<DealerLocationAndAddress> parse(InputStream is){
		ArrayList<DealerLocationAndAddress> dealerLocations = null;
		

		 try {
	            // create a XMLReader from SAXParser
	            XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser()
	                    .getXMLReader();
	            // create a ExampleHandler
	            DealerParser saxHandler = new DealerParser();
	            // store handler in XMLReader
	            xmlReader.setContentHandler(saxHandler);
	            // the process starts
	            xmlReader.parse(new InputSource(is));
	            // get the Project list`
	            /*projects = saxHandler.getParsedData();*/
	            
	            dealerLocations = saxHandler.getParsedData();
	            
	            /*for(int i=0;i<dealerLocations.size();i++){
	            	Log.i("DealerLocatorParser class---->>>", " City " + i + "=== >>>" +dealerLocations.get(i).getCityName().toString());
	            }
*/	            
	 
	        } catch (Exception ex) {
	            Log.d("XML", "ExampleParser: parse() failed");
	        }
		
		return dealerLocations;
		

	}
}
