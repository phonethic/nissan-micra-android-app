package com.phonethics.parser;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.content.Context;
import android.util.Log;

import com.phonethics.customclass.NeonGalleryLinks;

public class SplashImageLinkParser {
	static Context context;
	public SplashImageLinkParser(Context context){
		this.context = context;
	}
	 public static ArrayList<SplashImageLinksPojo> parse(InputStream is) {
		 ArrayList<SplashImageLinksPojo> priceListDataArr = null;
		 try {
			
			 
	            // create a XMLReader from SAXParser
	            XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();

	            SplashImageLinkHandler saxHandler = new SplashImageLinkHandler(context);
	            // store handler in XMLReader
	            xmlReader.setContentHandler(saxHandler);
	            // the process starts
	            xmlReader.parse(new InputSource(is));
	            // get the Project list`
	            /*projects = saxHandler.getParsedData();*/
	            
	            priceListDataArr	=	 saxHandler.getParsedData();
	            
	 
	        }
		 catch (Exception ex) {
	            Log.d("XML", "ExampleParser: parse() failed");
	            ex.printStackTrace();
	        }
		 return priceListDataArr;
	 }
}
