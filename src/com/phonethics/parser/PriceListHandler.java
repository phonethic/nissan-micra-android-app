package com.phonethics.parser;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.content.Context;
import android.util.Log;

import com.phonethics.customclass.NeonGalleryLinks;
import com.phonethics.nissanmicra.R;


public class PriceListHandler extends DefaultHandler {

	PriceListPojo				tempPriceListPojo;
	String						id,city,state,xe,xe_plus,xl,xl_primo,xv,xv_primo,xv_dl,xv_primo_dl,xv_prem_dl,xv_prem_primo_dl;
	ArrayList<PriceListPojo>	priclistArr;
	Context						context;
	

	public PriceListHandler(Context context){
		this.context = context;
	}
	@Override
	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub
		
		this.priclistArr = new ArrayList<PriceListPojo>();
		tempPriceListPojo = new PriceListPojo(context);
		super.startDocument();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		super.startElement(uri, localName, qName, attributes);
		if(qName.equalsIgnoreCase(context.getResources().getString(R.string.price_tag))){
			
			tempPriceListPojo = new PriceListPojo(context);
			
			id 					= attributes.getValue(context.getResources().getString(R.string.price_id));
			state				= attributes.getValue(context.getResources().getString(R.string.price_state));
			city				= attributes.getValue(context.getResources().getString(R.string.price_city));
			xe					= attributes.getValue(context.getResources().getString(R.string.price_xe));
			xe_plus				= attributes.getValue(context.getResources().getString(R.string.price_xe_plus));
			xl					= attributes.getValue(context.getResources().getString(R.string.price_xl));
			xl_primo			= attributes.getValue(context.getResources().getString(R.string.price_xl_primo));
			xv					= attributes.getValue(context.getResources().getString(R.string.price_xv));
			xv_primo			= attributes.getValue(context.getResources().getString(R.string.price_xv_primo));
			xv_dl				= attributes.getValue(context.getResources().getString(R.string.price_dl_xv));
			xv_primo_dl			= attributes.getValue(context.getResources().getString(R.string.price_dl_xv_primo));
			xv_prem_dl			= attributes.getValue(context.getResources().getString(R.string.price_dl_xv_prem));
			xv_prem_primo_dl	= attributes.getValue(context.getResources().getString(R.string.price_dl_xv_prem_primo));
			
			//Log.d("=====", "Price "+id+" "+state+" "+city+" "+xe+" "+xe_plus+" "+xl+" "+xl_primo+" "+xv_primo+" "+xv_dl+" ");
			
			tempPriceListPojo.setId(id);
			tempPriceListPojo.setState(state);
			tempPriceListPojo.setCity(city);
			tempPriceListPojo.setXe(xe);
			tempPriceListPojo.setXe_plus(xe_plus);
			tempPriceListPojo.setXl(xl);
			tempPriceListPojo.setXl_primo(xl_primo);
			tempPriceListPojo.setXv(xv);
			tempPriceListPojo.setXv_primo(xv_primo);
			tempPriceListPojo.setXv_dl(xv_dl);
			tempPriceListPojo.setXv_primo_dl(xv_primo_dl);
			tempPriceListPojo.setXv_prem_dl(xv_prem_dl);
			tempPriceListPojo.setXv_prem_primo_dl(xv_prem_primo_dl);
			
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
		super.characters(ch, start, length);
	}
	
	
	@Override
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub
		super.endDocument();
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// TODO Auto-generated method stub
		super.endElement(uri, localName, qName);
		if(qName.equalsIgnoreCase(context.getResources().getString(R.string.price_tag))){
			priclistArr.add(tempPriceListPojo);
		}
	}
	
	
	public ArrayList<PriceListPojo> getParsedData(){
		return priclistArr;
	}
	
	
	
}
