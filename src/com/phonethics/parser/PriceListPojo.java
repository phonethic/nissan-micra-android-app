package com.phonethics.parser;

import java.io.Serializable;
import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.phonethics.database.MicraDataBase;
import com.phonethics.nissanmicra.R;

public class PriceListPojo implements Serializable{
	
	
	
	
	MicraDataBase	database;
	Context			context;

	String		state   	= "";
	String		city 		= "";
	String		id			= "";
	String		xe			= "";
	String		xe_plus		= "";
	String		xl			= "";
	String		xl_primo	= "";
	String		xv			= "";
	String		xv_primo	= "";
	String		xv_dl		= "";
	String		xv_primo_dl	= "";
	String		xv_prem_dl	= "";
	String		xv_prem_primo_dl = "";


	ArrayList<String>		idArr 					= new ArrayList<String>();
	
	ArrayList<String>		stateArr 				= new ArrayList<String>();
	ArrayList<String> 		cityArr					= new ArrayList<String>();
	
	ArrayList<String> 		xe_Arr					= new ArrayList<String>();
	ArrayList<String> 		xe_plus_Arr				= new ArrayList<String>();
	
	ArrayList<String> 		xl_Arr					= new ArrayList<String>();
	ArrayList<String> 		xl_primo_Arr			= new ArrayList<String>();
	
	ArrayList<String> 		xv_Arr					= new ArrayList<String>();
	ArrayList<String> 		xv_primo_Arr			= new ArrayList<String>();
	
	ArrayList<String> 		xv_dl_Arr				= new ArrayList<String>();
	ArrayList<String> 		xv_primo_dl_Arr			= new ArrayList<String>();
	
	ArrayList<String> 		xv_prem_dl_Arr			= new ArrayList<String>();
	ArrayList<String> 		xv_prem_primo_dl_Arr	= new ArrayList<String>();
	
	

	public PriceListPojo(Context context){
		this.context =context;
		database = new MicraDataBase(this.context);
	}


	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
		stateArr.add(this.state);
		//database.addValue(context.getResources().getString(R.string.price_state), this.state);
		
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
		cityArr.add(this.city);
		//database.addValue(context.getResources().getString(R.string.price_id), this.city);
		
	}


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
		//database.addValue(context.getResources().getString(R.string.price_id), this.id);
	}
	public String getXe() {
		return xe;
	}
	public void setXe(String xe) {
		this.xe = xe;
		xe_Arr.add(this.xe);
		//.addValue(context.getResources().getString(R.string.price_xe), this.xe);
	}
	public String getXe_plus() {
		return xe_plus;
		
	}
	public void setXe_plus(String xe_plus) {
		this.xe_plus = xe_plus;
		xe_plus_Arr.add(this.xe_plus);
		//database.addValue(context.getResources().getString(R.string.price_xe_plus), this.xe_plus);
	}
	public String getXl() {
		return xl;
	}
	public void setXl(String xl) {
		this.xl = xl;
		xl_Arr.add(this.xl);
		//database.addValue(context.getResources().getString(R.string.price_xl), this.xl);
	}
	public String getXl_primo() {
		return xl_primo;
	}
	public void setXl_primo(String xl_primo) {
		this.xl_primo = xl_primo;
		xl_primo_Arr.add(this.xl_primo);
		//database.addValue(context.getResources().getString(R.string.price_xl_primo), this.xl_primo);
	}
	public String getXv() {
		return xv;
	}
	public void setXv(String xv) {
		this.xv = xv;
		xv_Arr.add(this.xv);
		//database.addValue(context.getResources().getString(R.string.price_xv), this.xv);
	}
	public String getXv_primo() {
		return xv_primo;
	}
	public void setXv_primo(String xv_primo) {
		this.xv_primo = xv_primo;
		xv_primo_Arr.add(this.xv_primo);
		//database.addValue(context.getResources().getString(R.string.price_xv_primo), this.xv_primo);
	}
	public String getXv_dl() {
		return xv_dl;
	}
	public void setXv_dl(String xv_dl) {
		this.xv_dl = xv_dl;
		xv_dl_Arr.add(this.xv_dl);
		//database.addValue(context.getResources().getString(R.string.price_dl_xv), this.xv_dl);
	}
	public String getXv_primo_dl() {
		return xv_primo_dl;
		
	}
	public void setXv_primo_dl(String xv_primo_dl) {
		this.xv_primo_dl = xv_primo_dl;
		xv_primo_dl_Arr.add(this.xv_primo_dl);
		//database.addValue(context.getResources().getString(R.string.price_dl_xv_primo), this.xv_primo_dl);
	}
	public String getXv_prem_dl() {
		return xv_prem_dl;
	}
	public void setXv_prem_dl(String xv_prem_dl) {
		this.xv_prem_dl = xv_prem_dl;
		xv_prem_dl_Arr.add(this.xv_prem_dl);
		//database.addValue(context.getResources().getString(R.string.price_dl_xv_prem), this.xv_prem_dl);
	}
	public String getXv_prem_primo_dl() {
		return xv_prem_primo_dl;
	}
	public void setXv_prem_primo_dl(String xv_prem_primo_dl) {
		this.xv_prem_primo_dl = xv_prem_primo_dl;
		xv_prem_primo_dl_Arr.add(this.xv_prem_primo_dl);
		//database.addValue(context.getResources().getString(R.string.price_dl_xv_prem_primo), this.xv_prem_primo_dl);
	}
	
	
	public ArrayList<String> getCitysOfStates(String state){
		ArrayList<String> cityNames  = new ArrayList<String>();
		
		for(int i=0;i<cityArr.size();i++){
			if(state.equalsIgnoreCase(stateArr.get(i))){
				cityNames.add(cityArr.get(i));
				Log.d("===", "City "+cityArr.get(i));
			}
			Log.d("===", "City d "+cityArr.get(i));
		}
		Log.d("===", "City e ");
		
		return cityNames;
	}
	
	
	public ArrayList<String> getStates(){
		ArrayList<String> stateNames  = new ArrayList<String>();
		
		for(int i=0;i<stateArr.size();i++){
			if(!stateNames.contains(stateArr.get(i))){
				stateNames.add(stateArr.get(i));
				Log.d("===", "State "+stateArr.get(i));
			}
			Log.d("===", "State d "+stateArr.get(i));
		}
		Log.d("===", "State e ");
		
		return stateNames;
	}
	
	
	public ArrayList<String> getPrices(String state,String city){
		ArrayList<String> tempArrayList = new ArrayList<String>();
		int num = 0;
		for(int i=0;i<stateArr.size();i++){
			if(   (state.equalsIgnoreCase(stateArr.get(i)))   && (city.equalsIgnoreCase(cityArr.get(i)))  ){
				num = i;
				break;
			}
		}
		
		
		tempArrayList.add(xe_Arr.get(num));
		tempArrayList.add(xe_plus_Arr.get(num));
		
		tempArrayList.add(xl_Arr.get(num));
		tempArrayList.add(xl_primo_Arr.get(num));
		
		tempArrayList.add(xv_Arr.get(num));
		tempArrayList.add(xv_primo_Arr.get(num));
		
		tempArrayList.add(xv_dl_Arr.get(num));
		tempArrayList.add(xv_primo_dl_Arr.get(num));
		
		tempArrayList.add(xv_prem_dl_Arr.get(num));
		tempArrayList.add(xv_prem_primo_dl_Arr.get(num));
		
		
		return tempArrayList;
	}
	
	
}
