package com.phonethics.parser;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.phonethics.customclass.DealerLocationAndAddress;

import android.util.Log;

public class DealerParser extends DefaultHandler {
	
	private DealerLocationAndAddress	tempDealer;
	private ArrayList<DealerLocationAndAddress> dealerDetails;
	
	
	public ArrayList<DealerLocationAndAddress> getParsedData(){
		return dealerDetails;
	}
	
	public void startDocument() throws SAXException{
		Log.d("Document", "Inside XML Document");
		this.dealerDetails = new ArrayList<DealerLocationAndAddress>();
		tempDealer = new DealerLocationAndAddress();

	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
		super.characters(ch, start, length);
	}

	@Override
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub
		super.endDocument();
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// TODO Auto-generated method stub
		super.endElement(uri, localName, qName);
		
		if(qName.equalsIgnoreCase("city")){
			dealerDetails.add(tempDealer);
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		super.startElement(uri, localName, qName, attributes);
		
		if(qName.equalsIgnoreCase("city")){
			tempDealer = new DealerLocationAndAddress();
			String cityName = attributes.getValue("name");
			tempDealer.setCityName(cityName);
		}else if(qName.equalsIgnoreCase("store")){
			tempDealer.setDealerName(attributes.getValue("name"));
			tempDealer.setDealerAddress(attributes.getValue("address"));
			tempDealer.setDealerPhone1(attributes.getValue("phone1"));
			tempDealer.setDealerPhone2(attributes.getValue("phone2"));
			tempDealer.setDealerMobile1(attributes.getValue("mobile1"));
			tempDealer.setDealerMobile2(attributes.getValue("mobile2"));
			tempDealer.setDealerFax(attributes.getValue("fax"));
			tempDealer.setDealerEmail(attributes.getValue("email"));
			tempDealer.setDealerWebsite(attributes.getValue("website"));
			tempDealer.setDealerPhoto1(attributes.getValue("photo1"));
			tempDealer.setDealerPhoto2(attributes.getValue("photo2"));
			tempDealer.setDealerDescription(attributes.getValue("description"));
			tempDealer.setDealerlatitude(attributes.getValue("latitude"));
			tempDealer.setDealerlongitude(attributes.getValue("longitude"));
			
		}
	}
}
