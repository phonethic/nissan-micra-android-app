package com.phonethics.customclass;

import java.util.ArrayList;

public class TipsGalleryData {

	private String id="";
	private String ilink="";
	private String vlink="";
	private String sectionName="";
	private String thumb="";
	
	private ArrayList<String> arr_Id=new ArrayList<String>();
	private ArrayList<String> arr_Ilink=new ArrayList<String>();
	private ArrayList<String> arr_Vlink=new ArrayList<String>();
	private ArrayList<String> arr_thumb=new ArrayList<String>();
	
	
	public String getThumb() {
		return thumb;
	}
	public void setThumb(String thumb) {
		this.thumb = thumb;
		arr_thumb.add(thumb);
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
		arr_Id.add(id);
	}
	public String getIlink() {
		return ilink;
	}
	public void setIlink(String ilink) {
		this.ilink = ilink;
		arr_Ilink.add(ilink);
	}
	public String getVlink() {
		return vlink;
	}
	public void setVlink(String vlink) {
		this.vlink = vlink;
		arr_Vlink.add(vlink);
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public ArrayList<String> getArr_Id() {
		return arr_Id;
	}
	
	public ArrayList<String> getArr_Ilink() {
		return arr_Ilink;
	}
	
	public ArrayList<String> getArr_Vlink() {
		return arr_Vlink;
	}
	
	
	
	
	
}
