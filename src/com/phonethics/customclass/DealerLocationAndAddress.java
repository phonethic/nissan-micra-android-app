package com.phonethics.customclass;

import java.io.Serializable;
import java.util.ArrayList;

import android.util.Log;
import android.widget.Toast;

public class DealerLocationAndAddress implements Serializable {
	
	String 		cityName;
	String 		dealerName;
	String		dealerAddress;
	String		dealerPhone1;
	String		dealerPhone2;
	String		dealerMobile1;
	String		dealerMobile2;
	String		dealerFax;
	String		dealerEmail;
	String		dealerWebsite;
	String 		dealerPhoto1;
	String 		dealerPhoto2;
	String		dealerDescription;
	String		dealerlatitude;
	String		dealerlongitude;
	
	public ArrayList<String> dealerCityArray = new ArrayList<String>();
	public ArrayList<String> dlrName = new ArrayList<String>();
	public ArrayList<String> dlrAddress = new ArrayList<String>();
	public ArrayList<String> dlrPhone1 = new ArrayList<String>();
	public ArrayList<String> dlrPhone2 = new ArrayList<String>();
	public ArrayList<String> dlrMobile1 = new ArrayList<String>();
	public ArrayList<String> dlrMobile2 = new ArrayList<String>();
	public ArrayList<String> dlrFax = new ArrayList<String>();
	public ArrayList<String> dlrEmail = new ArrayList<String>();
	public ArrayList<String> dlrWebsite = new ArrayList<String>();
	public ArrayList<String> dlrPhoto1 = new ArrayList<String>();
	public ArrayList<String> dlrPhoto2 = new ArrayList<String>();
	public ArrayList<String> dlrDescription = new ArrayList<String>();
	public ArrayList<String> dlrlatitude = new ArrayList<String>();
	public ArrayList<String> dlrlongitude = new ArrayList<String>();
	
	
	/*private ArrayList<String> carName 	= new ArrayList<String>();*/

	
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
		dealerCityArray.add(this.cityName);
		/*Toast.makeText(context, "city name ", Toast.LENGTH_SHORT).*/
		Log.i("-------->>>>>>", " City Name = " + this.cityName);
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
		dlrName.add(this.dealerName);
	}
	public String getDealerAddress() {
		return dealerAddress;
	}
	public void setDealerAddress(String dealerAddress) {
		this.dealerAddress = dealerAddress;
		dlrAddress.add(this.dealerAddress);
	}
	public String getDealerPhone1() {
		return dealerPhone1;
	}
	public void setDealerPhone1(String dealerPhone1) {
		this.dealerPhone1 = dealerPhone1;
		dlrPhone1.add(this.dealerPhone1);
	}
	public String getDealerPhone2() {
		return dealerPhone2;
	}
	public void setDealerPhone2(String dealerPhone2) {
		this.dealerPhone2 = dealerPhone2;
		dlrPhone2.add(this.dealerPhone2);
	}
	public String getDealerMobile1() {
		return dealerMobile1;
	}
	public void setDealerMobile1(String dealerMobile1) {
		this.dealerMobile1 = dealerMobile1;
		dlrMobile1.add(this.dealerMobile1);
	}
	public String getDealerMobile2() {
		return dealerMobile2;
	}
	public void setDealerMobile2(String dealerMobile2) {
		this.dealerMobile2 = dealerMobile2;
		dlrMobile2.add(this.dealerMobile2);
	}
	public String getDealerFax() {
		return dealerFax;
	}
	public void setDealerFax(String dealerFax) {
		this.dealerFax = dealerFax;
		dlrFax.add(this.dealerFax);
	}
	public String getDealerEmail() {
		return dealerEmail;
	}
	public void setDealerEmail(String dealerEmail) {
		this.dealerEmail = dealerEmail;
		dlrEmail.add(this.dealerEmail);
	}
	public String getDealerWebsite() {
		return dealerWebsite;
	}
	public void setDealerWebsite(String dealerWebsite) {
		this.dealerWebsite = dealerWebsite;
		dlrWebsite.add(this.dealerWebsite);
	}
	public String getDealerPhoto1() {
		return dealerPhoto1;
	}
	public void setDealerPhoto1(String dealerPhoto1) {
		this.dealerPhoto1 = dealerPhoto1;
		dlrPhoto1.add(this.dealerPhoto1);
	}
	public String getDealerPhoto2() {
		return dealerPhoto2;
	}
	public void setDealerPhoto2(String dealerPhoto2) {
		this.dealerPhoto2 = dealerPhoto2;
		dlrPhoto2.add(this.dealerPhoto2);
	}
	public String getDealerDescription() {
		return dealerDescription;
	}
	public void setDealerDescription(String dealerDescription) {
		this.dealerDescription = dealerDescription;
		dlrDescription.add(this.dealerDescription);
	}
	public String getDealerlatitude() {
		return dealerlatitude;
	}
	public void setDealerlatitude(String dealerlatitude) {
		this.dealerlatitude = dealerlatitude;
		dlrlatitude.add(this.dealerlatitude);
	}
	public String getDealerlongitude() {
		return dealerlongitude;
	}
	public void setDealerlongitude(String dealerlongitude) {
		this.dealerlongitude = dealerlongitude;
		dlrlongitude.add(this.dealerlongitude);
	}
	
	public ArrayList<String> allCitys(){
		return dealerCityArray;
	}
	

}
