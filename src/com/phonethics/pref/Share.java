package com.phonethics.pref;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Share {
	
	Context 			context;
	SharedPreferences 	pref; // 0 - for private mode
	SharedPreferences	activityLaunchPref;
	SharedPreferences	appOpenPref,app_open_first_time,update_build;
	Editor 				editor;
	
	public Share(Context context){
		this.context 		= context;
		pref 				= this.context.getSharedPreferences("MyPref", 0); // 0 - for private mode
		activityLaunchPref 	= this.context.getSharedPreferences("ActivityLaunch", 0);
		app_open_first_time = this.context.getSharedPreferences("APP_OPEN_FIRST_TIME", 0);
		update_build		= this.context.getSharedPreferences("update", 0);	
		appOpenPref 	= this.context.getSharedPreferences("AppOpen", 0);
		
	}
	
	public void setAppClose(boolean isSet){
		editor 		= pref.edit();
		editor.putBoolean("App_Launch", isSet);
		editor.commit();
		
	}
	
	public boolean isAppOpen(){
		return pref.getBoolean("App_Launch", true);
	}
	
	public void setLaunchFirstTime(boolean isFirstTime){
		editor 		= activityLaunchPref.edit();
		editor.putBoolean("FirstTime", isFirstTime);
		editor.commit();
	}
	
	public boolean isAppLaunchFirstTime(){
		return activityLaunchPref.getBoolean("FirstTime", true);
	}
	
	public void setActivityOpen(boolean set){
		editor 		= activityLaunchPref.edit();
		editor.putBoolean("FirstTimeActivity", set);
		editor.commit();
	}
	
	public boolean isActivityOpenFirstTime(){
		return activityLaunchPref.getBoolean("FirstTimeActivity", true);
	}
	
	
	public boolean isAppOpenFirstTime(){
		return app_open_first_time.getBoolean("FIRST_TIME_LUANCH", true);
	}
	
	public void setAppOpenFirstTime(boolean set){
		editor 		= app_open_first_time.edit();
		editor.putBoolean("FIRST_TIME_LUANCH", set);
		editor.commit();
	}
	
	public void setUpdateBuild(boolean update){
		editor 		= update_build.edit();
		editor.putBoolean("UPDATE_app", update);
		editor.commit();
	}
	
	public boolean getUpdateBuild(){
		return update_build.getBoolean("UPDATE_app", true);
	}
	
	public void setXEDSL(boolean present){
		editor 		= update_build.edit();
		editor.putBoolean("XE_DSL", present);
		editor.commit();
	}
	
	public boolean geXEDSL(){
		return update_build.getBoolean("XE_DSL", false);
	}
	
	/*public void setAppOpen(boolean set){
		editor 		= appOpenPref.edit();
		editor.putBoolean("AppOpen", set);
		editor.commit();
	}
	
	public boolean isAppOpen(){
		return appOpenPref.getBoolean("AppOpen", true);
	}*/
	
}
