package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.adapters.TechnicalListAdapter.ViewHolder;
import com.phonethics.nissanmicra.R;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PriceListAdapter extends BaseAdapter {

	Activity 		 actContext;
	ArrayList<String> modelNames;
	ArrayList<String> petrolPriceArr;
	ArrayList<String> dieselPriceArr;
	ArrayList<String> actPriceArr;
	LayoutInflater 	inflater	=	null;
	boolean			secondConst = true; 
	Typeface 				font;


	public PriceListAdapter(Activity actContext,
			ArrayList<String> modelNames, 
			ArrayList<String> petrolPriceArr, 
			ArrayList<String> dieselPriceArr,
			ArrayList<String> actPriceArr){

		this.actContext =	actContext;
		this.modelNames = modelNames;
		this.petrolPriceArr = petrolPriceArr;
		this.dieselPriceArr	= dieselPriceArr;
		this.actPriceArr	= actPriceArr;
		secondConst = false;

	}

	public PriceListAdapter(Activity actContext,
			ArrayList<String> modelNames, 
			ArrayList<String> priceArr){

		this.actContext 	=	actContext;
		this.modelNames 	= modelNames;
		this.petrolPriceArr = priceArr;
		secondConst = true;
		font 		= Typeface.createFromAsset(actContext.getAssets(), "fonts/n_ex____.ttf");

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return modelNames.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){

			ViewHolder holder 		= new ViewHolder();
			inflater				= actContext.getLayoutInflater();
			if(secondConst){
				convertView 			= inflater.inflate(R.layout.price_list_layout_new_act, null);
			}else{
				convertView 			= inflater.inflate(R.layout.price_list_layout_new, null);
			}
			holder.text_modelName		= (TextView) convertView.findViewById(R.id.text_model_name);
			holder.text_price_petrol	= (TextView) convertView.findViewById(R.id.text_petrol_price);
			holder.text_price_diesel	= (TextView) convertView.findViewById(R.id.text_diesel_price);
			holder.text_price_active	= (TextView) convertView.findViewById(R.id.text_active_price);
			

			holder.text_modelName.setTypeface(font);
			holder.text_price_petrol.setTypeface(font);

			convertView.setTag(holder);

		}

		ViewHolder hold = (ViewHolder) convertView.getTag();


		try{
			if(secondConst){
				hold.text_modelName.setText(modelNames.get(position));
				hold.text_price_petrol.setText(petrolPriceArr.get(position));
				hold.text_price_diesel.setVisibility(View.GONE);
				hold.text_price_active.setVisibility(View.GONE);
			}else{
				hold.text_modelName.setText(modelNames.get(position));
				hold.text_price_petrol.setText(petrolPriceArr.get(position));
				hold.text_price_diesel.setText(dieselPriceArr.get(position));
				hold.text_price_active.setText(actPriceArr.get(position));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return convertView;
	}


	static class ViewHolder{
		TextView	text_modelName,text_price_petrol,text_price_diesel,text_price_active;
	}

}
