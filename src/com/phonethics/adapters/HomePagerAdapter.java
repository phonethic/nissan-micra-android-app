package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.nissanmicra.HomePagerFragment;
import com.phonethics.nissanmicra.R;



import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class HomePagerAdapter extends BaseAdapter {

	Activity 			context;
	ArrayList<String> 	orders;
	ArrayList<ArrayList<String>> 	allLinks,allNames,allCaption,allThumbnails,allThumbnailsCopy,allDescription,allVLinks;
	private static LayoutInflater inflator = null;
	PagerAdapter		pagerAdapter;
	FragmentManager 	fm;
	ArrayList<Boolean>	isSetNewAdapter = new ArrayList<Boolean>();
	
	public HomePagerAdapter(Activity context,FragmentManager fm, ArrayList<String> orders,
			ArrayList<ArrayList<String>> allLinks,
			ArrayList<ArrayList<String>> allNames,
			ArrayList<ArrayList<String>> allCaption,
			ArrayList<ArrayList<String>> allThumbnails,
			ArrayList<ArrayList<String>> allThumbnailsCopy,
			ArrayList<ArrayList<String>> allDescription,
			ArrayList<ArrayList<String>> allVLinks){
		
		this.context 			= context;
		this.orders				= orders;
		this.allLinks 			= allLinks;
		this.allNames 			= allNames;
		this.allCaption 		= allCaption;
		this.allThumbnails 		= allThumbnails;
		this.allThumbnailsCopy 	= allThumbnailsCopy;
		this.allDescription 	= allDescription;
		this.allVLinks 			= allVLinks;
		this.fm					= fm;
		
		for(int i=0;i<this.orders.size();i++){
			isSetNewAdapter.add(true);
		}
		
		
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return orders.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return orders.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder 		= new ViewHolder();
		if(convertView == null){
		
			inflator = context.getLayoutInflater();
			convertView = inflator.inflate(R.layout.home_pager_layout, null);
			holder.pager = (ViewPager) convertView.findViewById(R.id.pager_gallery);
			/*pagerAdapter = new PagerAdapter(fm, allThumbnails.get(position));
			holder.pager.setAdapter(pagerAdapter);*/
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();	
			
		}
		
		if(isSetNewAdapter.get(position)){
			pagerAdapter = new PagerAdapter(fm, allThumbnails.get(position));
			holder.pager.setAdapter(pagerAdapter);
			isSetNewAdapter.set(position, false);
		}
		
		
		return convertView;
	}
	
	
	public static class ViewHolder{
		ViewPager	pager;
	}

	
	public class PagerAdapter extends FragmentPagerAdapter {

		
		ArrayList<String> imgUrls;

		public PagerAdapter(FragmentManager fm, ArrayList<String> imgUrls) {
			super(fm);
			this.imgUrls = imgUrls;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			
			return "";
		}

		@Override
		public int getCount() {
			return imgUrls.size();
		}

		@Override
		public Fragment getItem(int position) {
			
			HomePagerFragment fragment= new HomePagerFragment(context, imgUrls.get(position));
			return fragment;
			
		}
	}
}
