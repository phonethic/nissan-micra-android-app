package com.phonethics.adapters;

import java.util.ArrayList;


import com.phonethics.nissanmicra.Features_new;
import com.phonethics.nissanmicra.R;
import com.phonethics.nissanmicra.TechnicalSpecification;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TechnicalListAdapter extends BaseAdapter {

	Activity 			context;
	ArrayList<String> 	feturesData,car1Data,car2Data,car3Data,car4Data,car5Data;
	LayoutInflater 		inflater	=	null;
	boolean 			showDifference = true,applyAnim;
	Animation				anim_from_right,anim_from_left,temp_anim;
	boolean 				from_left = false;
	ArrayList<Boolean> 		animationApply;




	public TechnicalListAdapter(Activity conetextAct,ArrayList<String> feturesData,
			ArrayList<String> car1Data,
			ArrayList<String> car2Data,
			ArrayList<String> car3Data,
			ArrayList<String> car4Data,
			ArrayList<String> car5Data,
			boolean showDifference){

		this.context = conetextAct;
		this.car1Data =car1Data;
		this.car2Data = car2Data;
		this.car3Data = car3Data;
		this.car4Data = car4Data;
		this.car5Data = car5Data;
		this.feturesData = feturesData;
		this.showDifference = showDifference;
		this.applyAnim = showDifference;

		animationApply = new ArrayList<Boolean>();
		for(int i = 0;i<this.feturesData.size();i++){
			animationApply.add(true);
		}
		anim_from_right = AnimationUtils.loadAnimation(context, R.anim.slide_anim_from_right_slow);
		anim_from_left = AnimationUtils.loadAnimation(context, R.anim.slide_anim_from_left_slow);

	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return feturesData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return feturesData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){

			ViewHolder holder 		= new ViewHolder();
			inflater				= context.getLayoutInflater();
			convertView 			= inflater.inflate(R.layout.tecfeatures_list_layout_new, null);
			holder.text_feature 	= (TextView) convertView.findViewById(R.id.text_feature_content);
			holder.text_1 			= (TextView) convertView.findViewById(R.id.text_1);
			holder.text_2 			= (TextView) convertView.findViewById(R.id.text_2);
			holder.text_3 			= (TextView) convertView.findViewById(R.id.text_3);
			holder.text_4 			= (TextView) convertView.findViewById(R.id.text_4);
			holder.text_5 			= (TextView) convertView.findViewById(R.id.text_5);
			holder.root_layout		= (LinearLayout) convertView.findViewById(R.id.root_layout);
			convertView.setTag(holder);

		}

		ViewHolder hold = (ViewHolder) convertView.getTag();

		String text_a = car1Data.get(position);
		String text_b = car2Data.get(position);
		String text_c = car3Data.get(position);
		String text_d = "";
		String text_e = "";

		try{
			Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf");
			hold.text_feature.setTypeface(font);
			hold.text_feature.setText(getItem(position).toString());
			hold.text_1.setText(car1Data.get(position));
			hold.text_2.setText(car2Data.get(position));
			hold.text_3.setText(car3Data.get(position));
			
			
			hold.text_1.setTypeface(font);
			hold.text_2.setTypeface(font);
			hold.text_3.setTypeface(font);
			hold.text_4.setTypeface(font);
			hold.text_5.setTypeface(font);
			
			if(TechnicalSpecification.isTechModelPetrol){
				hold.text_4.setVisibility(View.GONE);
				hold.text_5.setVisibility(View.GONE);
			}else if(TechnicalSpecification.isTechModelActive){
				text_d = car4Data.get(position);
				hold.text_4.setVisibility(View.VISIBLE);
				hold.text_4.setText(text_d);
				hold.text_5.setVisibility(View.GONE);
			}else{
				text_d = car4Data.get(position);
				text_e = car5Data.get(position);
				hold.text_5.setVisibility(View.VISIBLE);
				hold.text_4.setVisibility(View.VISIBLE);
				hold.text_4.setText(text_d);
				hold.text_5.setText(text_e);
			}
		}catch(IndexOutOfBoundsException ex){
			ex.printStackTrace();

		}




		if(TechnicalSpecification.isTechModelPetrol){
			if(text_a.equalsIgnoreCase(text_b) && text_a.equalsIgnoreCase(text_c)){
				hold.text_2.setVisibility(View.GONE);
				hold.text_3.setVisibility(View.GONE);
			}else{
				hold.text_2.setVisibility(View.VISIBLE);
				hold.text_3.setVisibility(View.VISIBLE);
			}
		}else if(TechnicalSpecification.isTechModelActive){
			if(text_a.equalsIgnoreCase(text_b) && text_a.equalsIgnoreCase(text_c) && text_a.equalsIgnoreCase(text_d)){
				hold.text_2.setVisibility(View.GONE);
				hold.text_3.setVisibility(View.GONE);
				hold.text_4.setVisibility(View.GONE);
			}else{
				hold.text_2.setVisibility(View.VISIBLE);
				hold.text_3.setVisibility(View.VISIBLE);
				hold.text_4.setVisibility(View.VISIBLE);
			}
		}else{
			if(text_a.equalsIgnoreCase(text_b) && text_a.equalsIgnoreCase(text_c) && text_a.equalsIgnoreCase(text_d) && text_a.equalsIgnoreCase(text_e)){
				hold.text_2.setVisibility(View.GONE);
				hold.text_3.setVisibility(View.GONE);
				hold.text_4.setVisibility(View.GONE);
				hold.text_5.setVisibility(View.GONE);
			}else{
				hold.text_2.setVisibility(View.VISIBLE);
				hold.text_3.setVisibility(View.VISIBLE);
				hold.text_4.setVisibility(View.VISIBLE);
				hold.text_5.setVisibility(View.VISIBLE);
			}
		}


		if(TechnicalSpecification.showDifference){
			if(TechnicalSpecification.isTechModelPetrol){
				if(!(text_a.equalsIgnoreCase(text_b)) || !(text_a.equalsIgnoreCase(text_c)) ){
					hold.text_1.setBackgroundColor(Color.parseColor("#C9EBF5"));
					hold.text_2.setBackgroundColor(Color.parseColor("#C9EBF5"));
					hold.text_3.setBackgroundColor(Color.parseColor("#C9EBF5"));
					hold.text_4.setBackgroundColor(Color.parseColor("#C9EBF5"));
					hold.text_feature.setBackgroundColor(Color.parseColor("#C9EBF5"));
				}else{
					hold.text_1.setBackgroundColor(Color.parseColor("#FFFFFF"));
					hold.text_2.setBackgroundColor(Color.parseColor("#FFFFFF"));
					hold.text_3.setBackgroundColor(Color.parseColor("#FFFFFF"));
					hold.text_4.setBackgroundColor(Color.parseColor("#FFFFFF"));
					hold.text_feature.setBackgroundColor(Color.parseColor("#FFFFFF"));	
				}
			}else if(TechnicalSpecification.isTechModelActive){
				if(!(text_a.equalsIgnoreCase(text_b)) || !(text_a.equalsIgnoreCase(text_c)) || !(text_a.equalsIgnoreCase(text_d)) ){
					hold.text_1.setBackgroundColor(Color.parseColor("#C9EBF5"));
					hold.text_2.setBackgroundColor(Color.parseColor("#C9EBF5"));
					hold.text_3.setBackgroundColor(Color.parseColor("#C9EBF5"));
					hold.text_4.setBackgroundColor(Color.parseColor("#C9EBF5"));
					hold.text_feature.setBackgroundColor(Color.parseColor("#C9EBF5"));
				}else{
					hold.text_1.setBackgroundColor(Color.parseColor("#FFFFFF"));
					hold.text_2.setBackgroundColor(Color.parseColor("#FFFFFF"));
					hold.text_3.setBackgroundColor(Color.parseColor("#FFFFFF"));
					hold.text_4.setBackgroundColor(Color.parseColor("#FFFFFF"));
					hold.text_feature.setBackgroundColor(Color.parseColor("#FFFFFF"));	
				}
			}else{
				if(!(text_a.equalsIgnoreCase(text_b)) || !(text_a.equalsIgnoreCase(text_c)) || !(text_a.equalsIgnoreCase(text_d)) || !(text_a.equalsIgnoreCase(text_e)) ){
					hold.text_1.setBackgroundColor(Color.parseColor("#C9EBF5"));
					hold.text_2.setBackgroundColor(Color.parseColor("#C9EBF5"));
					hold.text_3.setBackgroundColor(Color.parseColor("#C9EBF5"));
					hold.text_4.setBackgroundColor(Color.parseColor("#C9EBF5"));
					hold.text_5.setBackgroundColor(Color.parseColor("#C9EBF5"));
					hold.text_feature.setBackgroundColor(Color.parseColor("#C9EBF5"));
				}else{
					hold.text_1.setBackgroundColor(Color.parseColor("#FFFFFF"));
					hold.text_2.setBackgroundColor(Color.parseColor("#FFFFFF"));
					hold.text_3.setBackgroundColor(Color.parseColor("#FFFFFF"));
					hold.text_4.setBackgroundColor(Color.parseColor("#FFFFFF"));
					hold.text_5.setBackgroundColor(Color.parseColor("#FFFFFF"));
					hold.text_feature.setBackgroundColor(Color.parseColor("#FFFFFF"));	
				}
			}


		}else{
			hold.text_1.setBackgroundColor(Color.parseColor("#FFFFFF"));
			hold.text_2.setBackgroundColor(Color.parseColor("#FFFFFF"));
			hold.text_3.setBackgroundColor(Color.parseColor("#FFFFFF"));
			hold.text_4.setBackgroundColor(Color.parseColor("#FFFFFF"));
			hold.text_feature.setBackgroundColor(Color.parseColor("#FFFFFF"));	
		}

		/*if(TechnicalSpecification.hideSimilaraties){
			if(TechnicalSpecification.isTechModelPetrol){
				if(text_a.equalsIgnoreCase(text_b) && text_a.equalsIgnoreCase(text_c)){
					hold.root_layout.setVisibility(View.GONE);
				}else{
					hold.root_layout.setVisibility(View.VISIBLE);
				}
			}else{
				if(text_a.equalsIgnoreCase(text_b) && text_a.equalsIgnoreCase(text_c) && text_a.equalsIgnoreCase(text_d)){
					hold.root_layout.setVisibility(View.GONE);
				}else{
					hold.root_layout.setVisibility(View.VISIBLE);
				}
			}
		}else{
			hold.root_layout.setVisibility(View.VISIBLE);
		}*/


		if(from_left){
			temp_anim = anim_from_left;
			from_left = false;
		}else{
			temp_anim = anim_from_right;
			from_left = true;
		}
		if(applyAnim){
			if(animationApply.get(position)){
//				convertView.startAnimation(temp_anim);
//				animationApply.set(position, false);
			}
		}
		return convertView;
	}

	static class ViewHolder{

		TextView 		text_1,text_2,text_3,text_4,text_5,text_feature;
		LinearLayout 	root_layout;
		//ImageView text_11,text_21,text_31,text_41;

	}

}
