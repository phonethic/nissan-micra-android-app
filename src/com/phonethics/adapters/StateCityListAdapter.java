package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.adapters.TechnicalListAdapter.ViewHolder;
import com.phonethics.nissanmicra.R;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class StateCityListAdapter extends BaseAdapter {

	Activity context;
	ArrayList<String> cityStateArr;
	LayoutInflater 	inflater	=	null;
	Typeface 				font;
	
	public StateCityListAdapter(Activity context,ArrayList<String> cityStateArr){
		this.context = context;
		this.cityStateArr = cityStateArr;
		font 		= Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf");
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return cityStateArr.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return cityStateArr.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			ViewHolder holder 		= new ViewHolder();
			inflater				= context.getLayoutInflater();
			convertView 			= inflater.inflate(R.layout.city_state_text, null);
			holder.textView 		= (TextView) convertView.findViewById(R.id.text_city_name);
			holder.textView.setTypeface(font);
			convertView.setTag(holder);
		}
		
		ViewHolder hold = (ViewHolder) convertView.getTag();
		hold.textView.setText(getItem(position).toString());
		return convertView;
	}
	
	
	static class ViewHolder{
		TextView textView;
	}

}
