
package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.adapters.FlipAdapter.ViewHolder;
import com.phonethics.database.MicraDataBase;
import com.phonethics.nissanmicra.Features_new;
import com.phonethics.nissanmicra.R;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class FeaturesAdapterNew extends BaseAdapter{

	Activity 				context;
	ArrayList<String> 		data;
	ArrayList<String> 		presence;
	ArrayList<ArrayList<String>> tempData;
	boolean					isPetrol;
	LayoutInflater inflater	=	null;
	int	index;
	boolean					previousLayout = false;
	MicraDataBase					database;

	String					xl_pet 		= "XL_Pet";
	String					xl_dsl 		= "XL_Dsl";
	String					xl_0_pet 	= "XL_O_Pet";
	String					xl_0_dsl 	= "XL_O_Dsl";
	String					xv_cvt 		= "XV_CVT";
	String					xv_dsl 		= "XV_Dsl";
	String					xv_pre_dsl 	= "XV_Premium_Dsl";


	public FeaturesAdapterNew(Activity context){
		this.context =  context;
	} 

	public FeaturesAdapterNew(Activity context, ArrayList<String> data, ArrayList<ArrayList<String>> tempData, int index){
		this.data = null;
		this.tempData = null;
		this.context =  context;
		this.data = data;
		this.presence = presence;
		this.index = index;
		this.tempData = tempData;


		//Toast.makeText(context, "Size adapter "+tempData.size(),0).show();
		Log.d("======", "size ==== " + tempData.size());
		//this.isPetrol	= isPetrol;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub

		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		if(convertView==null){
			ViewHolder holder 		= new ViewHolder();
			inflater				= context.getLayoutInflater();
			if(previousLayout){
				convertView 			= inflater.inflate(R.layout.features_list_layout_new, null);


				holder.text_feature 	= (TextView) convertView.findViewById(R.id.text_feature_content);
				holder.text_1 			= (TextView) convertView.findViewById(R.id.text_1);
				holder.text_2 			= (TextView) convertView.findViewById(R.id.text_2);
				holder.text_3 			= (TextView) convertView.findViewById(R.id.text_3);
				holder.text_4 			= (TextView) convertView.findViewById(R.id.text_4);
			}else{
				convertView 			= inflater.inflate(R.layout.features_list_layout_new_img, null);


				holder.text_feature 	= (TextView) convertView.findViewById(R.id.text_feature_content);
				holder.text_11 			= (ImageView) convertView.findViewById(R.id.text_1);
				holder.text_21 			= (ImageView) convertView.findViewById(R.id.text_2);
				holder.text_31 			= (ImageView) convertView.findViewById(R.id.text_3);
				holder.text_41 			= (ImageView) convertView.findViewById(R.id.text_4);
				holder.text_51 			= (ImageView) convertView.findViewById(R.id.text_5);


			}

			convertView.setTag(holder);
		}
		ViewHolder hold = (ViewHolder) convertView.getTag();
		/*hold.text_feature.setText(getItem(position).toString());
		hold.text_1.setText(presence.get(position));
		hold.text_2.setText(presence.get(position));
		hold.text_3.setText(presence.get(position));
		hold.text_4.setText(presence.get(position));
		if(Features_new.isModelPetrol){
			hold.text_4.setVisibility(View.GONE);
		}else{
			hold.text_4.setVisibility(View.VISIBLE);
		}*/
		if(previousLayout){
			hold.text_feature.setText(getItem(position).toString());
			hold.text_1.setText(tempData.get(0).get(position+index));
			hold.text_2.setText(tempData.get(1).get(position+index));
			hold.text_3.setText(tempData.get(2).get(position+index));
			if(Features_new.isModelPetrol){
				hold.text_4.setVisibility(View.GONE);
			}else if(Features_new.isModelActive){
				hold.text_4.setVisibility(View.VISIBLE);
				hold.text_4.setText(tempData.get(3).get(position+index));
			}else{
				hold.text_4.setVisibility(View.VISIBLE);
				hold.text_4.setText(tempData.get(3).get(position+index));
			}

		}else{
			Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/n_ex____.ttf");
			hold.text_feature.setText(getItem(position).toString());
			hold.text_feature.setTypeface(font);
			//hold.text_feature.setText(Features_new.featuresArr.get(index).get(position).toString());
			String text_a="",text_b="",text_c="",text_d="",text_e="";
			try{
				text_a = tempData.get(0).get(position+index);
				text_b = tempData.get(1).get(position+index);
				text_c = tempData.get(2).get(position+index);
				text_d = "";
				text_e = "";
			}catch (Exception ex){
				ex.printStackTrace();
			}
			if(text_a.equalsIgnoreCase("s")){
				hold.text_11.setImageResource(R.drawable.bullet_tick2);
			}else{
				hold.text_11.setImageResource(R.drawable.dialog_cancel2);
			}
			if(text_b.equalsIgnoreCase("s")){
				hold.text_21.setImageResource(R.drawable.bullet_tick2);
			}else{
				hold.text_21.setImageResource(R.drawable.dialog_cancel2);
			}
			if(text_c.equalsIgnoreCase("s")){
				hold.text_31.setImageResource(R.drawable.bullet_tick2);
			}else{
				hold.text_31.setImageResource(R.drawable.dialog_cancel2);
			}

			if(Features_new.isModelPetrol){
				//hold.text_feature.setText(Features_new.featuresArr.get(index).get(position).toString());
				hold.text_41.setVisibility(View.GONE);
				hold.text_51.setVisibility(View.GONE);

			}else if(Features_new.isModelActive){
				//hold.text_feature.setText(Features_new.featuresArrAct.get(index).get(position).toString());
				try{
					text_d = tempData.get(3).get(position+index);
				}catch(IndexOutOfBoundsException ex){
					ex.printStackTrace();
					text_d = tempData.get(2).get(position+index);
				}
				hold.text_41.setVisibility(View.VISIBLE);
				hold.text_51.setVisibility(View.GONE);
				if(text_d.equalsIgnoreCase("s")){
					hold.text_41.setImageResource(R.drawable.bullet_tick2);
				}else{
					hold.text_41.setImageResource(R.drawable.dialog_cancel2);
				}
			}else{
				//hold.text_feature.setText(Features_new.featuresArr.get(index).get(position).toString());
				try{
					text_d = tempData.get(3).get(position+index);
					text_e = tempData.get(4).get(position+index);
				}catch(IndexOutOfBoundsException ex){
					ex.printStackTrace();
					text_d = tempData.get(2).get(position+index);
					text_e = tempData.get(3).get(position+index);
				}
				hold.text_41.setVisibility(View.VISIBLE);
				hold.text_51.setVisibility(View.VISIBLE);
				if(text_d.equalsIgnoreCase("s")){
					hold.text_41.setImageResource(R.drawable.bullet_tick2);
					hold.text_51.setImageResource(R.drawable.bullet_tick2);
				}else{
					hold.text_41.setImageResource(R.drawable.dialog_cancel2);
					hold.text_51.setImageResource(R.drawable.dialog_cancel2);
				}
			}


			if(Features_new.asDifference){
				if(Features_new.isModelActive){
					if(!(text_a.equalsIgnoreCase(text_b)) || !(text_a.equalsIgnoreCase(text_c)) || !(text_a.equalsIgnoreCase(text_d)) ){
						hold.text_11.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_21.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_31.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_41.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_feature.setBackgroundColor(Color.parseColor("#C9EBF5"));
					}else{
						hold.text_11.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_21.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_31.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_41.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_feature.setBackgroundColor(Color.parseColor("#FFFFFF"));
					}
				}else if(Features_new.isModelPetrol){
					if(!(text_a.equalsIgnoreCase(text_b)) || !(text_a.equalsIgnoreCase(text_c)) ){
						hold.text_11.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_21.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_31.setBackgroundColor(Color.parseColor("#C9EBF5"));
						//hold.text_41.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_feature.setBackgroundColor(Color.parseColor("#C9EBF5"));
					}else{
						hold.text_11.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_21.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_31.setBackgroundColor(Color.parseColor("#FFFFFF"));
						//hold.text_41.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_feature.setBackgroundColor(Color.parseColor("#FFFFFF"));
					}
				}else{
					if((text_a.equalsIgnoreCase(text_b)) && (text_a.equalsIgnoreCase(text_c))  && (text_a.equalsIgnoreCase(text_d) && (text_a.equalsIgnoreCase(text_e))) ){
					
						/*hold.text_11.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_21.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_31.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_41.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_51.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_feature.setBackgroundColor(Color.parseColor("#C9EBF5"));*/
						
						hold.text_11.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_21.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_31.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_41.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_51.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_feature.setBackgroundColor(Color.parseColor("#FFFFFF"));
						
					}else{
						/*hold.text_11.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_21.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_31.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_41.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_51.setBackgroundColor(Color.parseColor("#FFFFFF"));
						hold.text_feature.setBackgroundColor(Color.parseColor("#FFFFFF"));*/
						
						hold.text_11.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_21.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_31.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_41.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_51.setBackgroundColor(Color.parseColor("#C9EBF5"));
						hold.text_feature.setBackgroundColor(Color.parseColor("#C9EBF5"));
					}
				}
			}else{
				hold.text_11.setBackgroundColor(Color.parseColor("#FFFFFF"));
				hold.text_21.setBackgroundColor(Color.parseColor("#FFFFFF"));
				hold.text_31.setBackgroundColor(Color.parseColor("#FFFFFF"));
				hold.text_41.setBackgroundColor(Color.parseColor("#FFFFFF"));
				hold.text_51.setBackgroundColor(Color.parseColor("#FFFFFF"));
				hold.text_feature.setBackgroundColor(Color.parseColor("#FFFFFF"));
			}
		}



		return convertView;
	}

	static class ViewHolder{

		TextView text_1,text_2,text_3,text_4,text_5,text_feature;
		ImageView text_11,text_21,text_31,text_41,text_51;

	}

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub


		super.notifyDataSetChanged();
	}



}
