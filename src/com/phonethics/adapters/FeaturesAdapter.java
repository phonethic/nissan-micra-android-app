
package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.adapters.FlipAdapter.ViewHolder;
import com.phonethics.nissanmicra.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FeaturesAdapter extends BaseAdapter{

	Activity 				context;
	ArrayList<String> 		data;
	ArrayList<String> 		presence;
	LayoutInflater inflater	=null;
	
	public FeaturesAdapter(Activity context, ArrayList<String> data,ArrayList<String> presence){
		this.context =  context;
		this.data = data;
		this.presence = presence;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if(convertView==null){
			inflater				= context.getLayoutInflater();
			convertView 			= inflater.inflate(R.layout.features_list_layout, null);
			ViewHolder holder 		= new ViewHolder();
			holder.text_data 		= (TextView) convertView.findViewById(R.id.text_feture);
			holder.text_presence 	= (TextView) convertView.findViewById(R.id.text_presence);	
			convertView.setTag(holder);
		}
		ViewHolder hold = (ViewHolder) convertView.getTag();
		hold.text_data.setText(getItem(position).toString());
		hold.text_presence.setText(presence.get(position));
		
		
		
		return convertView;
	}
	
	 static class ViewHolder{
		
		 TextView text_data,text_presence;
		 
	}

}
