package com.phonethics.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import com.phonethics.nissanmicra.R;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class MicraDataBase {



	private 				DbHelper 	dbHelper;
	private static 			Context 	ourContext;
	private static final 	String 		DATABASE_NAME="micra.db";
	private static final 	int  		DATABASE_VERSOIN=3;
	private static 			String 		DB_PATH = ""; 
	private static 			String TAG = "DataBaseHelper";
	private static			SQLiteDatabase mDataBase; 
	public static					boolean	newDatabase = false;

	static String table_features_active = "features_active";
	static String tabel_tec_active = "techspec_active";
	static String table_price = "price_micra";
	static String table_features_micra = "features_micra";
	static String tabele_tec_micra = "techspec_micra";


	public MicraDataBase(Context c){
		ourContext=c;
		dbHelper=new DbHelper(ourContext);
		try {
			dbHelper.createDataBase();
			openDataBase();
			int currentVersion = mDataBase.getVersion();
			Log.d("", "version current "+currentVersion);
			if(currentVersion<DATABASE_VERSOIN){
				newDatabase = true;
				dbHelper.copyDataBase();
				mDataBase.setVersion(DATABASE_VERSOIN);
				Log.d("", "version new "+mDataBase.getVersion());
			
			}else{

			}
			close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static class  DbHelper extends SQLiteOpenHelper{

		public DbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSOIN);
			// TODO Auto-generated constructor stub
			DB_PATH = "/data/data/" + ourContext.getPackageName() + "/databases/"; 


		}

		public void createDataBase() throws IOException 
		{ 
			//If database not exists copy it from the assets 

			boolean mDataBaseExist = checkDataBase(); 
			if(!mDataBaseExist) 
			{ 
				this.getReadableDatabase(); 
				this.close(); 
				try  
				{ 
					//Copy the database from assests

					copyDataBase(); 

					Log.e(TAG, "createDatabase database created"); 
				}  
				catch (IOException mIOException)  
				{ 
					throw new Error("ErrorCopyingDataBase"); 
				} 
			} 


			//Copy the database from assests 
			/*File dbFile = new File(DB_PATH + DATABASE_NAME); 
			try{
				if(dbFile.exists()){
					dbFile.delete();

				}else{

				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
			copyDataBase();*/

			Log.e(TAG, "createDatabase database created"); 
		} 

		private boolean checkDataBase() 
		{ 
			File dbFile = new File(DB_PATH + DATABASE_NAME); 
			//Log.v("dbFile", dbFile + "   "+ dbFile.exists());
			/*String mPath = DB_PATH + DATABASE_NAME; 
			//Log.v("mPath", mPath); 
			try{
				mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READWRITE);
				mDataBase.getVersion();
			}catch(Exception ex){
				ex.printStackTrace();
			}
			Log.d("", "database version "+mDataBase.getVersion());
			close();*/
			return dbFile.exists(); 
		} 


		//Copy the database from assets 
		private void copyDataBase() throws IOException 
		{ 
			InputStream mInput = ourContext.getAssets().open(DATABASE_NAME); 
			String outFileName = DB_PATH + DATABASE_NAME; 
			OutputStream mOutput = new FileOutputStream(outFileName); 
			byte[] mBuffer = new byte[1024]; 
			int mLength; 
			while ((mLength = mInput.read(mBuffer))>0) 
			{ 
				mOutput.write(mBuffer, 0, mLength); 
			} 
			mOutput.flush(); 
			mOutput.close(); 
			mInput.close(); 
		} 


		@Override
		public void onCreate(SQLiteDatabase db) {

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			Log.d("","============ micra db upgrade");
			db.execSQL("DROP TABLE IF EXISTS "+ table_features_active);
			db.execSQL("DROP TABLE IF EXISTS "+ tabel_tec_active);
			db.execSQL("DROP TABLE IF EXISTS "+ table_price);
			db.execSQL("DROP TABLE IF EXISTS "+ table_features_micra);
			db.execSQL("DROP TABLE IF EXISTS "+ tabele_tec_micra);
			try {
				File dbFile = new File(DB_PATH + DATABASE_NAME); 
				if(dbFile.exists()){
					dbFile.delete();
				}

				copyDataBase();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	//Open the database, so we can query it 
	public void openDataBase() throws SQLException 
	{ 
		String mPath = DB_PATH + DATABASE_NAME; 
		//Log.v("mPath", mPath); 
		try{
			mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READWRITE); 
		}catch(Exception ex){
			ex.printStackTrace();
		}
		//mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS); 
		//return mDataBase != null; 
	}

	public void create(){
		dbHelper=new DbHelper(ourContext);

	}

	public void close()
	{ 
		if(mDataBase != null) 
			mDataBase.close(); 

	}


	public ArrayList<String> getExternalFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT features FROM features_micra WHERE HEADERS = 'Exterior'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			featuresListArr.add(c.getString(iFeatures));	 
		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getUniqueExternalPetrolFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT * FROM features_micra WHERE HEADERS = 'Exterior'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");
		int iXl_pet = c.getColumnIndex("XL_Pet");
		int iXl_0_pet = c.getColumnIndex("XL_O_Pet");
		int iXv_cvt = c.getColumnIndex("XV_CVT");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){

			String a1 = c.getString(iXl_pet);
			String b1 = c.getString(iXl_0_pet);
			String c1 = c.getString(iXv_cvt);
			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1)){

			}else{
				featuresListArr.add(c.getString(iFeatures));
			}

		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getUniqueExternaDieselFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT * FROM features_micra WHERE HEADERS = 'Exterior'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");
		int iXe_dsl = c.getColumnIndex("XE_Dsl"); 
		int iXl_dsl = c.getColumnIndex("XL_Dsl"); 
		int iXl_0_dsl = c.getColumnIndex("XL_O_Dsl");
		int iXv_dsl = c.getColumnIndex("XV_Dsl");
		int iXv_pre_dsl = c.getColumnIndex("XV_Premium_Dsl");


		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){

			String a1 = c.getString(iXe_dsl);
			String b1 = c.getString(iXl_dsl);
			String c1 = c.getString(iXl_0_dsl);
			String d1 = c.getString(iXv_dsl);
			String e1 = c.getString(iXv_pre_dsl);
			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1) && c1.equalsIgnoreCase(d1) && d1.equalsIgnoreCase(e1)){

			}else{
				featuresListArr.add(c.getString(iFeatures));
			}

		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getComfortFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT features FROM features_micra WHERE HEADERS = 'Comfort and Convenience'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			featuresListArr.add(c.getString(iFeatures));	 
		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getUniqueComfortPetrolFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT * FROM features_micra WHERE HEADERS = 'Comfort and Convenience'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");
		int iXl_pet = c.getColumnIndex("XL_Pet");
		int iXl_0_pet = c.getColumnIndex("XL_O_Pet");
		int iXv_cvt = c.getColumnIndex("XV_CVT");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){

			String a1 = c.getString(iXl_pet);
			String b1 = c.getString(iXl_0_pet);
			String c1 = c.getString(iXv_cvt);
			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1)){

			}else{
				featuresListArr.add(c.getString(iFeatures));
			}

		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getUniqueComfortDieselFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT * FROM features_micra WHERE HEADERS = 'Comfort and Convenience'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");
		int iXe_dsl = c.getColumnIndex("XE_Dsl"); 
		int iXl_dsl = c.getColumnIndex("XL_Dsl"); 
		int iXl_0_dsl = c.getColumnIndex("XL_O_Dsl");
		int iXv_dsl = c.getColumnIndex("XV_Dsl");
		int iXv_pre_dsl = c.getColumnIndex("XV_Premium_Dsl");


		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){

			String a1 = c.getString(iXe_dsl);
			String b1 = c.getString(iXl_dsl);
			String c1 = c.getString(iXl_0_dsl);
			String d1 = c.getString(iXv_dsl);
			String e1 = c.getString(iXv_pre_dsl);
			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1) && c1.equalsIgnoreCase(d1) && d1.equalsIgnoreCase(e1)){

			}else{
				featuresListArr.add(c.getString(iFeatures));
			}

		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getInteriorsFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT features FROM features_micra WHERE HEADERS = 'Interiors'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			featuresListArr.add(c.getString(iFeatures));	 
		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getUniqueInteriorsPetrolFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT * FROM features_micra WHERE HEADERS = 'Interiors'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");
		int iXl_pet = c.getColumnIndex("XL_Pet");
		int iXl_0_pet = c.getColumnIndex("XL_O_Pet");
		int iXv_cvt = c.getColumnIndex("XV_CVT");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){

			String a1 = c.getString(iXl_pet);
			String b1 = c.getString(iXl_0_pet);
			String c1 = c.getString(iXv_cvt);
			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1)){

			}else{
				featuresListArr.add(c.getString(iFeatures));
			}

		}
		c.close();
		close();
		return featuresListArr;

	}


	public ArrayList<String> getUniqueInteriorsDieselFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT * FROM features_micra WHERE HEADERS = 'Interiors'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");
		int iXe_dsl = c.getColumnIndex("XE_Dsl"); 
		int iXl_dsl = c.getColumnIndex("XL_Dsl"); 
		int iXl_0_dsl = c.getColumnIndex("XL_O_Dsl");
		int iXv_dsl = c.getColumnIndex("XV_Dsl");
		int iXv_pre_dsl = c.getColumnIndex("XV_Premium_Dsl");


		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){

			String a1 = c.getString(iXe_dsl);
			String b1 = c.getString(iXl_dsl);
			String c1 = c.getString(iXl_0_dsl);
			String d1 = c.getString(iXv_dsl);
			String e1 = c.getString(iXv_pre_dsl);
			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1) && c1.equalsIgnoreCase(d1) && d1.equalsIgnoreCase(e1)){

			}else{
				featuresListArr.add(c.getString(iFeatures));
			}

		}
		c.close();
		close();
		return featuresListArr;

	}




	public ArrayList<String> getSafetyFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT features FROM features_micra WHERE HEADERS = 'Safety'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			featuresListArr.add(c.getString(iFeatures));	 
		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getUniqueSafetyPetrolFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT * FROM features_micra WHERE HEADERS = 'Safety'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");
		int iXl_pet = c.getColumnIndex("XL_Pet");
		int iXl_0_pet = c.getColumnIndex("XL_O_Pet");
		int iXv_cvt = c.getColumnIndex("XV_CVT");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){

			String a1 = c.getString(iXl_pet);
			String b1 = c.getString(iXl_0_pet);
			String c1 = c.getString(iXv_cvt);
			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1)){

			}else{
				featuresListArr.add(c.getString(iFeatures));
			}

		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getUniqueSafetyDieselFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT * FROM features_micra WHERE HEADERS = 'Safety'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");
		int iXe_dsl = c.getColumnIndex("XE_Dsl"); 
		int iXl_dsl = c.getColumnIndex("XL_Dsl"); 
		int iXl_0_dsl = c.getColumnIndex("XL_O_Dsl");
		int iXv_dsl = c.getColumnIndex("XV_Dsl");
		int iXv_pre_dsl = c.getColumnIndex("XV_Premium_Dsl");


		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){

			String a1 = c.getString(iXe_dsl);
			String b1 = c.getString(iXl_dsl);
			String c1 = c.getString(iXl_0_dsl);
			String d1 = c.getString(iXv_dsl);
			String e1 = c.getString(iXv_pre_dsl);
			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1) && c1.equalsIgnoreCase(d1)  && d1.equalsIgnoreCase(e1)){

			}else{
				featuresListArr.add(c.getString(iFeatures));
			}

		}
		c.close();
		close();
		return featuresListArr;


	}


	public ArrayList<String> getActiveExternalFeatures(){

		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT features FROM features_active WHERE HEADERS = 'Exterior'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			featuresListArr.add(c.getString(iFeatures));
			Log.d("", "DataBase exterior -- " + c.getString(iFeatures));
		}
		c.close();
		close();
		return featuresListArr;

	}


	public ArrayList<String> getUniqueActiveExternalFeatures(){

		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT * FROM features_active WHERE HEADERS = 'Exterior'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");
		int iXe		 = c.getColumnIndex("XE");
		int iXl		 = c.getColumnIndex("XL");
		int iXv		 = c.getColumnIndex("XV");
		int iXv_s	 = c.getColumnIndex("XV_S");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){

			Log.d("", "DataBase exterior -- " + c.getString(iFeatures));

			String a1 = c.getString(iXe);
			String b1 = c.getString(iXl);
			String c1 = c.getString(iXv);
			String d1 = c.getString(iXv_s);

			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1) && c1.equalsIgnoreCase(d1)){

			}else{
				featuresListArr.add(c.getString(iFeatures));
			}

		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getActiveSafetyFeatures(){
		Log.d("", "DataBase safety ==============================");
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT features FROM features_active WHERE HEADERS = 'Safety'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			featuresListArr.add(c.getString(iFeatures));
			Log.d("", "DataBase safety -- " + c.getString(iFeatures));
		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getUniqueActiveSafetyFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT * FROM features_active WHERE HEADERS = 'Safety'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");
		int iXe		 = c.getColumnIndex("XE");
		int iXl		 = c.getColumnIndex("XL");
		int iXv		 = c.getColumnIndex("XV");
		int iXv_s	 = c.getColumnIndex("XV_S");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){

			Log.d("", "DataBase exterior -- " + c.getString(iFeatures));

			String a1 = c.getString(iXe);
			String b1 = c.getString(iXl);
			String c1 = c.getString(iXv);
			String d1 = c.getString(iXv_s);

			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1) && c1.equalsIgnoreCase(d1)){

			}else{
				featuresListArr.add(c.getString(iFeatures));
			}

		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getActiveComfortFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT features FROM features_active WHERE HEADERS = 'Comfort and Convenience'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			featuresListArr.add(c.getString(iFeatures));
			Log.d("", "DataBase comfort -- " + c.getString(iFeatures));
		}
		c.close();
		close();
		return featuresListArr;

	}


	public ArrayList<String> getUniqueActiveComfortFeatures(){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT * FROM features_active WHERE HEADERS = 'Comfort and Convenience'";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Features");
		int iXe		 = c.getColumnIndex("XE");
		int iXl		 = c.getColumnIndex("XL");
		int iXv		 = c.getColumnIndex("XV");
		int iXv_s	 = c.getColumnIndex("XV_S");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			String a1 = c.getString(iXe);
			String b1 = c.getString(iXl);
			String c1 = c.getString(iXv);
			String d1 = c.getString(iXv_s);
			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1) && c1.equalsIgnoreCase(d1)){

			}else{
				featuresListArr.add(c.getString(iFeatures));
			}

		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getModelInfo(String model){
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT "+ model+ " FROM features_micra";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex(model);
		int p =0;
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			featuresListArr.add(c.getString(iFeatures));
			p++;
			Log.d("", "Model Feture  "+ p + " === "+ c.getString(iFeatures));
		}
		c.close();
		close();
		return featuresListArr;

	}


	public ArrayList<ArrayList<String>> getUniqueDieselValues(){
		ArrayList<String> tempData1 				= new ArrayList<String>();
		ArrayList<String> tempData2 				= new ArrayList<String>();
		ArrayList<String> tempData3 				= new ArrayList<String>();
		ArrayList<String> tempData4 				= new ArrayList<String>();
		ArrayList<String> tempData5 				= new ArrayList<String>();
		ArrayList<ArrayList<String>> ferturesArr 	= new ArrayList<ArrayList<String>>();

		openDataBase();
		String query = "SELECT * FROM features_micra";
		Cursor c = mDataBase.rawQuery(query, null);
		int iXe_dsl = c.getColumnIndex("XE_Dsl"); 
		int iXl_dsl = c.getColumnIndex("XL_Dsl"); 
		int iXl_0_dsl = c.getColumnIndex("XL_O_Dsl");
		int iXv_dsl = c.getColumnIndex("XV_Dsl");
		int iXv_pre_dsl = c.getColumnIndex("XV_Premium_Dsl");


		int p =0;
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			//featuresListArr.add(c.getString(iFeatures));
			String a1 = c.getString(iXe_dsl);
			String b1 = c.getString(iXl_dsl);
			String c1 = c.getString(iXl_0_dsl);
			String d1 = c.getString(iXv_dsl);
			String e1 = c.getString(iXv_pre_dsl);

			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1) && c1.equalsIgnoreCase(d1)  && d1.equalsIgnoreCase(e1)){

			}else{
				tempData1.add(a1);
				tempData2.add(b1);
				tempData3.add(c1);
				tempData4.add(d1);
				tempData5.add(e1);
			}

			p++;
		}

		ferturesArr.add(tempData1);
		ferturesArr.add(tempData2);
		ferturesArr.add(tempData3);
		ferturesArr.add(tempData4);
		ferturesArr.add(tempData5);

		c.close();
		close();
		return ferturesArr;

	}


	public ArrayList<ArrayList<String>> getUniqueActiveValues(){
		ArrayList<String> tempData1 				= new ArrayList<String>();
		ArrayList<String> tempData2 				= new ArrayList<String>();
		ArrayList<String> tempData3 				= new ArrayList<String>();
		ArrayList<String> tempData4 				= new ArrayList<String>();
		ArrayList<ArrayList<String>> ferturesArr 	= new ArrayList<ArrayList<String>>();

		openDataBase();
		String query = "SELECT * FROM features_active";
		Cursor c = mDataBase.rawQuery(query, null);
		int iXe		 = c.getColumnIndex("XE");
		int iXl		 = c.getColumnIndex("XL");
		int iXv		 = c.getColumnIndex("XV");
		int iXv_s	 = c.getColumnIndex("XV_S");


		int p =0;
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			//featuresListArr.add(c.getString(iFeatures));
			String a1 = c.getString(iXe);
			String b1 = c.getString(iXl);
			String c1 = c.getString(iXv);
			String d1 = c.getString(iXv_s);

			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1) && c1.equalsIgnoreCase(d1)){

			}else{
				tempData1.add(a1);
				tempData2.add(b1);
				tempData3.add(c1);
				tempData4.add(d1);
			}

			p++;
		}

		ferturesArr.add(tempData1);
		ferturesArr.add(tempData2);
		ferturesArr.add(tempData3);
		ferturesArr.add(tempData4);

		c.close();
		close();
		return ferturesArr;

	}


	public ArrayList<ArrayList<String>> getUniquePetrolValues(){
		ArrayList<String> tempData1 				= new ArrayList<String>();
		ArrayList<String> tempData2 				= new ArrayList<String>();
		ArrayList<String> tempData3 				= new ArrayList<String>();
		ArrayList<ArrayList<String>> ferturesArr 	= new ArrayList<ArrayList<String>>();

		openDataBase();
		String query = "SELECT * FROM features_micra";
		Cursor c = mDataBase.rawQuery(query, null);
		int iXl_pet = c.getColumnIndex("XL_Pet");
		int iXl_0_pet = c.getColumnIndex("XL_O_Pet");
		int iXv_cvt = c.getColumnIndex("XV_CVT");


		int p =0;
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			//featuresListArr.add(c.getString(iFeatures));
			String a1 = c.getString(iXl_pet);
			String b1 = c.getString(iXl_0_pet);
			String c1 = c.getString(iXv_cvt);

			if(a1.equalsIgnoreCase(b1) && b1.equalsIgnoreCase(c1)){

			}else{
				tempData1.add(a1);
				tempData2.add(b1);
				tempData3.add(c1);
			}

			p++;
		}

		ferturesArr.add(tempData1);
		ferturesArr.add(tempData2);
		ferturesArr.add(tempData3);

		c.close();
		close();
		return ferturesArr;

	}




	public ArrayList<String> getActiveModelInfo(String model){
		//Toast.makeText(ourContext, "Model " + model, 0).show();
		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT "+ model+ " FROM features_active";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex(model);

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			featuresListArr.add(c.getString(iFeatures));	 
		}
		c.close();
		close();
		return featuresListArr;

	}


	public ArrayList<String> getTechFetures(){

		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT Models FROM techspec_micra";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Models");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			featuresListArr.add(c.getString(iFeatures));
		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getActiveTechFetures(){

		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT Models FROM techspec_active";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex("Models");

		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			featuresListArr.add(c.getString(iFeatures));
		}
		c.close();
		close();
		return featuresListArr;

	}


	public ArrayList<String> getTechModelInfo(String model){

		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT "+ model+ " FROM techspec_micra";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex(model);
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			featuresListArr.add(c.getString(iFeatures));
		}
		c.close();
		close();
		return featuresListArr;

	}

	public ArrayList<String> getActiveTechModelInfo(String model){

		ArrayList<String> featuresListArr = new ArrayList<String>();
		openDataBase();
		String query = "SELECT "+ model+ " FROM techspec_active";
		Cursor c = mDataBase.rawQuery(query, null);
		int iFeatures = c.getColumnIndex(model);
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			featuresListArr.add(c.getString(iFeatures));
		}
		c.close();
		close();
		return featuresListArr;

	}



	public void addDetails(String coulmnNam, ArrayList<String> values,ArrayList<String> idArr){

		Log.d("======", "Micra Databse value added");

		if(idArr.size()==0){
			ContentValues cv = new ContentValues();
			openDataBase();
			mDataBase.beginTransaction();
			try{
				for(int i=0;i<values.size();i++){
					cv.put(coulmnNam,   values.get(i));	
					cv.toString();
					Log.d("==", "Micra Database Coulmn name " + coulmnNam + " value "+ values.get(i));
					Log.d("==", "CV VALUE "+ cv.toString() );
					mDataBase.insert("price_list_micra", null, cv);

				}
				mDataBase.setTransactionSuccessful();
			}catch(Exception ex){
				ex.printStackTrace();
			}
			finally { 
				mDataBase.endTransaction();
			}
			close();
		}else{
			ContentValues cv = new ContentValues();
			openDataBase();
			mDataBase.beginTransaction();
			try{
				for(int i=0;i<values.size();i++){
					cv.put(coulmnNam,   values.get(i));
					/*String query = "UPDATE price_list_micra SET "+ coulmnNam+"= '"+values.get(i)+"' WHERE Id = '"+idArr.get(i)+"'";
					mDataBase.rawQuery(query, null);*/
					String table = "price_list_micra";
					String where = "Id = '"+idArr.get(i)+"'";
					mDataBase.update(table, cv, where, null);
					Log.d("==", "Micra Database Coulmn name " + coulmnNam + " value "+ values.get(i) +" id = " +idArr.get(i));
				}
				mDataBase.setTransactionSuccessful();
			}catch(Exception ex){
				ex.printStackTrace();
			}
			finally { 
				mDataBase.endTransaction();
			}
			close();
		}

	}



	public void addDetails(ArrayList<String> coulmnNames,ArrayList<String> values){
		ContentValues cv = new ContentValues();
		openDataBase();
		mDataBase.beginTransaction();
		try{
			String query = "SELECT * FROM price_micra";
			Cursor cursor = mDataBase.rawQuery(query, null);

			for(int i=0;i<values.size();i++){
				if(     cursor.getColumnIndex(coulmnNames.get(i))!=-1 ){
					String coulmnNam = coulmnNames.get(i);
					String val		 = values.get(i);
					cv.put(coulmnNam,   val);
					Log.d("==", "Micra Database Coulmn name " + coulmnNam + " value "+ val);
				}
			}
			cursor.close();
			mDataBase.insert("price_micra", null, cv);
			mDataBase.setTransactionSuccessful();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally { 
			mDataBase.endTransaction();
		}
		close();
	}


	public ArrayList<String> getStates(){


		ArrayList<String> stateArr = new ArrayList<String>();
		openDataBase();
		try{
		String query = "SELECT state FROM price_micra";
		Cursor c = mDataBase.rawQuery(query, null);
		int iState = c.getColumnIndex("state");
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			String tempState = c.getString(iState);
			if(!stateArr.contains(tempState)){
				stateArr.add(tempState);
			}
			/*stateArr.add(tempState);*/
		}
		c.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		close();
		return stateArr;
	}

	public void deleteAllRow(){
		openDataBase();
		try{
			mDataBase.delete("price_micra", null, null);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		close();

	}

	/*
	 * Adding all the values into database
	 * Parameter's coulmnName = name of coulmn;
	 * Value = value to insert;
	 * 
	 */
	public void addValue(String coulmnName, String value){
		ContentValues dataVal = new ContentValues();
		dataVal.put(coulmnName,   value);
		try{
			Log.d("==", "Micra Database Coulmn name " + coulmnName + " value "+ value);
			openDataBase();
			mDataBase.insert("price_list_micra", null, dataVal);
			close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	public ArrayList<String> getCitysOfState(String state){
		ArrayList<String> tempCitys = new ArrayList<String>();
		Cursor cursor;
		String stateCoulmn = ourContext.getResources().getString(R.string.price_state);
		String cityCoulmn = ourContext.getResources().getString(R.string.price_city);
		String selection = "WHERE State ='"+state+"'";
		openDataBase();

		String getCityQuery = "SELECT "+ cityCoulmn + 
				" FROM price_micra WHERE state='"+state+"'";
		try{

			cursor 			= mDataBase.rawQuery(getCityQuery, null);
			int iCitys		=	cursor.getColumnIndex(ourContext.getResources().getString(R.string.price_city));
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				tempCitys.add(cursor.getString(iCitys));
			}
			cursor.close();
		}catch(Exception ex){
			Log.d("===", "ERROR getting citys" +ex.toString());
			ex.printStackTrace();
		}
		close();

		//mDataBase.query("price_list_micra", cityCoulmn, selection, null, null, null, null)
		return tempCitys;

	}


	public ArrayList<String> getPrices(String modelType,String stateName, String cityName){
		ArrayList<String> tempCitys = new ArrayList<String>();
		Cursor cursor;
		String stateCoulmn = ourContext.getResources().getString(R.string.price_state);
		String cityCoulmn = ourContext.getResources().getString(R.string.price_city);

		String selection = "WHERE State ='"+stateName+"' and City = '"+cityName+"'";
		openDataBase();

		String getCityQuery = "SELECT * FROM price_micra "+selection;
		try{

			cursor 				= mDataBase.rawQuery(getCityQuery, null);

			if(modelType.equalsIgnoreCase("p")){
				int iPET_XL			=	cursor.getColumnIndex(ourContext.getResources().getString(R.string.new_price_pet_xl));
				int iPET_XL_O		=	cursor.getColumnIndex(ourContext.getResources().getString(R.string.new_price_pet_xl_o));
				int iPET_XV_CVT		=	cursor.getColumnIndex(ourContext.getResources().getString(R.string.new_price_pet_xv_cvt));
				for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
					tempCitys.add(cursor.getString(iPET_XL));
					tempCitys.add(cursor.getString(iPET_XL_O));
					tempCitys.add(cursor.getString(iPET_XV_CVT));
				}
			}else if(modelType.equalsIgnoreCase("d")){
				int iDL_XL			=	cursor.getColumnIndex(ourContext.getResources().getString(R.string.new_price_dl_xl));
				int iDL_XL_O		=	cursor.getColumnIndex(ourContext.getResources().getString(R.string.new_price_dl_xl_o));
				int iDL_XV		=	cursor.getColumnIndex(ourContext.getResources().getString(R.string.new_price_dl_xv));
				int iDL_XV_P		=	cursor.getColumnIndex(ourContext.getResources().getString(R.string.new_price_dl_xv_p));
				
				int iDL_XE		=	cursor.getColumnIndex("DL_XE");
				for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
					tempCitys.add(cursor.getString(iDL_XL));
					tempCitys.add(cursor.getString(iDL_XL_O));
					tempCitys.add(cursor.getString(iDL_XV));
					tempCitys.add(cursor.getString(iDL_XV_P));
					
					
					tempCitys.add(cursor.getString(iDL_XE));
					Log.d("", "Dl_xe "+cursor.getString(iDL_XE));
				}
			}else{
				int iACT_XE			=	cursor.getColumnIndex(ourContext.getResources().getString(R.string.new_price_act_xe));
				int iACT_XL		=	cursor.getColumnIndex(ourContext.getResources().getString(R.string.new_price_act_xl));
				int iACT_XV		=	cursor.getColumnIndex(ourContext.getResources().getString(R.string.new_price_act_xv));
				int iACT_XV_S		=	cursor.getColumnIndex(ourContext.getResources().getString(R.string.new_price_act_xv_s));
				for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
					tempCitys.add(cursor.getString(iACT_XL));
					tempCitys.add(cursor.getString(iACT_XV));
					tempCitys.add(cursor.getString(iACT_XE));
					tempCitys.add(cursor.getString(iACT_XV_S));
				}
			}
			cursor.close();
		}catch(Exception ex){
			Log.d("===", "ERROR getting citys" +ex.toString());
			ex.printStackTrace();
		}
		close();

		//mDataBase.query("price_list_micra", cityCoulmn, selection, null, null, null, null)
		return tempCitys;

	}


}
